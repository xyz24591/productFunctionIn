# 欢迎使用activemq 工具包 ***撰写人 *** `华创寰宇济南研发中心--王兴亮`
> MQ功能列表(现只支持对象|字符串数据)具体参考API
> 注:使用说明可参考API文档，下文不做详细描述

- [x] 普通消息(订阅/队列)
- [x] 级别发送消息,设置消息存活时间(订阅/队列)
- [x] 延迟队列消息(订阅/队列)支持2种时间规则
- [x] 带回执消息


> MQTT 功能列表

- [x] 订阅主题,消息监听处理
- [x] 批量发送

### 1开启消息级别(broke开启级别校验，如果不开启，发送消息级别将无效)
```xml	
	<policyEntry queue=">" prioritizedMessages="true">
```	

### 2延迟发送消息
```xml
	<broker xmlns="http://activemq.apache.org/schema/core"  schedulerSupport="true" >
```
> 必须开启此功能否则消息延迟设置无效
	
### 3设置过期消息自动清除
```xml
    <broker xmlns="..." ... schedulePeriodForDestinationPurge="10000"/>
        <policyEntries>  
            <policyEntry queue=">" gcInactiveDestinations="true" inactiveTimoutBeforeGC="30000"/> 
        </policyEntries>  
        ...
    </broker>
```

> *  schedulePeriodForDestinationPurge：10000  每十秒检查一次，默认为0，此功能关闭
> *  gcInactiveDestinations： true  删除掉不活动队列，默认为false
> *  inactiveTimoutBeforeGC：30000 不活动30秒后删除，默认为60秒 

### 4 MQ初始化用户名密码
```xml
 	<plugins> 
       	<simpleAuthenticationPlugin> 
             <users> 
                  <authenticationUser username="huachuang" password="huachuang" groups="users,admins"/> 
             </users> 
       </simpleAuthenticationPlugin> 
	</plugins> 
	...
	</broker>
```
