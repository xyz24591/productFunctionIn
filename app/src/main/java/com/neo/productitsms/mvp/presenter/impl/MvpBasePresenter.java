package com.neo.productitsms.mvp.presenter.impl;

import com.neo.productitsms.mvp.presenter.MvpPresenter;
import com.neo.productitsms.mvp.view.MvpView;

/**
 * Created by Administrator on 2018/6/25.
 */

public abstract class MvpBasePresenter<V extends MvpView> implements MvpPresenter<V> {
    private V view;

    @Override
    public void attachView(V view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        if (view != null) {
            view = null;

        }
    }

    public V getView() {
        return view;
    }

}
