package com.neo.productitsms.mvp.view.impl;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.neo.productitsms.mvp.presenter.impl.MvpBasePresenter;
import com.neo.productitsms.mvp.view.MvpView;

/**
 * Created by Administrator on 2018/6/25.
 */

public abstract class MvpFragment<P extends MvpBasePresenter, V extends MvpView> extends Fragment {

    protected P presenter;


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (presenter != null) {

            presenter.attachView(bindView());
        }

    }

    //    绑定
    public abstract P bindPresenter();

    public abstract V bindView();

    public P getPresenter() {
        return presenter;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //解除绑定
        if (presenter != null) {
            presenter.detachView();
        }

    }
}
