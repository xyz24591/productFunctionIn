package com.neo.productitsms.mvp.view.impl;

/**
 * Created by Administrator on 2018/6/25.
 */

public class MvpBaseLceView<M> implements MvpLceView<M> {

    @Override
    public void showLoading(boolean pullToRefresh) {

    }

    @Override
    public void showContent() {

    }

    @Override
    public void showError(Exception e, boolean pullToRefresh) {

    }

    @Override
    public void showData(M data) {

    }
}
