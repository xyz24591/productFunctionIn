package com.neo.productitsms.mvp.view.impl;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.neo.productitsms.mvp.presenter.impl.MvpBasePresenter;
import com.neo.productitsms.mvp.view.MvpView;

/**
 * Created by Administrator on 2018/6/25.
 */

public abstract class MvpActivity<P extends MvpBasePresenter> extends AppCompatActivity implements MvpView {

    //    MVP 的架构P 是 动态的
    protected P presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = bindPresenter();
//        关联
        if (presenter != null) {
            presenter.attachView(this);
        }
    }

    //具体的实现我不知道,我给别人实现
    public abstract P bindPresenter();

    @Override
    protected void onDestroy() {
        super.onDestroy();

        //Activity销毁的时候---解除绑定
        if (presenter != null) {
            presenter.detachView();
        }

    }
}
