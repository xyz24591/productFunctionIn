package com.neo.productitsms.mvp.presenter;

import com.neo.productitsms.mvp.view.MvpView;

/**
 * Created by Administrator on 2018/6/25.
 */

public interface MvpPresenter<V extends MvpView> {
    //    绑定
    void attachView(V view);

    //    解除绑定
    void detachView();

}
