package com.neo.productitsms.mvp.view.impl;

import com.neo.productitsms.mvp.view.MvpView;

/**
 * Created by Administrator on 2018/6/25.
 * 请求数据,刷新UI界面监听 加载中 内容 空 页面
 */

public interface MvpLceView<M> extends MvpView {
    //显示加载中的视图(说白了监听加载类型:下啦刷新或者上啦加载)
    void showLoading(boolean pullToRefresh);

    //    显示 ContentView
    void showContent();

    //    加载异常
    void showError(Exception e, boolean pullToRefresh);

    //绑定数据
    void showData(M data);
}
