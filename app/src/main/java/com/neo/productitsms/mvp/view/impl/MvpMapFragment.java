package com.neo.productitsms.mvp.view.impl;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.neo.productitsms.mvp.presenter.MvpPresenter;
import com.neo.productitsms.mvp.view.MvpView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2018/6/25.
 */

public abstract class MvpMapFragment extends Fragment {

    protected Map<MvpPresenter, MvpView> mvpMap;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (mvpMap == null) {

            mvpMap = new HashMap<>();
            bindPresenter();

        }

    }

    public abstract void bindPresenter();


    public void putPresenter(MvpPresenter presenter, MvpView view) {
        presenter.attachView(view);
        mvpMap.put(presenter, view);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mvpMap != null) {

            for (MvpPresenter presenter : mvpMap.keySet()) {
                presenter.detachView();
            }
            mvpMap = null;
        }

    }
}
