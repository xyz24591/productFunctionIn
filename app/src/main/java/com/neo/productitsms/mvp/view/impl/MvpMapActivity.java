package com.neo.productitsms.mvp.view.impl;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.neo.productitsms.mvp.presenter.MvpPresenter;
import com.neo.productitsms.mvp.view.MvpView;

import java.util.HashMap;
import java.util.Map;

/**
 * MVP架构集成到我们的Activity
 * MvpMapActivity---是MVP框架的
 * <p>
 * Created by Administrator on 2018/6/25.
 */

public abstract class MvpMapActivity extends AppCompatActivity {
    //    MVP 的P 是 动态的
    protected Map<MvpPresenter, MvpView> presenterMvp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        关联
        if (presenterMvp == null) {
            presenterMvp = new HashMap<MvpPresenter, MvpView>();
            bindPresenter();
        }

    }

    public abstract void bindPresenter();
//    具体实现 不清楚 交给 子类实现

    public void putPresenter(MvpPresenter presenter, MvpView mvpView) {

        presenter.attachView(mvpView);
        presenterMvp.put(presenter, mvpView);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Activity销毁的时候---解除绑定

        if (presenterMvp != null) {

            for (MvpPresenter presenter : presenterMvp.keySet()) {
                presenter.detachView();
            }
            presenterMvp = null;
        }

    }
}
