package com.neo.productitsms.http.framework;

import com.neo.productitsms.R;
import com.neo.productitsms.bean.BaseBean;
import com.neo.productitsms.utils.ToastUtil;
import com.neo.productitsms.utils.UIUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Administrator on 2018/6/27.
 * 网路回调 统一处理 集成 统一 基类 baseBean
 */

public abstract class RequestCallBack<T extends BaseBean> implements Callback<T> {
    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.raw().code() == 200) {
            T body = response.body();
            if (body == null) {
                onFail(UIUtils.getString(R.string.server_resp_error));
            } else {
                boolean res = body.isRes();
                if (res) {
                    onResponse(response);
                } else {
                    String message = body.getMessage();
                    onResFalse(message);
                }
            }
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        onFail(t.getMessage());
    }

    public abstract void onResponse(Response<T> response);

    public abstract void onResFalse(String message);

    public abstract void onFail(String errorMessage);

}
