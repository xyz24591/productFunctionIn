package com.neo.productitsms.http.framework.api;

/**
 * Created by Administrator on 2018/6/28.
 */

public class ApiParams {
    public static final String LOGIN = "/webService/app/user/login/{userCode}/{pwd}";

    public static final String GETNEWITEMLISTBYEQUID = "/webService/app/gblmitem/getNewItemListByEquId/{equId}";
    public static final String GETITEMHISTORY = "/webService/app/gblmitem/getItemHistory";
    public static final String SCAN_CODE = "/qrcodelogin/scan/{uid}";
    public static final String SCAN_CODE_CONFIRM = "/qrcodelogin/confirm/{uid}/{userId}";
    public static final String SCAN_CODE_CANCEL = "/qrcodelogin/cancel/{uid}";

}
