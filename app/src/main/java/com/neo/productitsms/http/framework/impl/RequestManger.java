package com.neo.productitsms.http.framework.impl;

import android.text.TextUtils;
import android.util.Log;

import com.neo.productitsms.config.ConstantParams;
import com.neo.productitsms.http.framework.api.ApiServer;
import com.neo.productitsms.utils.GsonUtil;

import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by Administrator on 2018/6/28.
 */

public class RequestManger {
    private static RequestManger ourInstance;

    private RequestManger() {
    }

    public static RequestManger getInstance() {
        if (ourInstance == null) {
            ourInstance = new RequestManger();
        }
        return ourInstance;
    }


    /**
     * @return 返回 接口 service
     */
    public ApiServer getServices() {

//        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
//        builder.readTimeout(15, TimeUnit.SECONDS);
//        builder.connectTimeout(30, TimeUnit.SECONDS);
//
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(ConstantParams.BASE_URL)
//                .client(getOkHttpClient())
//                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//
//        ApiServer service = retrofit.create(ApiServer.class);

        ApiServer service = getService(null);
        return service;
    }


    public ApiServer getService(String baseUrl) {

        String url = ConstantParams.BASE_URL;

        if (!TextUtils.isEmpty(baseUrl)) {
            url = baseUrl;
        }

        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(15, TimeUnit.SECONDS);
        builder.connectTimeout(30, TimeUnit.SECONDS);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .client(getOkHttpClient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiServer service = retrofit.create(ApiServer.class);


        return service;
    }


    /**
     * @return 返回 String 的 service
     */
    public ApiServer getStringService() {


//        okhttp3.OkHttpClient.Builder builder = new okhttp3.OkHttpClient().newBuilder();
//        builder.readTimeout(15, TimeUnit.SECONDS);
//        builder.connectTimeout(30, TimeUnit.SECONDS);
//
//        retrofit2.Retrofit retrofit = new retrofit2.Retrofit.Builder()
//                .baseUrl(ConstantParams.BASE_URL)
//                .client(getOkHttpClient())
//                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                .addConverterFactory(ScalarsConverterFactory.create())
//                .build();
//
//        ApiServer service = retrofit.create(ApiServer.class);

        ApiServer service = getStringService(null);

        return service;
    }

    public ApiServer getStringService(String BaseUrl) {

        String url = ConstantParams.BASE_URL;

        if (!TextUtils.isEmpty(BaseUrl)) {
            url = BaseUrl;
        }
        okhttp3.OkHttpClient.Builder builder = new okhttp3.OkHttpClient().newBuilder();
        builder.readTimeout(15, TimeUnit.SECONDS);
        builder.connectTimeout(30, TimeUnit.SECONDS);

        retrofit2.Retrofit retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(url)
                .client(getOkHttpClient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        ApiServer service = retrofit.create(ApiServer.class);

        return service;
    }


    public RequestBody getRequestBody(Object obj) {

        return RequestBody.create(MediaType.parse("application/json; charset=utf-8"), GsonUtil.GsonString(obj));

    }

    private OkHttpClient getOkHttpClient() {
        //日志显示级别
        HttpLoggingInterceptor.Level level = HttpLoggingInterceptor.Level.BODY;
        //新建log拦截器
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Log.d("xyz", "网络日志:==>>" + message);
            }
        });
        loggingInterceptor.setLevel(level);
        //定制OkHttp
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient
                .Builder()
                .addInterceptor(loggingInterceptor)
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS);
        //OkHttp进行添加拦截器loggingInterceptor
        return httpClientBuilder.build();
    }

}
