package com.neo.productitsms.http.framework.api;

import com.neo.productitsms.bean.UserRespBean;
import com.neo.productitsms.pro.third.chart.entity.CharHistoryEntity;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Administrator on 2018/6/28.
 * 网络请求接口
 */

public interface ApiServer {


    /**
     * @param userCode
     * @param pwd
     * @return
     */
    @GET(ApiParams.LOGIN)
    Call<UserRespBean> login(@Path("userCode") String userCode, @Path("pwd") String pwd);

    @POST(ApiParams.GETNEWITEMLISTBYEQUID)
    Call<String> getNewItemListByEquId(@Path("equId") String equId);

    @POST(ApiParams.GETITEMHISTORY)
    Call<String> getItemHistory(@Body RequestBody body);

    /**
     * @param uid
     * @return <p/>
     * 1    手机二维码扫描接口
     */
    @GET(ApiParams.SCAN_CODE)
    Call<String> scanCode(@Path("uid") String uid);

    /**
     * @param uid
     * @return <p/>
     * 2    确认二维码登陆
     */
    @GET(ApiParams.SCAN_CODE_CONFIRM)
    Call<String> scanCodeConfirm(@Path("uid") String uid, @Path("userId") String userId);


    @GET(ApiParams.SCAN_CODE_CANCEL)
    Call<String> scanCodeCancel(@Path("uid") String uid);


}
