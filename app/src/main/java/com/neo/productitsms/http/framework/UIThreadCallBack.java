package com.neo.productitsms.http.framework;

import retrofit2.Response;

/**
 * Created by Administrator on 2018/6/29.
 */

public interface UIThreadCallBack<T> {

    void onResponse(Response<T> response);

    void onResFalse(String message);

    void onFail(String errorMessage);


}
