package com.neo.productitsms.load;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.neo.productitsms.pro.widget.RoundAngleImageView;
import com.youth.banner.loader.ImageLoader;

/**
 * Created by Administrator on 2018/6/30.
 */

public class GlideImageLoad extends ImageLoader {
    @Override
    public void displayImage(Context context, Object path, ImageView imageView) {

        Glide.with(context.getApplicationContext())
                .load(path)
                .centerCrop()
                .into(imageView);

    }

//    @Override
//    public ImageView createImageView(Context context) {
//        //圆角
//        return new RoundAngleImageView(context);
//    }

}
