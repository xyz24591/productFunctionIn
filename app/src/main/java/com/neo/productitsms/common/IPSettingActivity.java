package com.neo.productitsms.common;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.neo.productitsms.R;
import com.neo.productitsms.common.widget.IPEditText;
import com.neo.productitsms.config.ConstantParams;
import com.neo.productitsms.mvp.presenter.impl.MvpBasePresenter;
import com.neo.productitsms.pro.base.view.activity.BaseActivtiy;
import com.neo.productitsms.pro.widget.dialogtip.UIAlertDialog;
import com.neo.productitsms.utils.LogUtils;
import com.neo.productitsms.utils.SPUtils;
import com.neo.productitsms.utils.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Administrator on 2018/7/5.
 */

public class IPSettingActivity extends BaseActivtiy {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.setting_webservice_ipe)
    IPEditText settingWebserviceIpe;
    @BindView(R.id.setting_webservice_port_et)
    EditText settingWebservicePortEt;
    @BindView(R.id.ip_setting_ok)
    Button ipSettingOk;
    @BindView(R.id.ip_setting_default)
    Button ipSettingDefault;

    public static void ipSeetingStart(Context context) {
        Intent intent = new Intent(context, IPSettingActivity.class);
        context.startActivity(intent);

    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_ipsetting;
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {

        String defaultIp = SPUtils.getInstance().getString(IPSettingActivity.this, ConstantParams.DEFAULT_IP);
        String useIp = SPUtils.getInstance().getString(IPSettingActivity.this, ConstantParams.DEFAULT_IP_INUSE);


        if (TextUtils.isEmpty(useIp)) {
            processHttpIP(defaultIp);
        } else {
            processHttpIP(useIp);
        }

    }

    @OnClick({R.id.ip_setting_ok, R.id.ip_setting_default})
    public void OnClick(View view) {

        switch (view.getId()) {

            case R.id.ip_setting_ok:

                String ip = settingWebserviceIpe.getText();
                Editable port = settingWebservicePortEt.getText();

                if (TextUtils.isEmpty(ip)) {
                    ToastUtil.show("请输入IP");
                    return;
                }
                if (TextUtils.isEmpty(port)) {
                    ToastUtil.show("请输入端口");
                    return;
                }

                ConstantParams.BASE_URL = "http://" + ip + ":" + port;

                LogUtils.i("--地址--" + ConstantParams.BASE_URL);

//                SPUtils.getContent(Constant.DEFAULT_IP_INUSE);

                SPUtils.getInstance().putString(IPSettingActivity.this, ConstantParams.DEFAULT_IP_INUSE, ConstantParams.BASE_URL);
                ToastUtil.show("IP设置成功");
                finish();

                break;

            case R.id.ip_setting_default:
                String defaultIp = SPUtils.getInstance().getString(IPSettingActivity.this, ConstantParams.DEFAULT_IP);
                LogUtils.i("默认IP" + defaultIp);
                processHttpIP(defaultIp);
//                processHttpIP("http://192.168.1.208:8080");
                break;
            default:
                break;

        }


    }


    public IPBean processHttpIP(String baseUrl) {

        IPBean ipBean = new IPBean();

//        String baseUrl = Constant.BASE_URL;

        if (!TextUtils.isEmpty(baseUrl)) {

            String replaceUrl = baseUrl.replace("http://", "").trim();

            String[] splitUrl = replaceUrl.split(":");

            if (splitUrl != null && splitUrl.length > 0) {

                ipBean.setUrl(splitUrl[0]);
                ipBean.setPort(splitUrl[1]);
                LogUtils.i("--显示IP-" + splitUrl[0]);
                settingWebserviceIpe.setDefaultIp(splitUrl[0]);
                settingWebservicePortEt.setText(splitUrl[1]);

            }


        }

        return ipBean;
    }


    private class IPBean {

        private String url;
        private String port;


        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getPort() {
            return port;
        }

        public void setPort(String port) {
            this.port = port;
        }
    }

    @Override
    public MvpBasePresenter bindPresenter() {
        return null;
    }

}
