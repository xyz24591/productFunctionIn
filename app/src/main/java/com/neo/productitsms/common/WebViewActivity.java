package com.neo.productitsms.common;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.neo.productitsms.R;
import com.neo.productitsms.mvp.presenter.impl.MvpBasePresenter;
import com.neo.productitsms.pro.base.view.activity.BaseActivtiy;
import com.neo.productitsms.pro.base.view.activity.BaseMapActivity;
import com.neo.productitsms.utils.LogUtils;

import butterknife.BindView;

/**
 * Created by Administrator on 2018/6/28.
 */

//public class WebViewActivity extends BaseMapActivity {
public class WebViewActivity extends BaseActivtiy {

//    String url = "http://192.168.1.136:8080/busi/h5/toPurchaseDetail";
        String url = "https://www.so.com/";
    @BindView(R.id.web_webview)
    WebView webWebview;
    @BindView(R.id.web_pb)
    ProgressBar webPb;

    public static void goWeb(Context context, String url) {
        Intent intent = new Intent(context, WebViewActivity.class);

        if (!TextUtils.isEmpty(url)) {
            Bundle bundle = new Bundle();
            bundle.putString("url", url);
            intent.putExtra("bundle", bundle);
        }

        context.startActivity(intent);
    }


    @Override
    public MvpBasePresenter bindPresenter() {
        return null;
    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_webview;
    }

    @Override
    protected void initData() {
//        Bundle bundle = getIntent().getExtras();
//        url = bundle.getString("url");
    }


    @Override
    protected void initView() {
//        getToolbarTitle().setText("登录");
//        getSubTitle().setText("注册");
        initWebView();

    }


    private void initWebView() {

        WebSettings webSettings = webWebview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webWebview.requestFocus();
//        设置  setWebChromeClient 对象

        webWebview.setWebChromeClient(new WebChrome());
        webWebview.setWebViewClient(new WebViewShow());
        webWebview.loadUrl(url);
    }

    @Override
    protected void setMiddleTitle(String middleTitle) {
        super.setMiddleTitle(middleTitle);
    }

    class WebChrome extends WebChromeClient {
        @Override
        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
            setMiddleTitle(title);

        }

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            webPb.setProgress(newProgress);
        }
    }


    class WebViewShow extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            webWebview.loadUrl(url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

            setBarTitlt(webWebview.getUrl());
            webWebview.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

//            String webWebviewUrl = webWebview.getUrl();
//            getToolbarTitle().setText(webWebviewUrl.trim());
            setBarTitlt(webWebview.getTitle());
        }
    }


    //系统自带监听方法<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

//    @Override
//    public void onBackPressed() {
//        if (wvWebView.canGoBack()) {
//            wvWebView.goBack();
//            return;
//        }
//
//        super.onBackPressed();
//    }


    @Override
    public void onBackPressed() {

        if (webWebview.canGoBack()) {
            webWebview.goBack();
            return;
        }
        super.onBackPressed();
    }


    @Override
    protected void onPause() {
        super.onPause();
        webWebview.onPause();
    }

    @Override
    protected void onResume() {
        webWebview.onResume();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        if (webWebview != null) {
            webWebview.destroy();
            webWebview = null;
        }
        super.onDestroy();
    }


    public void setBarTitlt(String s) {

        if (TextUtils.isEmpty(s)) {
//            getToolbarTitle().setText("");
            setMiddleTitle("");
        } else {
//            getToolbarTitle().setText(s.trim());
            setMiddleTitle(s.trim());
        }

    }

}
