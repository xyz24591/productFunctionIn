package com.neo.productitsms.common.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Administrator on 2018/7/25.
 */


public class MyView extends View {
    public float X = 50;
    public float Y = 50;
    Paint paint = new Paint();

    public MyView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        paint.setColor(Color.BLUE);
        canvas.drawCircle(X, Y, 30, paint);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {

        this.X = event.getX();
        this.Y = event.getY();
        this.invalidate();
        return true;
    }
}
