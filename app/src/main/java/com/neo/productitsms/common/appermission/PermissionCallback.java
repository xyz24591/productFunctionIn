package com.neo.productitsms.common.appermission;

import java.io.Serializable;

/**
 * Created by Administrator on 2018/7/20.
 */

public interface PermissionCallback extends Serializable {

    void onClose();

    void onFinish();

    void onDeny(String permission, int position);

    void onGuarantee(String permission, int position);


}
