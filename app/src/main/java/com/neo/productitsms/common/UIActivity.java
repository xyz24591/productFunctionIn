package com.neo.productitsms.common;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.neo.productitsms.R;
import com.neo.productitsms.mvp.presenter.impl.MvpBasePresenter;
import com.neo.productitsms.pro.base.view.activity.BaseActivtiy;
import com.neo.productitsms.pro.widget.dialogtip.UIAlertDialog;
import com.neo.productitsms.pro.widget.dialogtip.tip.TipLoadDialog;
import com.neo.productitsms.utils.ToastUtil;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Administrator on 2018/7/9.
 * <p>
 * 程序中 常用的 控件展示
 */

public class UIActivity extends BaseActivtiy {


    @BindView(R.id.ui_loading)
    Button uiLoading;
    @BindView(R.id.ui_success)
    Button uiSuccess;
    @BindView(R.id.ui_fail)
    Button uiFail;
    @BindView(R.id.ui_shadow)
    Button uiShadow;
    @BindView(R.id.ui_shadow1)
    Button uiShadow1;
    @BindView(R.id.ui_fade_listener)
    Button uiFadeListener;
    @BindView(R.id.ui_tip2s)
    Button uiTip2s;
    @BindView(R.id.ui_long_tip)
    Button uiLongTip;
    @BindView(R.id.ui_login_tip)
    Button uiLginTip;


    public TipLoadDialog tipLoadDialog;

    public static final String LOADING = "加载中...";
    private final String sucTip = "发送成功";
    private final String failTip = "发送失败";
    private final String infoTip = "字数太多就分段显示，保证textview的宽度";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ui_two_alert_dialog)
    Button uiTwoAlertDialog;
    @BindView(R.id.ui_one_alert_dialog)
    Button uiOneAlertDialog;

    public static void actionStart(Context context) {

        Intent intent = new Intent(context, UIActivity.class);
        context.startActivity(intent);
    }

    @Override
    public MvpBasePresenter bindPresenter() {
        return null;
    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_ui;
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
        initAppBar();
        createDialog();
    }

    private void initAppBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle("对话框提示框");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @OnClick({R.id.ui_loading, R.id.ui_success, R.id.ui_fail, R.id.ui_shadow,
            R.id.ui_shadow1, R.id.ui_fade_listener, R.id.ui_tip2s,
            R.id.ui_long_tip, R.id.ui_one_alert_dialog, R.id.ui_two_alert_dialog, R.id.ui_login_tip})
    public void OnClickBtn(View view) {
        switch (view.getId()) {
            case R.id.ui_loading:
                //默认是无阴影主题
                tipLoadDialog.setMsgAndType(LOADING, TipLoadDialog.ICON_TYPE_LOADING).show();
                break;

            case R.id.ui_success:
                //设置无阴影主题
                tipLoadDialog.setNoShadowTheme().setMsgAndType(sucTip, TipLoadDialog.ICON_TYPE_SUCCESS).show();
                break;

            case R.id.ui_fail:
                //设置无阴影主题
                tipLoadDialog.setNoShadowTheme().setMsgAndType(failTip, TipLoadDialog.ICON_TYPE_FAIL).show();
                break;

            case R.id.ui_shadow:
                //设置提示框阴影主题
                tipLoadDialog.setShadowTheme().setMsgAndType(sucTip, TipLoadDialog.ICON_TYPE_SUCCESS).show();
                break;
            case R.id.ui_shadow1:
                //设置加载框阴影主题
                tipLoadDialog.setShadowTheme().setMsgAndType(LOADING, TipLoadDialog.ICON_TYPE_LOADING).show();
                break;
            case R.id.ui_fade_listener:

                //弹窗消失事件监听
                tipLoadDialog.setNoShadowTheme()
                        .setMsgAndType("消失监听", TipLoadDialog.ICON_TYPE_SUCCESS)
                        .setDismissListener(new TipLoadDialog.DismissListener() {
                            @Override
                            public void onDimissListener() {

                                Toast.makeText(UIActivity.this, "消失", Toast.LENGTH_SHORT).show();
                                //然后可以finish掉当前登录页
                            }
                        })
                        .show();

                break;

            case R.id.ui_tip2s:

                //设置tip提示弹框时间
                tipLoadDialog.setNoShadowTheme()
                        .setMsgAndType("停留2秒消失", TipLoadDialog.ICON_TYPE_SUCCESS)
                        .setTipTime(2000)
                        .show();
                break;
            case R.id.ui_long_tip:

                //设置无阴影主题
                tipLoadDialog.setNoShadowTheme().setMsgAndType(infoTip, TipLoadDialog.ICON_TYPE_INFO).show();
                break;
            case R.id.ui_one_alert_dialog:
                showOne();
                break;
            case R.id.ui_two_alert_dialog:

                showTow();
                break;
            case R.id.ui_login_tip:
                tipLoadDialog.setMsgAndType("登录中...", TipLoadDialog.ICON_TYPE_LOADING).show();
                startThread();

                break;
            default:
                break;
        }
    }


    private void startThread() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tipLoadDialog.setMsgAndType("登录成功", TipLoadDialog.ICON_TYPE_SUCCESS)
                                .setDismissListener(new TipLoadDialog.DismissListener() {
                                    @Override
                                    public void onDimissListener() {
                                        ToastUtil.show("成功");
                                        //然后可以finish掉当前登录页
                                    }
                                }).show();
                    }
                });
            }
        }).start();
    }

    private void showTow() {

        new UIAlertDialog(UIActivity.this).builder().setTitle("退出当前账号")
                .setMsg("再连续登陆15天，就可变身为QQ达人。退出QQ可能会使你现有记录归零，确定退出？")
                .setPositiveButton("确认退出", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

//                        Toast.makeText(UIActivity.this, "确定", Toast.LENGTH_SHORT).show();
                        ToastUtil.show("确定");
                    }
                }).setNegativeButton("取消", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToastUtil.show("取消");
            }
        }).show();

    }


    public void showOne() {

        new UIAlertDialog(UIActivity.this).builder()
                .setMsg("你现在无法接收到新消息提醒。请到系统-设置-通知中开启消息提醒")
                .setNegativeButton("确定", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ToastUtil.show("确定");
                    }
                }).show();

    }

    public void showDialog(String msg, int type) {

        if (tipLoadDialog == null) {
            tipLoadDialog = new TipLoadDialog(UIActivity.this, msg, type);
        }
        if (tipLoadDialog.isShowing()) {
            tipLoadDialog.dismiss();
        }
        tipLoadDialog.setMsgAndType(msg, type).show();
    }

    public void createDialog() {
        if (tipLoadDialog == null) {
            tipLoadDialog = new TipLoadDialog(UIActivity.this);
        }
    }

}
