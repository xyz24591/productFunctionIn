package com.neo.productitsms.common.update;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.method.TransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.neo.productitsms.R;
import com.neo.productitsms.common.update.service.DownloadService;
import com.neo.productitsms.common.widget.UpdateDialog;
import com.neo.productitsms.mvp.presenter.impl.MvpBasePresenter;
import com.neo.productitsms.pro.base.view.activity.BaseActivtiy;
import com.neo.productitsms.utils.LogUtils;

/**
 * Created by Administrator on 2018/6/30.
 */

public class UpdateActivity extends BaseActivtiy {

    //    实例化自定义dialog
    UpdateDialog dialog;
    //    最近版本号
    TextView dialog_hxb_update_TV_new_version;
    //    最新版本大小
    TextView dialog_hxb_update_TV_new_version_size;
    //    更新的内容（文本）
    TextView dialog_hxb_update_TV_update_content;
    //    对话框的取消按钮
    ImageView dialog_hxb_update_btn_update_cancel;

    @Override
    public MvpBasePresenter bindPresenter() {
        return null;
    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_update;
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {


//        startDownServer();

    }


    private DownloadService.DownloadBinder downloadBinder;

    private void startDownServer() {

        ServiceConnection connection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {

                downloadBinder = (DownloadService.DownloadBinder) service;

            }

            @Override
            public void onServiceDisconnected(ComponentName name) {

            }
        };


        Intent intent = new Intent(this, DownloadService.class);
        startService(intent); // 启动服务
        bindService(intent, connection, BIND_AUTO_CREATE); // 绑定服务
        LogUtils.i("启动服务...");

        if (downloadBinder != null) {
            downloadBinder.startDownload("url");
        }


    }

    public void showDialog(View view) {

        initializeUpdateDialog();
    }


    /**
     * 初始化更新的对话框
     */
    private void initializeUpdateDialog() {
//         注： 这里一定要先show之后再找id，不然会报空指针异常，不信可以试试
        UpdateDialog.Builder builder = new UpdateDialog.Builder(UpdateActivity.this);
        dialog = builder
                .style(R.style.Dialog)
                .cancelTouchout(false)
                .widthdp(300)
                .heightdp(430)
                .view(R.layout.layout_update_dialog)
                //立即更新
                .addViewOnclick(R.id.dialog_hxb_update_btn_update_now, listener)
//                取消键
                .addViewOnclick(R.id.dialog_hxb_update_btn_update_cancel, listener)
                .build();
        //        设置按返回键取消不了对话框
        dialog.setCancelable(false);
        dialog.show();

        //      最近版本号
        dialog_hxb_update_TV_new_version = (TextView) dialog.findViewById(R.id.dialog_hxb_update_TV_new_version);
//        最新版本大小
        dialog_hxb_update_TV_new_version_size = (TextView) dialog.findViewById(R.id.dialog_hxb_update_TV_new_version_size);
//        更新的内容
        dialog_hxb_update_TV_update_content = (TextView) dialog.findViewById(R.id.dialog_hxb_update_TV_update_content);
        //  返回键
        dialog_hxb_update_btn_update_cancel = (ImageView) dialog.findViewById(R.id.dialog_hxb_update_btn_update_cancel);

//          设置最新版本号
        dialog_hxb_update_TV_new_version.setText("最新版本：" + "后台的版本号");
//        设置最新版本大小
        dialog_hxb_update_TV_new_version_size.setText("最近版本大小" + "后台给的大小");
//        设置更新的内容
        dialog_hxb_update_TV_update_content.setText("后台给的文本");
    }


    //    对话框的点击事件
    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
//                立即更新点击事件
                case R.id.dialog_hxb_update_btn_update_now:
//              可以做其他事情了
                    dialog.dismiss();
                    break;
//                取消按钮点击事件
                case R.id.dialog_hxb_update_btn_update_cancel:
                    dialog.dismiss();
                    break;
                default:
                    break;
            }
        }
    };

}
