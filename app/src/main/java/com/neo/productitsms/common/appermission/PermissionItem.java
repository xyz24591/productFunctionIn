package com.neo.productitsms.common.appermission;

import java.io.Serializable;

/**
 * Created by Administrator on 2018/7/20.
 */

public class PermissionItem implements Serializable {

    public String PermissionName;
    public String Permission;
    public int PermissionIconRes;

    public PermissionItem(String permission, String permissionName, int permissionIconRes) {
        Permission = permission;
        PermissionName = permissionName;
        PermissionIconRes = permissionIconRes;
    }

    public PermissionItem(String permission) {
        Permission = permission;
    }


}
