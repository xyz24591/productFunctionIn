package com.neo.productitsms.bean;

/**
 * Created by Administrator on 2018/6/29.
 */

public class UserRespBean extends BaseBean {


    /**
     * bring : {"userId":"219d9f94e7b047e8bd72786e2a4d562c","userCode":"wktest","password":null,"deptId":null,"cdt":"2018-09-05 13:23:30","cuser":"17c4520f6cfd1ab53d8745e84681eb49","uuser":"17c4520f6cfd1ab53d8745e84681eb49","udt":"2018-09-05 13:23:30","status":null,"checked":null,"userName":"wk","userInfoEmail":"595469946@qq.com","userInfoMobile":"13233333333","userInfoQq":"","userInfoWeixin":null,"id":null,"dept":{"deptId":null,"id":null,"name":null,"deptName":null,"deptParentId":null,"parentName":null,"deptRemark":null,"status":null,"checked":null,"userNumber":null,"deptAddress":null,"deptType":null,"deptDistrict":null,"deptIdentify":null,"items":null,"treeId":null,"coordinate":null,"pid":null},"group":{"groupId":null,"groupName":null,"groupRemark":null,"userNumber":null,"checked":null,"groupDept":null,"dept":{"deptId":null,"id":null,"name":null,"deptName":null,"deptParentId":null,"parentName":null,"deptRemark":null,"status":null,"checked":null,"userNumber":null,"deptAddress":null,"deptType":null,"deptDistrict":null,"deptIdentify":null,"items":null,"treeId":null,"coordinate":null,"pid":null}},"userInfo":{"userInfoId":"5e9545c9b0474f20b90601bff23338cf","userName":"wk","userInfoUserId":null,"sex":"1","userInfoEmail":"595469946@qq.com","userInfoAddress":null,"userInfoMobile":"13233333333","userInfoPost":"","userInfoStatus":null,"userInfoQq":"","userInfoWeixin":null,"education":null,"skill":null,"remark":null},"moduleId":null,"roleId":null,"deptName":null,"deptlist":null,"rolelist":null,"modulelist":null,"operationlist":null,"grouplist":null,"rmolist":null}
     */

    private BringBean bring;

    public BringBean getBring() {
        return bring;
    }

    public void setBring(BringBean bring) {
        this.bring = bring;
    }

    public static class BringBean {
        /**
         * userId : 219d9f94e7b047e8bd72786e2a4d562c
         * userCode : wktest
         * password : null
         * deptId : null
         * cdt : 2018-09-05 13:23:30
         * cuser : 17c4520f6cfd1ab53d8745e84681eb49
         * uuser : 17c4520f6cfd1ab53d8745e84681eb49
         * udt : 2018-09-05 13:23:30
         * status : null
         * checked : null
         * userName : wk
         * userInfoEmail : 595469946@qq.com
         * userInfoMobile : 13233333333
         * userInfoQq :
         * userInfoWeixin : null
         * id : null
         * dept : {"deptId":null,"id":null,"name":null,"deptName":null,"deptParentId":null,"parentName":null,"deptRemark":null,"status":null,"checked":null,"userNumber":null,"deptAddress":null,"deptType":null,"deptDistrict":null,"deptIdentify":null,"items":null,"treeId":null,"coordinate":null,"pid":null}
         * group : {"groupId":null,"groupName":null,"groupRemark":null,"userNumber":null,"checked":null,"groupDept":null,"dept":{"deptId":null,"id":null,"name":null,"deptName":null,"deptParentId":null,"parentName":null,"deptRemark":null,"status":null,"checked":null,"userNumber":null,"deptAddress":null,"deptType":null,"deptDistrict":null,"deptIdentify":null,"items":null,"treeId":null,"coordinate":null,"pid":null}}
         * userInfo : {"userInfoId":"5e9545c9b0474f20b90601bff23338cf","userName":"wk","userInfoUserId":null,"sex":"1","userInfoEmail":"595469946@qq.com","userInfoAddress":null,"userInfoMobile":"13233333333","userInfoPost":"","userInfoStatus":null,"userInfoQq":"","userInfoWeixin":null,"education":null,"skill":null,"remark":null}
         * moduleId : null
         * roleId : null
         * deptName : null
         * deptlist : null
         * rolelist : null
         * modulelist : null
         * operationlist : null
         * grouplist : null
         * rmolist : null
         */

        private String userId;
        private String userCode;
        private Object password;
        private Object deptId;
        private String cdt;
        private String cuser;
        private String uuser;
        private String udt;
        private Object status;
        private Object checked;
        private String userName;
        private String userInfoEmail;
        private String userInfoMobile;
        private String userInfoQq;
        private Object userInfoWeixin;
        private Object id;
        private DeptBean dept;
        private GroupBean group;
        private UserInfoBean userInfo;
        private Object moduleId;
        private Object roleId;
        private Object deptName;
        private Object deptlist;
        private Object rolelist;
        private Object modulelist;
        private Object operationlist;
        private Object grouplist;
        private Object rmolist;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getUserCode() {
            return userCode;
        }

        public void setUserCode(String userCode) {
            this.userCode = userCode;
        }

        public Object getPassword() {
            return password;
        }

        public void setPassword(Object password) {
            this.password = password;
        }

        public Object getDeptId() {
            return deptId;
        }

        public void setDeptId(Object deptId) {
            this.deptId = deptId;
        }

        public String getCdt() {
            return cdt;
        }

        public void setCdt(String cdt) {
            this.cdt = cdt;
        }

        public String getCuser() {
            return cuser;
        }

        public void setCuser(String cuser) {
            this.cuser = cuser;
        }

        public String getUuser() {
            return uuser;
        }

        public void setUuser(String uuser) {
            this.uuser = uuser;
        }

        public String getUdt() {
            return udt;
        }

        public void setUdt(String udt) {
            this.udt = udt;
        }

        public Object getStatus() {
            return status;
        }

        public void setStatus(Object status) {
            this.status = status;
        }

        public Object getChecked() {
            return checked;
        }

        public void setChecked(Object checked) {
            this.checked = checked;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getUserInfoEmail() {
            return userInfoEmail;
        }

        public void setUserInfoEmail(String userInfoEmail) {
            this.userInfoEmail = userInfoEmail;
        }

        public String getUserInfoMobile() {
            return userInfoMobile;
        }

        public void setUserInfoMobile(String userInfoMobile) {
            this.userInfoMobile = userInfoMobile;
        }

        public String getUserInfoQq() {
            return userInfoQq;
        }

        public void setUserInfoQq(String userInfoQq) {
            this.userInfoQq = userInfoQq;
        }

        public Object getUserInfoWeixin() {
            return userInfoWeixin;
        }

        public void setUserInfoWeixin(Object userInfoWeixin) {
            this.userInfoWeixin = userInfoWeixin;
        }

        public Object getId() {
            return id;
        }

        public void setId(Object id) {
            this.id = id;
        }

        public DeptBean getDept() {
            return dept;
        }

        public void setDept(DeptBean dept) {
            this.dept = dept;
        }

        public GroupBean getGroup() {
            return group;
        }

        public void setGroup(GroupBean group) {
            this.group = group;
        }

        public UserInfoBean getUserInfo() {
            return userInfo;
        }

        public void setUserInfo(UserInfoBean userInfo) {
            this.userInfo = userInfo;
        }

        public Object getModuleId() {
            return moduleId;
        }

        public void setModuleId(Object moduleId) {
            this.moduleId = moduleId;
        }

        public Object getRoleId() {
            return roleId;
        }

        public void setRoleId(Object roleId) {
            this.roleId = roleId;
        }

        public Object getDeptName() {
            return deptName;
        }

        public void setDeptName(Object deptName) {
            this.deptName = deptName;
        }

        public Object getDeptlist() {
            return deptlist;
        }

        public void setDeptlist(Object deptlist) {
            this.deptlist = deptlist;
        }

        public Object getRolelist() {
            return rolelist;
        }

        public void setRolelist(Object rolelist) {
            this.rolelist = rolelist;
        }

        public Object getModulelist() {
            return modulelist;
        }

        public void setModulelist(Object modulelist) {
            this.modulelist = modulelist;
        }

        public Object getOperationlist() {
            return operationlist;
        }

        public void setOperationlist(Object operationlist) {
            this.operationlist = operationlist;
        }

        public Object getGrouplist() {
            return grouplist;
        }

        public void setGrouplist(Object grouplist) {
            this.grouplist = grouplist;
        }

        public Object getRmolist() {
            return rmolist;
        }

        public void setRmolist(Object rmolist) {
            this.rmolist = rmolist;
        }

        public static class DeptBean {
            /**
             * deptId : null
             * id : null
             * name : null
             * deptName : null
             * deptParentId : null
             * parentName : null
             * deptRemark : null
             * status : null
             * checked : null
             * userNumber : null
             * deptAddress : null
             * deptType : null
             * deptDistrict : null
             * deptIdentify : null
             * items : null
             * treeId : null
             * coordinate : null
             * pid : null
             */

            private Object deptId;
            private Object id;
            private Object name;
            private Object deptName;
            private Object deptParentId;
            private Object parentName;
            private Object deptRemark;
            private Object status;
            private Object checked;
            private Object userNumber;
            private Object deptAddress;
            private Object deptType;
            private Object deptDistrict;
            private Object deptIdentify;
            private Object items;
            private Object treeId;
            private Object coordinate;
            private Object pid;

            public Object getDeptId() {
                return deptId;
            }

            public void setDeptId(Object deptId) {
                this.deptId = deptId;
            }

            public Object getId() {
                return id;
            }

            public void setId(Object id) {
                this.id = id;
            }

            public Object getName() {
                return name;
            }

            public void setName(Object name) {
                this.name = name;
            }

            public Object getDeptName() {
                return deptName;
            }

            public void setDeptName(Object deptName) {
                this.deptName = deptName;
            }

            public Object getDeptParentId() {
                return deptParentId;
            }

            public void setDeptParentId(Object deptParentId) {
                this.deptParentId = deptParentId;
            }

            public Object getParentName() {
                return parentName;
            }

            public void setParentName(Object parentName) {
                this.parentName = parentName;
            }

            public Object getDeptRemark() {
                return deptRemark;
            }

            public void setDeptRemark(Object deptRemark) {
                this.deptRemark = deptRemark;
            }

            public Object getStatus() {
                return status;
            }

            public void setStatus(Object status) {
                this.status = status;
            }

            public Object getChecked() {
                return checked;
            }

            public void setChecked(Object checked) {
                this.checked = checked;
            }

            public Object getUserNumber() {
                return userNumber;
            }

            public void setUserNumber(Object userNumber) {
                this.userNumber = userNumber;
            }

            public Object getDeptAddress() {
                return deptAddress;
            }

            public void setDeptAddress(Object deptAddress) {
                this.deptAddress = deptAddress;
            }

            public Object getDeptType() {
                return deptType;
            }

            public void setDeptType(Object deptType) {
                this.deptType = deptType;
            }

            public Object getDeptDistrict() {
                return deptDistrict;
            }

            public void setDeptDistrict(Object deptDistrict) {
                this.deptDistrict = deptDistrict;
            }

            public Object getDeptIdentify() {
                return deptIdentify;
            }

            public void setDeptIdentify(Object deptIdentify) {
                this.deptIdentify = deptIdentify;
            }

            public Object getItems() {
                return items;
            }

            public void setItems(Object items) {
                this.items = items;
            }

            public Object getTreeId() {
                return treeId;
            }

            public void setTreeId(Object treeId) {
                this.treeId = treeId;
            }

            public Object getCoordinate() {
                return coordinate;
            }

            public void setCoordinate(Object coordinate) {
                this.coordinate = coordinate;
            }

            public Object getPid() {
                return pid;
            }

            public void setPid(Object pid) {
                this.pid = pid;
            }
        }

        public static class GroupBean {
            /**
             * groupId : null
             * groupName : null
             * groupRemark : null
             * userNumber : null
             * checked : null
             * groupDept : null
             * dept : {"deptId":null,"id":null,"name":null,"deptName":null,"deptParentId":null,"parentName":null,"deptRemark":null,"status":null,"checked":null,"userNumber":null,"deptAddress":null,"deptType":null,"deptDistrict":null,"deptIdentify":null,"items":null,"treeId":null,"coordinate":null,"pid":null}
             */

            private Object groupId;
            private Object groupName;
            private Object groupRemark;
            private Object userNumber;
            private Object checked;
            private Object groupDept;
            private DeptBeanX dept;

            public Object getGroupId() {
                return groupId;
            }

            public void setGroupId(Object groupId) {
                this.groupId = groupId;
            }

            public Object getGroupName() {
                return groupName;
            }

            public void setGroupName(Object groupName) {
                this.groupName = groupName;
            }

            public Object getGroupRemark() {
                return groupRemark;
            }

            public void setGroupRemark(Object groupRemark) {
                this.groupRemark = groupRemark;
            }

            public Object getUserNumber() {
                return userNumber;
            }

            public void setUserNumber(Object userNumber) {
                this.userNumber = userNumber;
            }

            public Object getChecked() {
                return checked;
            }

            public void setChecked(Object checked) {
                this.checked = checked;
            }

            public Object getGroupDept() {
                return groupDept;
            }

            public void setGroupDept(Object groupDept) {
                this.groupDept = groupDept;
            }

            public DeptBeanX getDept() {
                return dept;
            }

            public void setDept(DeptBeanX dept) {
                this.dept = dept;
            }

            public static class DeptBeanX {
                /**
                 * deptId : null
                 * id : null
                 * name : null
                 * deptName : null
                 * deptParentId : null
                 * parentName : null
                 * deptRemark : null
                 * status : null
                 * checked : null
                 * userNumber : null
                 * deptAddress : null
                 * deptType : null
                 * deptDistrict : null
                 * deptIdentify : null
                 * items : null
                 * treeId : null
                 * coordinate : null
                 * pid : null
                 */

                private Object deptId;
                private Object id;
                private Object name;
                private Object deptName;
                private Object deptParentId;
                private Object parentName;
                private Object deptRemark;
                private Object status;
                private Object checked;
                private Object userNumber;
                private Object deptAddress;
                private Object deptType;
                private Object deptDistrict;
                private Object deptIdentify;
                private Object items;
                private Object treeId;
                private Object coordinate;
                private Object pid;

                public Object getDeptId() {
                    return deptId;
                }

                public void setDeptId(Object deptId) {
                    this.deptId = deptId;
                }

                public Object getId() {
                    return id;
                }

                public void setId(Object id) {
                    this.id = id;
                }

                public Object getName() {
                    return name;
                }

                public void setName(Object name) {
                    this.name = name;
                }

                public Object getDeptName() {
                    return deptName;
                }

                public void setDeptName(Object deptName) {
                    this.deptName = deptName;
                }

                public Object getDeptParentId() {
                    return deptParentId;
                }

                public void setDeptParentId(Object deptParentId) {
                    this.deptParentId = deptParentId;
                }

                public Object getParentName() {
                    return parentName;
                }

                public void setParentName(Object parentName) {
                    this.parentName = parentName;
                }

                public Object getDeptRemark() {
                    return deptRemark;
                }

                public void setDeptRemark(Object deptRemark) {
                    this.deptRemark = deptRemark;
                }

                public Object getStatus() {
                    return status;
                }

                public void setStatus(Object status) {
                    this.status = status;
                }

                public Object getChecked() {
                    return checked;
                }

                public void setChecked(Object checked) {
                    this.checked = checked;
                }

                public Object getUserNumber() {
                    return userNumber;
                }

                public void setUserNumber(Object userNumber) {
                    this.userNumber = userNumber;
                }

                public Object getDeptAddress() {
                    return deptAddress;
                }

                public void setDeptAddress(Object deptAddress) {
                    this.deptAddress = deptAddress;
                }

                public Object getDeptType() {
                    return deptType;
                }

                public void setDeptType(Object deptType) {
                    this.deptType = deptType;
                }

                public Object getDeptDistrict() {
                    return deptDistrict;
                }

                public void setDeptDistrict(Object deptDistrict) {
                    this.deptDistrict = deptDistrict;
                }

                public Object getDeptIdentify() {
                    return deptIdentify;
                }

                public void setDeptIdentify(Object deptIdentify) {
                    this.deptIdentify = deptIdentify;
                }

                public Object getItems() {
                    return items;
                }

                public void setItems(Object items) {
                    this.items = items;
                }

                public Object getTreeId() {
                    return treeId;
                }

                public void setTreeId(Object treeId) {
                    this.treeId = treeId;
                }

                public Object getCoordinate() {
                    return coordinate;
                }

                public void setCoordinate(Object coordinate) {
                    this.coordinate = coordinate;
                }

                public Object getPid() {
                    return pid;
                }

                public void setPid(Object pid) {
                    this.pid = pid;
                }
            }
        }

        public static class UserInfoBean {
            /**
             * userInfoId : 5e9545c9b0474f20b90601bff23338cf
             * userName : wk
             * userInfoUserId : null
             * sex : 1
             * userInfoEmail : 595469946@qq.com
             * userInfoAddress : null
             * userInfoMobile : 13233333333
             * userInfoPost :
             * userInfoStatus : null
             * userInfoQq :
             * userInfoWeixin : null
             * education : null
             * skill : null
             * remark : null
             */

            private String userInfoId;
            private String userName;
            private Object userInfoUserId;
            private String sex;
            private String userInfoEmail;
            private Object userInfoAddress;
            private String userInfoMobile;
            private String userInfoPost;
            private Object userInfoStatus;
            private String userInfoQq;
            private Object userInfoWeixin;
            private Object education;
            private Object skill;
            private Object remark;

            public String getUserInfoId() {
                return userInfoId;
            }

            public void setUserInfoId(String userInfoId) {
                this.userInfoId = userInfoId;
            }

            public String getUserName() {
                return userName;
            }

            public void setUserName(String userName) {
                this.userName = userName;
            }

            public Object getUserInfoUserId() {
                return userInfoUserId;
            }

            public void setUserInfoUserId(Object userInfoUserId) {
                this.userInfoUserId = userInfoUserId;
            }

            public String getSex() {
                return sex;
            }

            public void setSex(String sex) {
                this.sex = sex;
            }

            public String getUserInfoEmail() {
                return userInfoEmail;
            }

            public void setUserInfoEmail(String userInfoEmail) {
                this.userInfoEmail = userInfoEmail;
            }

            public Object getUserInfoAddress() {
                return userInfoAddress;
            }

            public void setUserInfoAddress(Object userInfoAddress) {
                this.userInfoAddress = userInfoAddress;
            }

            public String getUserInfoMobile() {
                return userInfoMobile;
            }

            public void setUserInfoMobile(String userInfoMobile) {
                this.userInfoMobile = userInfoMobile;
            }

            public String getUserInfoPost() {
                return userInfoPost;
            }

            public void setUserInfoPost(String userInfoPost) {
                this.userInfoPost = userInfoPost;
            }

            public Object getUserInfoStatus() {
                return userInfoStatus;
            }

            public void setUserInfoStatus(Object userInfoStatus) {
                this.userInfoStatus = userInfoStatus;
            }

            public String getUserInfoQq() {
                return userInfoQq;
            }

            public void setUserInfoQq(String userInfoQq) {
                this.userInfoQq = userInfoQq;
            }

            public Object getUserInfoWeixin() {
                return userInfoWeixin;
            }

            public void setUserInfoWeixin(Object userInfoWeixin) {
                this.userInfoWeixin = userInfoWeixin;
            }

            public Object getEducation() {
                return education;
            }

            public void setEducation(Object education) {
                this.education = education;
            }

            public Object getSkill() {
                return skill;
            }

            public void setSkill(Object skill) {
                this.skill = skill;
            }

            public Object getRemark() {
                return remark;
            }

            public void setRemark(Object remark) {
                this.remark = remark;
            }
        }
    }
}
