package com.neo.productitsms.bean;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.neo.productitsms.R;
import com.neo.productitsms.utils.UIUtils;

/**
 * Created by Administrator on 2018/6/27.
 * 代表每一个Tab
 */

public class TabItem {

    //    正常情况 下 显示的图片
    private int imageNormal;
    //    选中情况下 显示的图片
    private int imagePress;
    //    Tab 的 名字
    private int title;
    private String titleString;
    private Class<? extends Fragment> fragmentClass;

    private View view;
    private ImageView imageView;
    private TextView textView;
    private Bundle bundle;

    public TabItem(int imageNormal, int imagePress, int title, Class<? extends Fragment> fragmentClass) {
        this.imageNormal = imageNormal;
        this.imagePress = imagePress;
        this.title = title;
        this.fragmentClass = fragmentClass;
    }

    public Class<? extends Fragment> getFragmentClass() {
        return fragmentClass;
    }

    public int getImageNormal() {
        return imageNormal;
    }

    public int getImagePress() {
        return imagePress;
    }

    public int getTitle() {
        return title;
    }

    public String getTitleString() {
        if (title == 0) {
            return "";
        }
        if (TextUtils.isEmpty(titleString)) {
            titleString = UIUtils.getString(title);
        }
        return titleString;
    }

    public Bundle getBundle() {
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putString("title", getTitleString());
        return bundle;
    }

    //还需要提供一个切换Tab方法---改变Tab样式
    public void setChecked(boolean isChecked) {
        if (imageView != null) {
            if (isChecked) {
                imageView.setImageResource(imagePress);
            } else {
                imageView.setImageResource(imageNormal);
            }
        }
        if (textView != null && title != 0) {
            if (isChecked) {
                textView.setTextColor(UIUtils.getColor(R.color.app_tab_selected_color));
            } else {
                textView.setTextColor(UIUtils.getColor(R.color.tabbar_text_normal_color));
            }
        }

    }

    public View getView() {
        if (this.view == null) {
            int layoutId = 0;
            if (this.title == 0) {
                layoutId = R.layout.tab_publish_indicator;
            } else {
                layoutId = R.layout.tab_indicator;
            }
            this.view = UIUtils.inflate(layoutId);
            this.imageView = (ImageView) this.view.findViewById(R.id.iv_tab);
            //判断资源是否存在,不再我就因此
            if (this.title != 0) {
                this.textView = (TextView) this.view.findViewById(R.id.tv_tab);
                this.textView.setText(getTitleString());
            }
            //绑定图片默认资源
            this.imageView.setImageResource(imageNormal);

        }
        return this.view;
    }

}
