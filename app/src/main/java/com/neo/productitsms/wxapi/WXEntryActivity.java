package com.neo.productitsms.wxapi;

import android.os.Bundle;

import com.neo.productitsms.utils.LogUtils;
import com.umeng.socialize.weixin.view.WXCallbackActivity;

/**
 * Created by Administrator on 2018/7/21.
 * <p>
 * 回调Activity
 * 微信
 *
 * @author Administrator
 */

public class WXEntryActivity extends WXCallbackActivity {

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        LogUtils.i("--微信回调 activity-");
    }
}
