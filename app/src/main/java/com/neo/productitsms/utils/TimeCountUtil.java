package com.neo.productitsms.utils;

import android.os.CountDownTimer;
import android.widget.Button;

/**
 * Created by Administrator on 2018/7/9.
 * 倒计时
 */

public class TimeCountUtil extends CountDownTimer {
    private Button mButton;

    /**
     * @param millisInFuture    The number of millis in the future from the call
     *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
     *                          is called.
     * @param countDownInterval The interval along the way to receive
     *                          {@link #onTick(long)} callbacks.
     */
    public TimeCountUtil(long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
    }

    public TimeCountUtil(long millisInFuture, long countDownInterval, Button mButton) {
        super(millisInFuture, countDownInterval);
        this.mButton = mButton;
    }

    @Override
    public void onTick(long millisUntilFinished) {
        // 按钮不可用
        mButton.setEnabled(false);
        String showText = millisUntilFinished / 1000 + "秒后可重新发送";
        mButton.setText(showText);
    }

    @Override
    public void onFinish() {
        // 按钮设置可用
        mButton.setEnabled(true);
        mButton.setText("重新获取验证码");
    }
}
