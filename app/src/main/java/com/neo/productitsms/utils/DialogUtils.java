package com.neo.productitsms.utils;

import android.text.TextUtils;

import com.neo.productitsms.common.UIActivity;
import com.neo.productitsms.pro.widget.dialogtip.tip.TipLoadDialog;

/**
 * Created by Administrator on 2018/7/9.
 */

public class DialogUtils {
    private static DialogUtils dialogUtils;
    private static TipLoadDialog tipLoadDialog;
    public static final String LOADING = "加载中...";
    public static final String SUCCESS = "成功";
    public static final String FAIL = "失败";
    public static final String INFO = "提示";

    public static DialogUtils getInstance() {

        if (dialogUtils == null) {
            dialogUtils = new DialogUtils();
        }
        if (tipLoadDialog == null) {
//            tipLoadDialog = new TipLoadDialog(UIActivity.this);
            tipLoadDialog = new TipLoadDialog(UIUtils.getContext());
        }
        return dialogUtils;
    }

    private DialogUtils() {

    }

    public void showLoading() {
        showLoading("");
    }

    public void showLoading(String msg) {
        if (TextUtils.isEmpty(msg)) {
            msg = LOADING;
        }
        //默认是无阴影主题
        tipLoadDialog.setMsgAndType(msg, TipLoadDialog.ICON_TYPE_LOADING).show();
    }
//TODO  设置一些 监听


    public void showSuccess() {
        showSuccess("");
    }

    public void showSuccess(String msg) {
        if (TextUtils.isEmpty(msg)) {
            msg = SUCCESS;
        }
        //默认是无阴影主题
        tipLoadDialog.setMsgAndType(msg, TipLoadDialog.ICON_TYPE_SUCCESS).show();
    }


    public void showInfo() {
        showInfo("");
    }

    public void showInfo(String msg) {
        if (TextUtils.isEmpty(msg)) {
            msg = INFO;
        }
        //默认是无阴影主题
        tipLoadDialog.setMsgAndType(msg, TipLoadDialog.ICON_TYPE_INFO).show();
    }

    public void showFail() {

    }

    public void showFail(String msg) {

        if (TextUtils.isEmpty(msg)) {
            msg = FAIL;
        }
        //默认是无阴影主题
        tipLoadDialog.setMsgAndType(msg, TipLoadDialog.ICON_TYPE_FAIL).show();

    }


    public void showMsgAndCallBack(String msg, final DialogUtilsCallBack dialogUtilsCallBack) {

        if (TextUtils.isEmpty(msg)) {
            msg = SUCCESS;
        }
        //默认是无阴影主题
        tipLoadDialog.setMsgAndType(msg, TipLoadDialog.ICON_TYPE_SUCCESS).setDismissListener(new TipLoadDialog.DismissListener() {
            @Override
            public void onDimissListener() {

                if (dialogUtilsCallBack != null) {
                    dialogUtilsCallBack.ActionCallBack();
                }

            }
        }).show();

    }

    public void showDisLoading() {
        if (tipLoadDialog.isShowing()) {
            tipLoadDialog.dismiss();
        }
    }


    public DialogUtilsCallBack dialogUtilsCallBack;

    public void setOnDialogUtilsListener(DialogUtilsCallBack dialogUtilsCallBack) {
        this.dialogUtilsCallBack = dialogUtilsCallBack;
    }

    public interface DialogUtilsCallBack {
        void ActionCallBack();
    }

}
