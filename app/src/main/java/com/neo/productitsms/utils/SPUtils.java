package com.neo.productitsms.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.neo.productitsms.config.ConstantParams;

/**
 * Created by Administrator on 2018/7/13.
 * <p>
 * SP 工具
 */

public class SPUtils {

    private static SPUtils spUtils;

    private SharedPreferences preferences;

    private SPUtils() {
    }

    public void initConfig(Context context) {

        if (preferences == null) {
            preferences = context.getSharedPreferences(ConstantParams.SP_NAME, Context.MODE_PRIVATE);
        }

    }

    public static SPUtils getInstance() {

        if (spUtils == null) {
            spUtils = new SPUtils();
        }
        return spUtils;
    }

    public void putString(Context context, String key, String value) {
        initConfig(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();

    }

    public String getString(Context context, String key) {
        initConfig(context);
        return preferences.getString(key, null);
    }

    public void putInt(Context context, String key, int value) {
        initConfig(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public int getInt(Context context, String key) {
        initConfig(context);
        return preferences.getInt(key, 0);
    }

    public void putBoolean(Context context, String key, boolean value) {
        initConfig(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public boolean getBoolean(Context context, String key) {
        initConfig(context);
        return preferences.getBoolean(key, false);
    }

}
