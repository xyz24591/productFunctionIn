package com.neo.productitsms.utils;

import android.content.Context;
import android.text.TextUtils;

import com.neo.productitsms.bean.UserRespBean;
import com.neo.productitsms.config.ConstantParams;
import com.neo.productitsms.pro.login.view.LoginActivity;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Administrator on 2018/8/16.
 */

public class GetUserInfoUtils {

    private static GetUserInfoUtils instance;
    private static Context mContext;

    private GetUserInfoUtils() {
    }

    public static synchronized GetUserInfoUtils getInstance(Context context) {
        mContext = context;
        if (instance == null) {
            instance = new GetUserInfoUtils();
        }
        return instance;
    }

    public UserRespBean.BringBean getUserInfo() {

        String userInfo = SPUtils.getInstance().getString(mContext, ConstantParams.USER_CONTENT);

        UserRespBean.BringBean bringBean = GsonUtil.GsonToBean(userInfo, UserRespBean.BringBean.class);
        if (bringBean == null) {
            bringBean = new UserRespBean.BringBean();
        }

        return bringBean;
    }

    public void setUserInfo(UserRespBean.BringBean bring) {

        SPUtils.getInstance().putString(mContext, ConstantParams.USER_CONTENT, GsonUtil.GsonString(bring));

    }

    public void setLoginInfo(String userName, String pwd) {

        SPUtils.getInstance().putString(mContext, ConstantParams.USER_NAME, userName);
        SPUtils.getInstance().putString(mContext, ConstantParams.USER_PWD, userName);

    }

    public Map<String, String> getLoginInfo() {

        Map<String, String> userMap = new HashMap<>();

        String userName = SPUtils.getInstance().getString(mContext, ConstantParams.USER_NAME);
        String pwd = SPUtils.getInstance().getString(mContext, ConstantParams.USER_PWD);

        if (!TextUtils.isEmpty(userName) && !TextUtils.isEmpty(pwd)) {

            userMap.put(ConstantParams.USER_NAME, userName);
            userMap.put(ConstantParams.USER_PWD, pwd);

            return userMap;

        } else {
            return null;
        }


    }


}
