package com.neo.productitsms.calendarui.basefcal.listener;

import org.joda.time.LocalDate;

public interface OnCalendarChangedListener {
    void onCalendarChanged(LocalDate date);
}
