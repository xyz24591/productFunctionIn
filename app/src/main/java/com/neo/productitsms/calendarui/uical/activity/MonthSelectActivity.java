package com.neo.productitsms.calendarui.uical.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;


import com.neo.productitsms.R;
import com.neo.productitsms.calendarui.basefcal.calendar.MonthCalendar;
import com.neo.productitsms.calendarui.basefcal.listener.OnMonthCalendarChangedListener;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.List;


public class MonthSelectActivity extends Activity {

    private MonthCalendar monthcalendar;
    private TextView dateText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        dateText = (TextView) findViewById(R.id.tv_date);

        monthcalendar = (MonthCalendar) findViewById(R.id.monthcalendar);
        monthcalendar.setOnMonthCalendarChangedListener(new OnMonthCalendarChangedListener() {
            @Override
            public void onMonthCalendarChanged(LocalDate dateTime) {
                dateText.setText(dateTime.toString());
            }
        });


        monthcalendar.post(new Runnable() {
            @Override
            public void run() {

                List<String> list = new ArrayList<>();
                list.add("2018-09-21");
                list.add("2018-10-21");
                list.add("2018-10-1");
                list.add("2018-10-15");
                list.add("2018-10-18");
                list.add("2018-10-26");
                list.add("2018-11-21");

                monthcalendar.setPointList(list);
            }
        });

    }


    public void toLastMonth(View view) {
        monthcalendar.toLastPager();

    }

    public void toNextMonth(View view) {
        monthcalendar.toNextPager();
    }

    public void toToday(View view) {
        monthcalendar.toToday();
    }

    public void setDate(View view) {
        monthcalendar.setDate("2018-10-11");
      //  monthcalendar.setDate("1991-04-14");
    }
}
