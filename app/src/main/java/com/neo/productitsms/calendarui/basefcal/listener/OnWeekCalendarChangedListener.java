package com.neo.productitsms.calendarui.basefcal.listener;

import org.joda.time.LocalDate;


public interface OnWeekCalendarChangedListener {
    void onWeekCalendarChanged(LocalDate date);
}
