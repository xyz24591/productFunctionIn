package com.neo.productitsms.calendarui.basefcal.listener;

import org.joda.time.LocalDate;


public interface OnMonthCalendarChangedListener {
    void onMonthCalendarChanged(LocalDate date);
}
