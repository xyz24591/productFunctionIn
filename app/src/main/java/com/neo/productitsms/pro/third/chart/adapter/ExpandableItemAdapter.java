package com.neo.productitsms.pro.third.chart.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.neo.productitsms.R;
import com.neo.productitsms.pro.third.chart.DemoLineChartActivity;
import com.neo.productitsms.pro.third.chart.LineChartShowActivity;
import com.neo.productitsms.pro.third.chart.entity.BranchesEntity;
import com.neo.productitsms.pro.third.chart.entity.ChartEntity;
import com.neo.productitsms.utils.GsonUtil;
import com.neo.productitsms.utils.LogUtils;
import com.neo.productitsms.utils.ToastUtil;

import java.util.List;

/**
 * Created by Administrator on 2018/10/11.
 */

public class ExpandableItemAdapter extends BaseMultiItemQuickAdapter<MultiItemEntity, BaseViewHolder> {

    private static final String TAG = ExpandableItemAdapter.class.getSimpleName();

    public static final int TYPE_LEVEL_0 = 0;
    public static final int TYPE_LEVEL_1 = 1;
    public static final int TYPE_PERSON = 2;

    List<MultiItemEntity> data;

    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param data A new list is created out of this one to avoid mutable list
     */
    public ExpandableItemAdapter(List<MultiItemEntity> data) {
        super(data);
        this.data = data;
        addItemType(TYPE_LEVEL_0, R.layout.layout_branch_item);
        addItemType(TYPE_PERSON, R.layout.layout_item_leaf);
//        addItemType(TYPE_PERSON, R.layout.item_expandable_lv2);

    }


    @Override
    protected void convert(final BaseViewHolder holder, MultiItemEntity item) {

        switch (holder.getItemViewType()) {
            case TYPE_LEVEL_0:
                final BranchesEntity lv0 = (BranchesEntity) item;

                BranchesEntity multiItemEntity = (BranchesEntity) data.get(holder.getAdapterPosition());
                List<ChartEntity> subItems = multiItemEntity.getSubItems();

                holder.setText(R.id.title, lv0.getTitle() + "   (" + subItems.size() + ")")
                        .setImageResource(R.id.iv, lv0.isExpanded() ? R.mipmap.arrow_b : R.mipmap.arrow_r);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int pos = holder.getAdapterPosition();
                        Log.d(TAG, "Level 0 item pos: " + pos);
                        if (lv0.isExpanded()) {
                            collapse(pos);
                        } else {
//                            if (pos % 3 == 0) {
//                                expandAll(pos, false);
//                            } else {
                            expand(pos);
//                            }
                        }


                    }
                });

                break;

            case TYPE_PERSON:

                final ChartEntity person = (ChartEntity) item;

                holder.setText(R.id.tv, "名称:" + person.getNewDataName() + "\n最新数据时间 :" + person.getResentlyCheckDate() + "\n最新数据:" + person.getNewData());

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pos = holder.getAdapterPosition();
//                        remove(pos);

                        LogUtils.i("----" + GsonUtil.GsonString(person));

                        ToastUtil.showShort(String.valueOf(pos));

//                        Intent intent = new Intent(mContext, LineChartShowActivity.class);
////                        Intent intent = new Intent(mContext, DemoLineChartActivity.class);
//                        mContext.startActivity(intent);

                        String newDataId = person.getNewDataId();
                        if (TextUtils.isEmpty(newDataId)){
                            ToastUtil.showShort("未获取到 ID");
                            return;
                        }
                        LineChartShowActivity.actoinStart(mContext,newDataId);


                    }
                });

                break;

            default:
                break;

        }


    }
}
