package com.neo.productitsms.pro.ui.Entity;

import com.neo.productitsms.pro.base.entity.BaseEntity;

/**
 * Created by Administrator on 2018/8/2.
 */

public class SysNoticeEntity extends BaseEntity {

    private String sysTime;
    private String sysNotice;
    private String sysDetail;

    public String getSysTime() {
        return sysTime;
    }

    public void setSysTime(String sysTime) {
        this.sysTime = sysTime;
    }

    public String getSysNotice() {
        return sysNotice;
    }

    public void setSysNotice(String sysNotice) {
        this.sysNotice = sysNotice;
    }

    public String getSysDetail() {
        return sysDetail;
    }

    public void setSysDetail(String sysDetail) {
        this.sysDetail = sysDetail;
    }
}
