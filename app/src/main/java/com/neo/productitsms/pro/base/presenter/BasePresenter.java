package com.neo.productitsms.pro.base.presenter;


import android.content.Context;

import com.google.gson.Gson;
import com.neo.productitsms.mvp.presenter.impl.MvpBasePresenter;
import com.neo.productitsms.mvp.view.MvpView;
import com.neo.productitsms.pro.base.model.BaseModel;

/**
 * Created by Administrator on 2018/6/26.
 */

public abstract class BasePresenter<M extends BaseModel> extends MvpBasePresenter {
    private Context context;

    private Gson gson;

    private M baseModel;

    public BasePresenter(Context context) {
        this.context = context;
        this.gson = new Gson();
        this.baseModel = bindModel();
    }

    public M bindModel() {
        return null;
    }

    public M getModel() {

        if (this.baseModel == null) {
            baseModel = bindModel();
        }
        return baseModel;
    }

    public Context getContext() {
        return context;
    }

    public Gson getGson() {
        if (this.gson == null) {
            this.gson = new Gson();
        }
        return gson;
    }

    public interface OnUIThreadListener<T> {
        void onResult(T result, String errorMessage);
    }

}
