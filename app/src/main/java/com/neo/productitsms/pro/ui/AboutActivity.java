package com.neo.productitsms.pro.ui;

import com.neo.productitsms.R;
import com.neo.productitsms.mvp.presenter.impl.MvpBasePresenter;
import com.neo.productitsms.pro.base.view.activity.BaseActivtiy;

/**
 * Created by Administrator on 2018/7/28.
 */

public class AboutActivity extends BaseActivtiy {
    @Override
    public MvpBasePresenter bindPresenter() {
        return null;
    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_about;
    }

    @Override
    protected void initData() {

        
    }

    @Override
    protected void initView() {

    }
}
