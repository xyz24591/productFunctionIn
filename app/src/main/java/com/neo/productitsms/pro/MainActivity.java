package com.neo.productitsms.pro;


import android.content.Intent;
import android.support.v4.app.FragmentTabHost;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TabHost;
import android.widget.TabWidget;

import com.neo.productitsms.R;
import com.neo.productitsms.bean.TabItem;
import com.neo.productitsms.mvp.presenter.impl.MvpBasePresenter;
import com.neo.productitsms.pro.allfunction.view.FunctionFragment;
import com.neo.productitsms.pro.base.view.activity.BaseActivtiy;
import com.neo.productitsms.pro.base.view.activity.BaseMapActivity;
import com.neo.productitsms.pro.compose.view.ComposeFragment;
import com.neo.productitsms.pro.home.view.fragment.HomeFragment;
import com.neo.productitsms.pro.login.view.LoginActivity;
import com.neo.productitsms.pro.main.presenter.MainPresenter;
import com.neo.productitsms.pro.message.view.MessageFragment;
import com.neo.productitsms.pro.profile.view.ProfileFragment;
import com.neo.productitsms.pro.widget.dialogtip.UIAlertDialog;
import com.neo.productitsms.utils.ToastUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MainActivity extends BaseActivtiy implements TabHost.OnTabChangeListener {

    @BindView(android.R.id.tabcontent)
    FrameLayout tabcontent;
    @BindView(android.R.id.tabs)
    TabWidget tabs;
    @BindView(android.R.id.tabhost)
    FragmentTabHost fragmentTabHost;
    private List<TabItem> tabItemList;

    private MainPresenter homePresenter;

    @Override
    protected void initData() {
        tabItemList = new ArrayList<>();

        //添加首页Tab
        tabItemList.add(new TabItem(R.mipmap.tabbar_home
                , R.mipmap.tabbar_home_highlighted, R.string.tab_home_text, HomeFragment.class));

        //添加发现Tab
        tabItemList.add(new TabItem(R.mipmap.tabbar_discover
                , R.mipmap.tabbar_discover_highlighted, R.string.tab_discover_text, FunctionFragment.class));


//        //添加发布Tab
//        tabItemList.add(new TabItem(R.drawable.tabbar_compose_button
//                , R.drawable.tabbar_compose_button_highlighted, 0, ComposeFragment.class));

        //添加消息中心Tab
        tabItemList.add(new TabItem(R.mipmap.tabbar_message_center
                , R.mipmap.tabbar_message_center_highlighted, R.string.tab_message_text, MessageFragment.class));


        //添加我的Tab
        tabItemList.add(new TabItem(R.mipmap.tabbar_profile
                , R.mipmap.tabbar_profile_highlighted, R.string.tab_profile_text, ProfileFragment.class));


//        //添加首页Tab
//        tabItemList.add(new TabItem(R.mipmap.tabbar_home
//                , R.mipmap.tabbar_home_highlighted, R.string.tab_home_text, HomeFragment.class));
//        //添加消息中心Tab
//        tabItemList.add(new TabItem(R.mipmap.tabbar_message_center
//                , R.mipmap.tabbar_message_center_highlighted, R.string.tab_message_text, FunctionFragment.class));
//        //添加发布Tab
//        tabItemList.add(new TabItem(R.drawable.tabbar_compose_button
//                , R.drawable.tabbar_compose_button_highlighted, 0, ComposeFragment.class));
//        //添加发现Tab
//        tabItemList.add(new TabItem(R.mipmap.tabbar_discover
//                , R.mipmap.tabbar_discover_highlighted, R.string.tab_discover_text, MessageFragment.class));
//        //添加我的Tab
//        tabItemList.add(new TabItem(R.mipmap.tabbar_profile
//                , R.mipmap.tabbar_profile_highlighted, R.string.tab_profile_text, ProfileFragment.class));
    }

    @Override
    protected void initView() {
        //绑定TabHost(绑定我们的body)
        fragmentTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);
        fragmentTabHost.getTabWidget().setDividerDrawable(null);

        for (int i = 0; i < tabItemList.size(); i++) {
            TabItem tabItem = tabItemList.get(i);
            //绑定Fragment(将Fragment添加到FragmentTabHost组件上面)
            //newTabSpec:代表Tab名字
            //setIndicator:图片
            TabHost.TabSpec tabSpec = fragmentTabHost
                    .newTabSpec(tabItem.getTitleString())
                    .setIndicator(tabItem.getView());

            //添加Fragment
            //tabSpec:选项卡
            //tabItem.getFragmentClass():具体的Fragment
            //tabItem.getBundle():给我们的具体的Fragment传参数
            fragmentTabHost.addTab(tabSpec, tabItem.getFragmentClass(), tabItem.getBundle());
            //给我们的Tab按钮设置背景
            fragmentTabHost.getTabWidget()
                    .getChildAt(i)
                    .setBackgroundColor(getResources().getColor(R.color.main_bottom_bg));

            //监听点击Tab
            fragmentTabHost.setOnTabChangedListener(this);
            //默认选中第一个Tab
            if (i == 0) {
                tabItem.setChecked(true);
            }
        }
//initTab();

    }


//    private void initTab() {
//
//        getToolbarTitle().setText("登录");
//        getSubTitle().setText("注册");
//
//    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_main;
    }

    /**
     * 设置不显示返回按钮
     *
     * @return
     */
    protected boolean isShowBacking() {
        return false;
    }

    @Override
    public MvpBasePresenter bindPresenter() {


        homePresenter = new MainPresenter(this);

//        putPresenter(homePresenter, new ResultView<String>() {
//            @Override
//            public void onResult(String data, String errorMessage) {
//
//            }
//        });


        return homePresenter;
    }

    //    @Override
//    public void bindPresenter() {
//        homePresenter = new MainPresenter(this);
//
////        putPresenter(homePresenter, new ResultView<String>() {
////            @Override
////            public void onResult(String data, String errorMessage) {
////
////            }
////        });
//
//    }

    private int index = 0;

    @Override
    public void onTabChanged(String tabId) {

        if (TextUtils.isEmpty(tabId)) {

            ToastUtil.show("中间的");


//            showTow();

            fragmentTabHost.setCurrentTab(index);
            return;
        }

        //重置Tab样式
        for (int i = 0; i < tabItemList.size(); i++) {
            TabItem tabItem = tabItemList.get(i);

            if (tabId.equals(tabItem.getTitleString())) {
                //选中设置为选中壮体啊
                tabItem.setChecked(true);
                index = i;
            } else {
                //没有选择Tab样式设置为正常
                tabItem.setChecked(false);
            }
        }

    }

    private void showTow() {

        new UIAlertDialog(MainActivity.this).builder().setTitle("退出当前账号")
                .setMsg("再连续登陆15天，就可变身为QQ达人。退出QQ可能会使你现有记录归零，确定退出？")
                .setPositiveButton("确认退出", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        startActivity(new Intent(MainActivity.this, LoginActivity.class));

                    }
                }).setNegativeButton("取消", new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        }).show();

    }

    public void showOne() {

        new UIAlertDialog(MainActivity.this).builder()
                .setMsg("你现在无法接收到新消息提醒。请到系统-设置-通知中开启消息提醒")
                .setNegativeButton("确定", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                }).show();

    }

}
