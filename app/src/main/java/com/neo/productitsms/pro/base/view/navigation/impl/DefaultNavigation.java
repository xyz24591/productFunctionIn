package com.neo.productitsms.pro.base.view.navigation.impl;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.neo.productitsms.R;

/**
 * Created by Administrator on 2018/6/26.
 */

public class DefaultNavigation extends AbsNavigation<DefaultNavigation.Builder.DefaultNavigationParams>{

    protected DefaultNavigation(DefaultNavigation.Builder.DefaultNavigationParams params) {
        super(params);
    }

    @Override
    public int getLayoutId() {
        return R.layout.navigation_default;
    }

    @Override
    public void createAndBind() {
        super.createAndBind();
        setText(R.id.tv_left,getParams().leftTextColor,getParams().leftText,getParams().leftOnClickListener);
        setText(R.id.tv_center,getParams().centerTextColor,getParams().centerText,getParams().centerOnClickListener);
        setText(R.id.tv_right,getParams().rightTextColor,getParams().rightText,getParams().rightOnClickListener);
    }

    protected void setText(int viewId, int text, View.OnClickListener onClickListener){
        setText(viewId,0,getString(text),onClickListener);
    }

    protected void setText(int viewId, String text, View.OnClickListener onClickListener){
        setText(viewId,0,text,onClickListener);
    }

    protected void setText(int viewId,int textColor, int text, View.OnClickListener onClickListener){
        setText(viewId,textColor,getString(text),onClickListener);
    }

    protected void setText(int viewId,int textColor, String text, View.OnClickListener onClickListener){
        View view = findViewById(viewId);
        if (view == null && !(view instanceof TextView)){
            return;
        }
        TextView textView = (TextView) view;
        if (TextUtils.isEmpty(text)){
            textView.setVisibility(View.GONE);
        }else{
            textView.setVisibility(View.VISIBLE);
            textView.setText(text);
            textView.setOnClickListener(onClickListener);

            if (textColor > 0){
                textView.setTextColor(getColor(textColor));
            }
        }

    }

    public static class Builder extends AbsNavigation.Builder{

        protected DefaultNavigationParams navigationParams;

        public Builder(Context context, ViewGroup parent) {
            navigationParams = bindNavigationParams(context,parent);
        }

        protected DefaultNavigationParams bindNavigationParams(Context context, ViewGroup parent){
            return new DefaultNavigationParams(context,parent);
        }

        public Builder setLeftText(String leftText){
            navigationParams.leftText = leftText;
            return this;
        }

        public Builder setCenterText(String centerText){
            navigationParams.centerText = centerText;
            return this;
        }

        public Builder setRightText(String rightText){
            navigationParams.rightText = rightText;
            return this;
        }

        public Builder setLeftText(int leftText){
            navigationParams.leftText = navigationParams.context.getResources().getString(leftText);
            return this;
        }

        public Builder setCenterText(int centerText){
            navigationParams.centerText = navigationParams.context.getResources().getString(centerText);
            return this;
        }

        public Builder setRightText(int rightText){
            navigationParams.rightText = navigationParams.context.getResources().getString(rightText);
            return this;
        }

        public Builder setLeftTextColor(int leftTextColor){
            navigationParams.leftTextColor = leftTextColor;
            return this;
        }

        public Builder setCenterTextColor(int centerTextColor){
            navigationParams.centerTextColor = centerTextColor;
            return this;
        }

        public Builder setRightTextColor(int rightTextColor){
            navigationParams.rightTextColor = rightTextColor;
            return this;
        }

        public Builder setLeftOnClickListener(View.OnClickListener leftOnClickListener){
            navigationParams.leftOnClickListener = leftOnClickListener;
            return this;
        }

        public Builder setCenterOnClickListener(View.OnClickListener centerOnClickListener){
            navigationParams.centerOnClickListener = centerOnClickListener;
            return this;
        }

        public Builder setRightOnClickListener(View.OnClickListener rightOnClickListener){
            navigationParams.rightOnClickListener = rightOnClickListener;
            return this;
        }

        @Override
        public DefaultNavigation create() {
            DefaultNavigation defaultNavigation = new DefaultNavigation(navigationParams);
            return defaultNavigation;
        }

        public static class DefaultNavigationParams extends AbsNavigation.Builder.NavigationParams{
            public String leftText;
            public String centerText;
            public String rightText;
            public int leftTextColor;
            public int centerTextColor;
            public int rightTextColor;

            public View.OnClickListener leftOnClickListener;
            public View.OnClickListener centerOnClickListener;
            public View.OnClickListener rightOnClickListener;
            public DefaultNavigationParams(Context context, ViewGroup parent) {
                super(context, parent);
            }
        }
    }
}
