package com.neo.productitsms.pro;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.neo.productitsms.R;
import com.neo.productitsms.config.AppLoginConfig;
import com.neo.productitsms.pro.base.view.views.CircleIndicator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/6/26.
 */

public class GuideActivity extends FragmentActivity {
    private List<Integer> imageList;
    private List<ImageView> viewList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_guide);
        initImageList();
        initViewList();
        initView();
    }

    private void initView() {
        ViewPager viewPager = findViewById(R.id.guide_viewpager);
        CircleIndicator indicator = findViewById(R.id.guide_indicator);
        viewPager.setAdapter(new GuideViewPager());
        indicator.setViewPager(viewPager);
        viewPager.setCurrentItem(0);


        final TextView guideInTv = findViewById(R.id.guide_in_tv);
        guideInTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppLoginConfig.getInstance().putBoolean(GuideActivity.this, AppLoginConfig.IS_FIRST_IN, true);
                startActivity(new Intent(GuideActivity.this, MainActivity.class));
                finish();
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == imageList.size() - 1) {
                    guideInTv.setVisibility(View.VISIBLE);
                } else {
                    guideInTv.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void initViewList() {

        viewList = new ArrayList<>();
        for (int i = 0; i < imageList.size(); i++) {
            ImageView imageView = new ImageView(this);
            viewList.add(imageView);
        }
    }

    private void initImageList() {
        imageList = new ArrayList<>();

        imageList.add(R.mipmap.welcome_bg);
        imageList.add(R.mipmap.welcome_bg);
        imageList.add(R.mipmap.welcome_bg);

    }

    class GuideViewPager extends PagerAdapter {


        @Override
        public int getCount() {
            return imageList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {

            container.removeView(viewList.get(position));

        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            ImageView imageView = viewList.get(position);

            imageView.setImageResource(imageList.get(position));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            container.addView(imageView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            return imageView;

        }
    }

}
