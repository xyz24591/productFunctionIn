package com.neo.productitsms.pro.login.model;

import android.content.Context;

import com.neo.productitsms.bean.UserBean;
import com.neo.productitsms.bean.UserRespBean;
import com.neo.productitsms.http.framework.RequestCallBack;
import com.neo.productitsms.http.framework.impl.RequestManger;
import com.neo.productitsms.pro.base.model.BaseModel;
import com.neo.productitsms.utils.LogUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Administrator on 2018/6/29.
 */

public class LoginModel extends BaseModel {
    public LoginModel(Context context) {
        super(context);
    }

    public void login(UserBean userBean, RequestCallBack requestCallBack) {
        LogUtils.i("去服务器 登录...");

        Call<UserRespBean> login = RequestManger.getInstance().getServices().login(userBean.getUsername(), userBean.getPassword());
        login.enqueue(requestCallBack);
    }

}
