package com.neo.productitsms.pro.qrpro;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.neo.productitsms.R;
import com.neo.productitsms.mvp.presenter.impl.MvpBasePresenter;
import com.neo.productitsms.pro.base.view.activity.BaseActivtiy;
import com.neo.productitsms.qrcode.encoding.EncodingHandler;
import com.neo.productitsms.utils.LogUtils;
import com.neo.productitsms.utils.ToastUtil;
import com.neo.productitsms.utils.UIUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;

/**
 * Created by Administrator on 2018/6/30.
 */

public class QrShowActivity extends BaseActivtiy implements View.OnClickListener, View.OnLongClickListener, View.OnTouchListener {
    @BindView(R.id.qr_start_btn)
    Button qrStartBtn;
    @BindView(R.id.qr_string_et)
    EditText qrStringEt;
    @BindView(R.id.qr_start_convert_btn)
    Button qrStartConvertBtn;
    @BindView(R.id.qr_show_image)
    ImageView qrShowImage;
    private Bitmap mBitmap;

    @BindView(R.id.qr_change_server_btn)
    Button qrChangeServerBtn;


    public static void actionStart(Context context) {
        Intent intent = new Intent(context, QrShowActivity.class);
        context.startActivity(intent);
    }

    @Override
    public MvpBasePresenter bindPresenter() {
        return null;
    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_qrshow;
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
        qrStartBtn.setOnClickListener(this);
        qrStartConvertBtn.setOnClickListener(this);
        qrShowImage.setOnLongClickListener(this);
        qrChangeServerBtn.setOnTouchListener(this);
//        mAppToolbar.setTitle("扫码功能展示");
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.qr_start_btn:

                openActivity(SimpleCaptureActivity.class);

                break;

            case R.id.qr_start_convert_btn:

                showQrImage();

                break;
            default:
                break;

        }

    }


    private long touchDownTime = 0;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (v.getId() == R.id.qr_change_server_btn) {
                    touchDownTime = System.currentTimeMillis();
                    LogUtils.i("onTouch MotionEvent.ACTION: touchDownTime=" + touchDownTime);
                    return true;
                }
            case MotionEvent.ACTION_UP:
                if (v.getId() == R.id.qr_change_server_btn) {
                    long time = System.currentTimeMillis() - touchDownTime;
                    if (time < 5000 || time > 8000) {
                        ToastUtil.show("请长按5-8秒");
                    } else {
//                        toActivity(ServerSettingActivity.createIntent(context
//                                , SettingUtil.getServerAddress(false), SettingUtil.getServerAddress(true)
//                                , SettingUtil.APP_SETTING, Context.MODE_PRIVATE
//                                , SettingUtil.KEY_SERVER_ADDRESS_NORMAL, SettingUtil.KEY_SERVER_ADDRESS_TEST));
                        return true;
                    }
                }
                break;
            default:
                break;
        }

        return false;
    }


    private void showQrImage() {

        String string = qrStringEt.getText().toString();
        if (string.equals("")) {
            ToastUtil.show("内容为空，请重新输入");

        } else {
            try {
//                        mBitmap = BitmapFactory.decodeResource(getResources(),R.mipmap.ic_launcher);
                //得到二维码图片并且展示
                mBitmap = EncodingHandler.createQRCode(string, UIUtils.dip2px(240), UIUtils.dip2px(240), null);
//                        mBitmap = EncodingHandler.createQRCode(string, 400);
                if (mBitmap != null) {

                    qrShowImage.setImageBitmap(mBitmap);
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


    }


    public void saveImage(Bitmap bmp) {

        Toast.makeText(this, "1111", Toast.LENGTH_SHORT).show();

        File appDir = new File(Environment.getExternalStorageDirectory(), "MyQRCODE");
        if (!appDir.exists()) {
            appDir.mkdir();
        }
        String fileName = System.currentTimeMillis() + ".jpg";
        File file = new File(appDir, fileName);
        try {
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
//            MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), fileName, null);

            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + file.getPath())));
            Toast.makeText(this, "保存成功", Toast.LENGTH_SHORT).show();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onLongClick(View v) {


        if (mBitmap != null) {
            saveImage(mBitmap);
        }

        return false;
    }
}
