package com.neo.productitsms.pro.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.neo.productitsms.R;

/**
 * Created by Administrator on 2018/6/28.
 */

public class CommomActionBar extends Toolbar {

    private TextView mToolbarTitle;
    private TextView mToolbarSubTitle;
    private Toolbar mToolbar;
    private Context context;

    public CommomActionBar(Context context) {
        this(context, null);
    }

    public CommomActionBar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CommomActionBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initToolBar(context);
    }

    private void initToolBar(Context context) {

        View view = LayoutInflater.from(context).inflate(R.layout.layout_toolbar, this, false);

        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);

        mToolbarTitle = (TextView) view.findViewById(R.id.toolbar_title);
        mToolbarSubTitle = (TextView) view.findViewById(R.id.toolbar_subtitle);

        if (mToolbar != null) {
            //将Toolbar显示到界面
            ((AppCompatActivity) context).setSupportActionBar(mToolbar);
        }
        if (mToolbarTitle != null) {
            //getTitle()的值是activity的android:lable属性值
            mToolbarTitle.setText(getTitle());
            //设置默认的标题不显示
            ((AppCompatActivity) context).getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

    }

    /**
     * this Activity of tool bar.
     * 获取头部.
     *
     * @return support.v7.widget.Toolbar.
     */
    public Toolbar getToolbar() {
        return mToolbar;
    }

    /**
     * 版本号小于21的后退按钮图片
     */
    private void showBack() {
        //setNavigationIcon必须在setSupportActionBar(toolbar);方法后面加入
        getToolbar().setNavigationIcon(R.mipmap.icon_back);
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AppCompatActivity) context).onBackPressed();
            }
        });
    }



    /**
     * 获取头部标题的TextView
     *
     * @return
     */
    public TextView getToolbarTitle() {
        return mToolbarTitle;
    }

    /**
     * 获取头部标题的TextView
     *
     * @return
     */
    public TextView getSubTitle() {
        return mToolbarSubTitle;
    }

    /**
     * 设置头部标题
     *
     * @param title
     */
    public void setSubBarTitle(CharSequence title) {
        if (mToolbarSubTitle != null) {
            mToolbarSubTitle.setText(title);
        }
    }

    /**
     * 设置头部标题
     *
     * @param title
     */
    public void setToolBarTitle(CharSequence title) {
        if (mToolbarTitle != null) {
            mToolbarTitle.setText(title);
        } else {
            getToolbar().setTitle(title);
            ((AppCompatActivity) context).setSupportActionBar(getToolbar());
        }
    }


    public void showBackIcon(boolean isShow) {

        /**
         * 判断是否有Toolbar,并默认显示返回按钮
         */
        if (null != getToolbar() && isShow) {
            showBack();
        }

    }


}
