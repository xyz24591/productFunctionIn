package com.neo.productitsms.pro.third.chart.view;

import android.content.Context;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.github.mikephil.charting.charts.LineChart;

/**
 * Created by xyz on 2018/10/9.
 *
 * @author xyz
 */

public class LineChartInViewPager extends LineChart {

    PointF downPoint = new PointF();

    public LineChartInViewPager(Context context) {
        super(context);
    }

    public LineChartInViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LineChartInViewPager(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN:

                downPoint.x = event.getX();
                downPoint.y = event.getY();

                break;

            case MotionEvent.ACTION_MOVE:

                if (getScaleX() > 1 && Math.abs(event.getX() - downPoint.x) > 5) {
                    getParent().requestDisallowInterceptTouchEvent(true);
                }

                break;

            default:
                break;

        }
        return super.onTouchEvent(event);
    }
}
