package com.neo.productitsms.pro.third.chart.entity;

/**
 * Created by Administrator on 2018/10/15.
 */

public class VtDateValueBean {

    /**
     * fValue : -21.7467
     * sYearMonth : 2018-03
     */

    private double fValue;
    private String sYearMonth;

    public double getfValue() {
        return fValue;
    }

    public void setfValue(double fValue) {
        this.fValue = fValue;
    }

    public String getsYearMonth() {
        return sYearMonth;
    }

    public void setsYearMonth(String sYearMonth) {
        this.sYearMonth = sYearMonth;
    }
}
