package com.neo.productitsms.pro.third.chartshow;

import android.support.v7.widget.Toolbar;

import com.allen.library.SuperTextView;
import com.neo.productitsms.R;
import com.neo.productitsms.mvp.presenter.impl.MvpBasePresenter;
import com.neo.productitsms.pro.base.view.activity.BaseActivtiy;
import com.neo.productitsms.pro.third.chartshow.chartui.BarChartShowActivity;
import com.neo.productitsms.pro.third.chartshow.chartui.LinrChartShowActivity;
import com.neo.productitsms.pro.third.chartshow.chartui.PieChartShowActivity;

import butterknife.BindView;

/**
 * Created by Administrator on 2018/11/10.
 */

public class ChartShowActivity extends BaseActivtiy implements SuperTextView.OnSuperTextViewClickListener {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.go_linechart_stv)
    SuperTextView goLinechartStv;
    @BindView(R.id.go_piechart_stv)
    SuperTextView goPiechartStv;
    @BindView(R.id.go_barchart_stv)
    SuperTextView goBarchartStv;

    @Override
    public MvpBasePresenter bindPresenter() {
        return null;
    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_chart_shoe;
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
        goLinechartStv.setOnSuperTextViewClickListener(this);
        goPiechartStv.setOnSuperTextViewClickListener(this);
        goBarchartStv.setOnSuperTextViewClickListener(this);
    }

    @Override
    public void onClickListener(SuperTextView superTextView) {

        switch (superTextView.getId()) {
            case R.id.go_linechart_stv:

                openActivity(LinrChartShowActivity.class);

                break;
            case R.id.go_piechart_stv:

                openActivity(PieChartShowActivity.class);

                break;
            case R.id.go_barchart_stv:

                openActivity(BarChartShowActivity.class);

                break;
            default:
                break;

        }


    }
}
