package com.neo.productitsms.pro.base.view.fragment;

import com.neo.productitsms.pro.base.presenter.BasePresenter;
import com.neo.productitsms.pro.base.view.lce.ResultView;

/**
 * Created by Administrator on 2018/6/26.
 */

public abstract class ResultFragment<P extends BasePresenter, M> extends BaseFragment implements ResultView<M> {

    @Override
    public int bindLayoutId() {
        return 0;
    }


    @Override
    public void bindPresenter() {
    }
}