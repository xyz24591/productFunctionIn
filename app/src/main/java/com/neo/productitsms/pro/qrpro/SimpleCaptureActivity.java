package com.neo.productitsms.pro.qrpro;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.widget.Toast;

import com.neo.productitsms.R;
import com.neo.productitsms.qrcode.CaptureActivity;


public class SimpleCaptureActivity extends CaptureActivity {
    protected Activity mActivity = this;

    private AlertDialog mDialog;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        mActivity = this;
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        isLoginBottom(false);
    }

    @Override
    protected void handleResult(String resultString) {
        if (TextUtils.isEmpty(resultString)) {
            Toast.makeText(mActivity, R.string.scan_failed, Toast.LENGTH_SHORT).show();
            restartPreview();
        } else {
            if (mDialog == null) {
                mDialog = new AlertDialog.Builder(mActivity)
                        .setMessage(resultString)
                        .setPositiveButton("确定", null)
                        .create();
                mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        restartPreview();
                    }
                });
            }
            if (!mDialog.isShowing()) {
                mDialog.setMessage(resultString);
                mDialog.show();
            }
        }
    }
}


//public class SimpleCaptureActivity extends CaptureActivity {
//    protected Activity mActivity = this;
//
//    private AlertDialog mDialog;
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        mActivity = this;
//        super.onCreate(savedInstanceState);
//    }
//
//    @Override
//    protected void handleResult(final String resultString) {
//        if (TextUtils.isEmpty(resultString)) {
//            Toast.makeText(mActivity, io.github.xudaojie.qrcodelib.R.string.scan_failed, Toast.LENGTH_SHORT).show();
//            restartPreview();
//        } else {
//            if (mDialog == null) {
//                mDialog = new AlertDialog.Builder(mActivity)
//                        .setTitle("打开浏览器下载")
//                        .setMessage(resultString)
////                        .setMessage("打开浏览器下载")
//                        .setPositiveButton("确定", null)
//                        .create();
//                mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                    @Override
//                    public void onDismiss(DialogInterface dialog) {
//                        restartPreview();
//
//                        if (!TextUtils.isEmpty(resultString)) {
//                            boolean matches = Patterns.WEB_URL.matcher(resultString).matches();
//                            if (matches) {
//                                openBrowser(SimpleCaptureActivity.this, resultString);
//                            } else {
//                                Toast.makeText(mActivity, "不合法的URL", Toast.LENGTH_SHORT).show();
//                            }
//
//                        }
//
//
//                    }
//                });
//            }
//            if (!mDialog.isShowing()) {
//                mDialog.setMessage(resultString);
//                mDialog.show();
//            }
//        }
//    }
//
//    /**
//     * 调用第三方浏览器打开
//     *
//     * @param context
//     * @param url     要浏览的资源地址
//     */
//    public static void openBrowser(Context context, String url) {
//        final Intent intent = new Intent();
//        intent.setAction(Intent.ACTION_VIEW);
//        intent.setData(Uri.parse(url));
//        // 注意此处的判断intent.resolveActivity()可以返回显示该Intent的Activity对应的组件名
//        // 官方解释 : Name of the component implementing an activity that can display the intent
//        if (intent.resolveActivity(context.getPackageManager()) != null) {
//            final ComponentName componentName = intent.resolveActivity(context.getPackageManager());
//            // 打印Log   ComponentName到底是什么
//            Log.i("调用浏览器下载", "componentName = " + componentName.getClassName());
//            context.startActivity(Intent.createChooser(intent, "请选择浏览器"));
//        } else {
//            Toast.makeText(context.getApplicationContext(), "请下载浏览器", Toast.LENGTH_SHORT).show();
//        }
//    }
//
//
//}
