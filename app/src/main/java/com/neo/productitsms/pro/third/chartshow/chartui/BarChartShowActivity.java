package com.neo.productitsms.pro.third.chartshow.chartui;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.neo.productitsms.R;
import com.neo.productitsms.mvp.presenter.impl.MvpBasePresenter;
import com.neo.productitsms.pro.base.view.activity.BaseActivtiy;
import com.neo.productitsms.pro.third.chartshow.chartentity.BarChartEntity;
import com.neo.productitsms.pro.third.chartshow.entity.MonthItemEntity;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/11/10.
 */

public class BarChartShowActivity extends BaseActivtiy {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.new_the_bar_chart)
    BarChart newTheBarChart;
    @BindView(R.id.new_x_bar_chart)
    BarChart newXBarChart;
    private DecimalFormat mFormat;

    @Override
    public MvpBasePresenter bindPresenter() {
        return null;
    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_barchart_activity;
    }

    @Override
    protected void initData() {
        mFormat = new DecimalFormat("#,###.##");
    }

    @Override
    protected void initView() {
        updataBarChart();
        updataXBarChart();


    }

    /**
     * 柱状图 该在X轴得文字显示在柱状图上
     */
    private void updataBarChart() {
//        mBarChart = (BarChart) mView.findViewById(R.id.new_the_bar_chart);
        List<BarEntry>[] entries = new ArrayList[3];
        final String[] labels = {
                getResources().getString(R.string.actual_value),
                getResources().getString(R.string.budget_value),
                getResources().getString(R.string.yoy_value),};
        int[] cahrtColors = {
                Color.parseColor("#45A2FF"),
                Color.parseColor("#58D4C5"),
                Color.parseColor("#FDB25F")};

        final double[] values = new double[3];
        ArrayList<BarEntry> entries1 = new ArrayList<>();
        float actualValue = 10086;
        float budgetValue = 1001;
        float yoyValue = 12580;
        entries1.add(new BarEntry(0.5f, actualValue));
        entries1.add(new BarEntry(1.3f, budgetValue));
        entries1.add(new BarEntry(2.1f, yoyValue));

        values[0] = actualValue;
        values[1] = budgetValue;
        values[2] = yoyValue;
        entries[0] = entries1;

        if (newTheBarChart.getData() != null) {
            newTheBarChart.getData().clearValues();
        }
        BarChartEntity barChartEntity = new BarChartEntity(newTheBarChart, entries, labels, cahrtColors, Color.parseColor("#999999"), 13f);
        barChartEntity.setBarWidth(0.55f);
        barChartEntity.setDrawValueAboveBar(true);
        newTheBarChart.setPinchZoom(false);
        newTheBarChart.setScaleEnabled(false);

        newTheBarChart.animateY(2000, Easing.EasingOption.EaseInOutQuart);
        barChartEntity.setAxisFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return "";
            }
        }, null);

        newTheBarChart.getLegend().setEnabled(false);

        /**
         * 拼接柱状图上文字，涉及到修改源码
         */
        newTheBarChart.getData().setValueFormatter(new IValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                return labels[entry.getX() == 0.5f ? 0 : (entry.getX() == 1.3f ? 1 : 2)]
                        + "\n"
                        + mFormat.format(values[entry.getX() == 0.5f ? 0 : (entry.getX() == 1.3f ? 1 : 2)]);
            }
        });

        /**
         * 处理当数据都为0，不好看情况
         */
        float yMax = newTheBarChart.getData().getYMax() == 0 ? 100f : newTheBarChart.getData().getYMax();
        float delta = yMax / 5.5f;
        newTheBarChart.getAxisLeft().setAxisMaximum(yMax + delta);

        float yMin = newTheBarChart.getData().getYMin();
        if (yMin == 0) {
            yMin = 0;
        }
        float deltaMin = yMin / 5.0f;
        newTheBarChart.getAxisLeft().setAxisMinimum(yMin - deltaMin);
    }

    /**
     * X轴显示时间段的柱状图
     */
    public void updataXBarChart() {
        final MonthItemEntity entity = new MonthItemEntity();
//        BarChart  newXBarChart = (BarChart) findViewById(R.id.new_x_bar_chart);
        List<BarEntry>[] entries = new ArrayList[1];
        //x轴坐标 控制显示位置，并不会在X轴上显示具体的值
        final float[] xlabels = new float[entity.getMonthDatas().size()];
        int[] color = {Color.parseColor("#7f45a2ff")};
        final String unit = "%";
        //x轴坐标增量
        float delta = 1;

        for (int index = 0, len = entity.getMonthDatas().size(); index < len; index++) {
            xlabels[index] = index * delta + delta;
        }
        entries[0] = entity.getMonthEntries();
        BarChartEntity barChartEntity = new BarChartEntity(newXBarChart, entries, null, color, Color.parseColor("#999999"), 10f);
        /*属性修改设置*/
        barChartEntity.setBarWidth(0.55f);
        barChartEntity.setDrawValueAboveBar(true);
        newXBarChart.setPinchZoom(false);
        newXBarChart.setScaleEnabled(false);
        newXBarChart.getLegend().setEnabled(false);
        newXBarChart.getXAxis().setTextSize(9f);
        newXBarChart.getAxisLeft().setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
//        mBarChart.animateY(1000, Easing.EasingOption.EaseInOutQuart);
        newTheBarChart.animateY(1000, Easing.EasingOption.EaseInOutQuart);
        /*X,Y坐标显示设置*/
        barChartEntity.setAxisFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                for (int index = 0, len = xlabels.length; index < len; index++) {
                    if (value == xlabels[index]) {
                        return entity.getMonthDatas().get(index).getBeginTime() + "-" + entity.getMonthDatas().get(index).getEndTime();
                    }
                }
                return "";
            }
        }, new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return mFormat.format(value) + (unit.equals("%") ? unit : "");
            }
        });

        /*值显示设置*/
        newXBarChart.getData().setValueFormatter(new IValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                return mFormat.format(value) + (unit.equals("%") ? unit : "");
            }
        });

        /*Y轴最大值最小值设置，为了尽可能的显示完整数据*/
        float yMax = newXBarChart.getData().getYMax() == 0 ? 100f : newXBarChart.getData().getYMax();
        float yDelta = yMax / 5.5f;
        float axisMaximum = yMax + yDelta;
        if (axisMaximum < 5) {
            axisMaximum = 5;
        }
        newXBarChart.getAxisLeft().setAxisMaximum(axisMaximum);
        float yMin = newXBarChart.getData().getYMin();
        float deltaMin = yMin / 5.0f;
        newXBarChart.getAxisLeft().setAxisMinimum(yMin - deltaMin);
        /**
         * 设置X轴文字顺时针旋转角度
         */
        newXBarChart.getXAxis().setLabelRotationAngle(-60);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
