package com.neo.productitsms.pro.login.view;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.URLSpan;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.neo.productitsms.R;
import com.neo.productitsms.common.UIActivity;
import com.neo.productitsms.config.ConstantParams;
import com.neo.productitsms.mvp.presenter.impl.MvpBasePresenter;
import com.neo.productitsms.pro.base.view.activity.BaseActivtiy;
import com.neo.productitsms.pro.login.presenter.RegisterPresenter;
import com.neo.productitsms.pro.widget.TextViewIcon;
import com.neo.productitsms.pro.widget.ToolbarHelper;
import com.neo.productitsms.pro.widget.dialogtip.UIAlertDialog;
import com.neo.productitsms.utils.CommonUtil;
import com.neo.productitsms.utils.DialogUtils;
import com.neo.productitsms.utils.LogUtils;
import com.neo.productitsms.utils.TimeCountUtil;
import com.neo.productitsms.utils.ToastUtil;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Administrator on 2018/7/5.
 */

public class RegisterActivity extends BaseActivtiy implements View.OnClickListener {

    RegisterPresenter registerPresenter;

    @BindView(R.id.register_phone_prefix)
    TextView registerPhonePrefix;

    @BindView(R.id.register_protocol)
    TextView registerProtocol;
    @BindView(R.id.register_phone)
    EditText registerPhone;
    @BindView(R.id.register_clear_tvi)
    TextViewIcon registerClearTvi;
    @BindView(R.id.register_phone_getcode_btn)
    Button registerPhoneGetcodeBtn;
    @BindView(R.id.register_code_et)
    EditText registerCodeEt;
    @BindView(R.id.iv_clean_iv)
    ImageView ivCleanIv;
    @BindView(R.id.register_ok_btn)
    Button registerOkBtn;
    @BindView(R.id.register_agree_prot_cb)
    CheckBox registerAgreeCb;

    private TimeCountUtil mTimeCountUtil;

    public static void actionStart(Context context) {
        Intent intent = new Intent(context, RegisterActivity.class);
        context.startActivity(intent);
    }

    @Override
    public MvpBasePresenter bindPresenter() {
        registerPresenter = new RegisterPresenter(this);
        return registerPresenter;
    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_register;
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
        mTimeCountUtil = new TimeCountUtil(ConstantParams.REG_TOTAL_LONG, ConstantParams.REG_INTERVAL, registerPhoneGetcodeBtn);
//        initTitleBar();
        initProtocal();
        initListener();
        initEditText();
    }

    private void initEditText() {

        registerPhone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    String regsterPhoneStr = registerPhone.getText().toString().trim();

                    if (!TextUtils.isEmpty(regsterPhoneStr) && registerClearTvi.getVisibility() == View.GONE) {

                        registerClearTvi.setVisibility(View.VISIBLE);

                        registerPhone.setSelection(regsterPhoneStr.length());
                    } else if (TextUtils.isEmpty(regsterPhoneStr)){

                        registerClearTvi.setVisibility(View.GONE);

                    }


                }

            }
        });

        RxTextView.textChanges(registerPhone)
                .debounce(600, TimeUnit.MILLISECONDS)
                .skip(1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<CharSequence>() {
                    @Override
                    public void accept(CharSequence charSequence) throws Exception {

                        if (!TextUtils.isEmpty(charSequence)) {

                            registerClearTvi.setVisibility(View.VISIBLE);

                        } else {
                            registerClearTvi.setVisibility(View.GONE);

                        }

                    }
                });


    }


    private void initListener() {

        registerPhoneGetcodeBtn.setOnClickListener(this);
        registerClearTvi.setOnClickListener(this);

    }

    private void initProtocal() {

        final SpannableStringBuilder builder = new SpannableStringBuilder("我已阅读,并同意《用户注册协议》");
        builder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Toast.makeText(RegisterActivity.this, "阅读", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);


            }
        }, 8, 16, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        builder.setSpan(new UnderlineSpan(), 8, 16, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);  //下划线
        //字体颜色
        builder.setSpan(new ForegroundColorSpan(Color.BLUE), 8, 16, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        registerProtocol.setMovementMethod(LinkMovementMethod.getInstance());
        registerProtocol.append(builder);


    }

//    private void initTitleBar() {
//
//        Toolbar toolbar = findViewById(R.id.regist_toolbar);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        toolbar.setNavigationIcon(R.mipmap.icon_back);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                finish();
//                onBackPressed();
//            }
//        });
////        setTitle("注册");
//        ToolbarHelper.addMiddleTitle(this,"注册",toolbar);
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.register_phone_getcode_btn:

                String regPhone = registerPhone.getText().toString();

                if (TextUtils.isEmpty(regPhone)) {

                    DialogUtils.getInstance().showInfo("请输入手机号");

                    return;
                }

                boolean mobileNumber = CommonUtil.isMobileNumber(regPhone);
                if (!mobileNumber) {
                    DialogUtils.getInstance().showInfo("请输入正确的手机号");
                    return;
                }

                getCodeByPhoneNum(regPhone);

                break;
            case R.id.register_clear_tvi:

                registerPhone.setText("");

                break;


            default:
                break;

        }
    }

    private void getCodeByPhoneNum(String regPhone) {
        mTimeCountUtil.start();

//        mTimeCountUtil.onFinish();

    }
}
