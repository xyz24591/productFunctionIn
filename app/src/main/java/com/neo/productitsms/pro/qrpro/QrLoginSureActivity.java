package com.neo.productitsms.pro.qrpro;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.neo.productitsms.R;
import com.neo.productitsms.bean.UserRespBean;
import com.neo.productitsms.config.ConstantParams;
import com.neo.productitsms.http.framework.impl.RequestManger;
import com.neo.productitsms.mvp.presenter.impl.MvpBasePresenter;
import com.neo.productitsms.pro.base.view.activity.BaseActivtiy;
import com.neo.productitsms.utils.GetUserInfoUtils;
import com.neo.productitsms.utils.GsonUtil;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Administrator on 2018/11/6.
 */

public class QrLoginSureActivity extends BaseActivtiy implements View.OnClickListener {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.login_sure_btn)
    Button loginSureBtn;
    @BindView(R.id.login_cancle_btn)
    Button loginCancleBtn;

    @Override
    public MvpBasePresenter bindPresenter() {
        return null;
    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_qr_sure_layout;
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
        setTitle("扫码登录");
        loginSureBtn.setOnClickListener(this);
        loginCancleBtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case R.id.login_sure_btn:

                doLogin(true);

                break;

            case R.id.login_cancle_btn:
                doLogin(false);
                break;

            default:
                break;

        }

    }

    private void doLogin(boolean isOk) {

        Call<String> call = null;
        UserRespBean.BringBean userInfo = GetUserInfoUtils.getInstance(QrLoginSureActivity.this).getUserInfo();

        if (userInfo == null) {
            Toast.makeText(this, "异常", Toast.LENGTH_SHORT).show();
            return;
        }

        if (isOk) {
            call = RequestManger.getInstance().getStringService().scanCodeConfirm(ConstantParams.QR_UID, userInfo.getUserId());

        } else {
            call = RequestManger.getInstance().getStringService().scanCodeCancel(ConstantParams.QR_UID);
        }

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                String body = response.body();

                QrLoginRespBean loginRespBean = GsonUtil.GsonToBean(body, QrLoginRespBean.class);
                boolean res = loginRespBean.isRes();
                if (res) {
                    Toast.makeText(QrLoginSureActivity.this, loginRespBean.getMessage(), Toast.LENGTH_SHORT).show();
                    onBackPressed();
                } else {

                    Toast.makeText(QrLoginSureActivity.this, loginRespBean.getMessage(), Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

                Toast.makeText(QrLoginSureActivity.this, "异常", Toast.LENGTH_SHORT).show();
                onBackPressed();
            }
        });

    }
}
