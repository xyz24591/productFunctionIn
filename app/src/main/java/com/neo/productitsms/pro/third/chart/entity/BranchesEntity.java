package com.neo.productitsms.pro.third.chart.entity;

import com.chad.library.adapter.base.entity.AbstractExpandableItem;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.neo.productitsms.pro.third.chart.adapter.ExpandableItemAdapter;

/**
 * Created by Administrator on 2018/10/11.
 */

public class BranchesEntity extends AbstractExpandableItem<ChartEntity> implements MultiItemEntity {

    private String title;

    public BranchesEntity() {
    }

    public BranchesEntity(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int getLevel() {
        return 0;
    }

    @Override
    public int getItemType() {
        return ExpandableItemAdapter.TYPE_LEVEL_0;
    }
}
