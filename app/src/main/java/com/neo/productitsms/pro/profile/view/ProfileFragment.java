package com.neo.productitsms.pro.profile.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.allen.library.SuperTextView;
import com.neo.productitsms.R;
import com.neo.productitsms.calendarui.uical.activity.MainActivity;
import com.neo.productitsms.common.UIActivity;
import com.neo.productitsms.common.WebViewActivity;
import com.neo.productitsms.common.appermission.PermissionReqActivity;
import com.neo.productitsms.common.recviewpager.MainViewPagerActivity;
import com.neo.productitsms.common.update.UpdateActivity;
import com.neo.productitsms.pro.base.view.fragment.BaseFragment;
import com.neo.productitsms.pro.home.widget.ui.HomeMenuActivity;
import com.neo.productitsms.pro.login.view.LoginActivity;
import com.neo.productitsms.pro.login.view.RegisterActivity;
import com.neo.productitsms.pro.qrpro.QrShowActivity;
import com.neo.productitsms.pro.third.chart.ChartActivity;
import com.neo.productitsms.pro.third.chart.LineChartActivity;
import com.neo.productitsms.pro.third.chartshow.ChartShowActivity;
import com.neo.productitsms.pro.third.jpush.JMainAcivity;
import com.neo.productitsms.pro.third.share.ThirdShareActivity;
import com.neo.productitsms.pro.ui.AboutActivity;
import com.neo.productitsms.pro.ui.AccountSettingActivity;
import com.neo.productitsms.pro.ui.FeedBackActivity;
import com.neo.productitsms.pro.ui.NoticeSettingActivity;
import com.neo.productitsms.pro.ui.SafeSettingActivity;
import com.neo.productitsms.pro.widget.marquee.MarqueeActivity;
import com.neo.productitsms.timepicker.MainTimePickActivity;
import com.neo.productitsms.utils.LogUtils;
import com.neo.productitsms.utils.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Administrator on 2018/6/27.
 */

public class ProfileFragment extends BaseFragment implements SuperTextView.OnSuperTextViewClickListener, View.OnClickListener {
    @BindView(R.id.go_web)
    SuperTextView goWeb;
    @BindView(R.id.go_qr_stv)
    SuperTextView goQrStv;
    @BindView(R.id.go_reg_stv)
    SuperTextView goRegStv;
    @BindView(R.id.go_ui_stv)
    SuperTextView goUIStv;
    @BindView(R.id.go_login_stv)
    SuperTextView goLoginStv;
    @BindView(R.id.go_permission_stv)
    SuperTextView goPermissionStv;
    @BindView(R.id.go_viewpager_stv)
    SuperTextView goViewPagerStv;
    @BindView(R.id.go_jpush_stv)
    SuperTextView goJpushStv;
    @BindView(R.id.go_chart_stv)
    SuperTextView goChartStv;
    @BindView(R.id.go_share_stv)
    SuperTextView goShareStv;
    @BindView(R.id.go_marqueen_stv)
    SuperTextView goMarqueenStv;
    @BindView(R.id.go_homemenu_stv)
    SuperTextView goHomemenuStv;
    @BindView(R.id.go_update_stv)
    SuperTextView goUpdateStv;
    @BindView(R.id.notify_text)
    TextView notifyText;
    @BindView(R.id.profile_top_bar)
    FrameLayout profileTopBar;
    @BindView(R.id.profile_hurry_fl)
    FrameLayout profileHurryFl;
    @BindView(R.id.profile_safe_note_stv)
    SuperTextView profileSafeNoteStv;
    @BindView(R.id.profile_notice_setting_stv)
    SuperTextView profileNoticeSettingStv;
    @BindView(R.id.profile_cache_clear_stv)
    SuperTextView profileCacheClearStv;
    @BindView(R.id.profile_feeding_back_stv)
    SuperTextView profileFeedingBackStv;
    @BindView(R.id.profile_about_stv)
    SuperTextView profileAboutStv;
    @BindView(R.id.go_timepick_stv)
    SuperTextView goTimepickStv;
    @BindView(R.id.go_calendar_stv)
    SuperTextView goClandarStv;

    @BindView(R.id.go_show_stv)
    SuperTextView goShowStv;


    @BindView(R.id.profile_exit_btn)
    Button profileExitBtn;
    @BindView(R.id.profile_acc_setting_ll)
    LinearLayout profileAccSettingLl;

//888888888888888888888888888888888888888888888888888

    @Override
    public int bindLayoutId() {
        return R.layout.fragment_profile;
    }

    @Override
    public void initContentView(View contentView) {


    }

    @Override
    protected void initData() {

        profileSafeNoteStv.setOnSuperTextViewClickListener(this);
        profileNoticeSettingStv.setOnSuperTextViewClickListener(this);
        profileCacheClearStv.setOnSuperTextViewClickListener(this);
        profileFeedingBackStv.setOnSuperTextViewClickListener(this);
        profileAboutStv.setOnSuperTextViewClickListener(this);
        profileExitBtn.setOnClickListener(this);
        profileAccSettingLl.setOnClickListener(this);

        /***************分界线**********************/
        goWeb.setOnSuperTextViewClickListener(this);
        goQrStv.setOnSuperTextViewClickListener(this);
        goRegStv.setOnSuperTextViewClickListener(this);
        goUIStv.setOnSuperTextViewClickListener(this);
        goLoginStv.setOnSuperTextViewClickListener(this);
        goPermissionStv.setOnSuperTextViewClickListener(this);
        goPermissionStv.setOnSuperTextViewClickListener(this);
        goViewPagerStv.setOnSuperTextViewClickListener(this);
        goJpushStv.setOnSuperTextViewClickListener(this);
        goChartStv.setOnSuperTextViewClickListener(this);
        goShareStv.setOnSuperTextViewClickListener(this);
        goMarqueenStv.setOnSuperTextViewClickListener(this);
        goHomemenuStv.setOnSuperTextViewClickListener(this);
        goUpdateStv.setOnSuperTextViewClickListener(this);
        goTimepickStv.setOnSuperTextViewClickListener(this);
        goClandarStv.setOnSuperTextViewClickListener(this);
        goShowStv.setOnSuperTextViewClickListener(this);
    }


    private void jumpToActivity(Class<? extends Activity> activity) {

        Intent intent = new Intent(getContext(), activity);

        startActivity(intent);
    }

    @Override
    public void onClickListener(SuperTextView superTextView) {

        switch (superTextView.getId()) {

            case R.id.profile_safe_note_stv:

                jumpToActivity(SafeSettingActivity.class);
                break;
            case R.id.profile_notice_setting_stv:

                jumpToActivity(NoticeSettingActivity.class);
                break;
            case R.id.profile_cache_clear_stv:

                LogUtils.i("--清理缓存--");
                break;
            case R.id.profile_feeding_back_stv:

                jumpToActivity(FeedBackActivity.class);
                break;
            case R.id.profile_about_stv:

                jumpToActivity(AboutActivity.class);
                break;


            /***************分界线**********************/

            case R.id.go_web:

                WebViewActivity.goWeb(getContext(), null);

                break;

            case R.id.go_qr_stv:

                QrShowActivity.actionStart(getContext());
                break;

            case R.id.go_reg_stv:

                RegisterActivity.actionStart(getContext());

                break;
            case R.id.go_ui_stv:
                UIActivity.actionStart(getContext());
                break;

            case R.id.go_login_stv:

                startActivity(new Intent(getContext(), LoginActivity.class));
                break;
            case R.id.go_permission_stv:

                startActivity(new Intent(getContext(), PermissionReqActivity.class));
                break;
            case R.id.go_viewpager_stv:

                startActivity(new Intent(getContext(), MainViewPagerActivity.class));
                break;
            case R.id.go_jpush_stv:

                startActivity(new Intent(getContext(), JMainAcivity.class));
                break;
            case R.id.go_chart_stv:

//                startActivity(new Intent(getContext(), ChartActivity.class));
                startActivity(new Intent(getContext(), ChartShowActivity.class));
                break;
            case R.id.go_show_stv:
                startActivity(new Intent(getContext(), LineChartActivity.class));
                break;
            case R.id.go_share_stv:

                startActivity(new Intent(getContext(), ThirdShareActivity.class));
                break;
            case R.id.go_marqueen_stv:

                startActivity(new Intent(getContext(), MarqueeActivity.class));
                break;
            case R.id.go_homemenu_stv:

                startActivity(new Intent(getContext(), HomeMenuActivity.class));
                break;
            case R.id.go_update_stv:

                startActivity(new Intent(getContext(), UpdateActivity.class));
                break;
            case R.id.go_timepick_stv:

                startActivity(new Intent(getContext(), MainTimePickActivity.class));
                break;
            case R.id.go_calendar_stv:

                startActivity(new Intent(getContext(), MainActivity.class));
                break;

            default:
                break;

        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.profile_exit_btn:
                ToastUtil.showShort("退出");

                break;
            case R.id.profile_acc_setting_ll:
                jumpToActivity(AccountSettingActivity.class);

                break;

            default:
                break;
        }


    }
}
