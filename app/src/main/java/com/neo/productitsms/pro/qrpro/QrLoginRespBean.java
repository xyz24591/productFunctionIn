package com.neo.productitsms.pro.qrpro;

import java.io.Serializable;

/**
 * Created by Administrator on 2018/11/6.
 */

public class QrLoginRespBean implements Serializable {
    /**
     * res : true
     * code : “10001
     * message : ”二维码已失效”
     */

    private boolean res;
    private String code;
    private String message;

    public boolean isRes() {
        return res;
    }

    public void setRes(boolean res) {
        this.res = res;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
