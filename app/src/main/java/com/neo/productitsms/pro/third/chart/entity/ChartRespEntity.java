package com.neo.productitsms.pro.third.chart.entity;


import java.util.List;

/**
 * Created by Administrator on 2018/10/9.
 */

public class ChartRespEntity {

    private String purl;// 服务端用
    private boolean res; // 是否成功
    private String code; // 服务端result类中返回的编码
    private String message; // 返回信息

    private List<ChartEntity> bring; //返回的数据值


    public String getPurl() {
        return purl;
    }

    public void setPurl(String purl) {
        this.purl = purl;
    }

    public boolean isRes() {
        return res;
    }

    public void setRes(boolean res) {
        this.res = res;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ChartEntity> getBring() {
        return bring;
    }

    public void setBring(List<ChartEntity> bring) {
        this.bring = bring;
    }
}
