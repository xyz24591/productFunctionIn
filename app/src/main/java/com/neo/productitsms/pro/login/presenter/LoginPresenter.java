package com.neo.productitsms.pro.login.presenter;

import android.content.Context;

import com.neo.productitsms.bean.UserBean;
import com.neo.productitsms.bean.UserRespBean;
import com.neo.productitsms.http.framework.RequestCallBack;
import com.neo.productitsms.http.framework.UIThreadCallBack;
import com.neo.productitsms.pro.base.presenter.BasePresenter;
import com.neo.productitsms.pro.login.model.LoginModel;
import com.neo.productitsms.utils.LogUtils;

import retrofit2.Response;

/**
 * Created by Administrator on 2018/6/29.
 */

public class LoginPresenter extends BasePresenter<LoginModel> {


    public LoginPresenter(Context context) {
        super(context);
    }

    @Override
    public LoginModel bindModel() {
        return new LoginModel(getContext());
    }

    public void login(UserBean userBean, final UIThreadCallBack<UserRespBean> callBack) {

        LogUtils.i("loginPresenter 登录...");

        getModel().login(userBean, new RequestCallBack() {
            @Override
            public void onResponse(Response response) {
                callBack.onResponse(response);
            }

            @Override
            public void onResFalse(String message) {
                callBack.onResFalse(message);
            }

            @Override
            public void onFail(String errorMessage) {
                callBack.onFail(errorMessage);
            }
        });

    }

}
