package com.neo.productitsms.pro.third.chart;

import android.os.Bundle;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.neo.productitsms.R;
import com.neo.productitsms.mvp.presenter.impl.MvpBasePresenter;
import com.neo.productitsms.pro.base.view.activity.BaseActivtiy;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/7/21.
 */

public class ChartActivity extends BaseActivtiy {
    @BindView(R.id.line_chart)
    LineChart lineChart;

    @Override
    public MvpBasePresenter bindPresenter() {
        return null;
    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_chart;
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
        initLineChart();

    }

    private void initLineChart() {
//    显示边界
        lineChart.setDrawBorders(true);

//        lineChart.setBackgroundColor();

//        Description description=new Description();
//        description.setPosition();
//        description.set
//        lineChart.setDescription();

//        lineChart.setNoDataText();

//        lineChart.setDrawGridBackground();

//        lineChart.setDrawBorders(z);

//        设置数据
        //设置数据
        List<Entry> entries = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            entries.add(new Entry(i, (float) (Math.random()) * 80));
        }


//        Highlight highlight=new Highlight();
//        highlight.
//一个LineDataSet就是一条线

        LineDataSet lineDataSet = new LineDataSet(entries, "温度");
        LineData data = new LineData(lineDataSet);
        lineChart.setData(data);
/***************************************************************/

//        一.XAxis(X轴)
//        1.得到X轴：
        XAxis xAxis = lineChart.getXAxis();
//        2.设置X轴的位置（默认在上方）：//值：BOTTOM,BOTH_SIDED,BOTTOM_INSIDE,TOP,TOP_INSIDE
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//        3.设置X轴坐标之间的最小间隔（因为此图有缩放功能，X轴,Y轴可设置可缩放）
        xAxis.setGranularity(1f);
//        4.设置X轴的刻度数量
        xAxis.setLabelCount(12, true);


    }

}
