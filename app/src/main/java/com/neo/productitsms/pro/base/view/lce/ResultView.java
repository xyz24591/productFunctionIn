package com.neo.productitsms.pro.base.view.lce;

import com.neo.productitsms.mvp.view.MvpView;

/**
 * Created by Administrator on 2018/6/26.
 */

public interface ResultView<M> extends MvpView {

    void onResult(M data, String errorMessage);
}