package com.neo.productitsms.pro.widget;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.widget.TextView;

/**
 * Created by Administrator on 2018/7/11.
 */

public class ToolbarHelper {
    static TextView textView;
    static Toolbar mToolbar;

    public static void addMiddleTitle(Context context, CharSequence title, Toolbar toolbar) {

        if (!TextUtils.isEmpty(title)) {
            if (textView == null) {
                mToolbar = toolbar;
                textView = new TextView(context);
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(16);
                Toolbar.LayoutParams params = new Toolbar.LayoutParams(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.WRAP_CONTENT);
                params.gravity = Gravity.CENTER_HORIZONTAL;
                mToolbar.addView(textView, params);
            }
            textView.setText(title);

        }


    }

    public static void dettachMiddleTitle() {
        textView = null;
        mToolbar = null;
    }


}
