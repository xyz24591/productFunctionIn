package com.neo.productitsms.pro.base.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.neo.productitsms.config.AppLoginConfig;
import com.neo.productitsms.mvp.view.impl.MvpMapFragment;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Administrator on 2018/6/26.
 */

public abstract class BaseFragment extends MvpMapFragment {

    //我们自己的Fragment需要缓存视图
    private View contentView;//缓存视图
    private boolean isInit;
    private Unbinder bind;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (contentView == null) {
            contentView = inflater.inflate(bindLayoutId(), container, false);
            initContentView(contentView);

        }
// 判断Fragment对应的Activity是否存在这个视图

        ViewGroup parent = (ViewGroup) contentView.getParent();

        if (parent != null) {
//            如果存在 那么我们就干掉 重新添加 这样的方式我们就可以缓存视图
            parent.removeView(contentView);
        }
         bind = ButterKnife.bind(this, contentView);

        return contentView;
    }

    public View getContentView() {
        return contentView;
    }

    protected void resetContentView(View contentView) {
        ViewGroup viewGroup = (ViewGroup) contentView;
        viewGroup.removeAllViews();
    }


    protected void loadLayout(int layoutId, View v) {

        ViewGroup viewGroup = (ViewGroup) v;
        View view = LayoutInflater.from(getContext()).inflate(layoutId, viewGroup, false);
        //判断Fragment对应的Activity是否存在这个视图

        ViewGroup parent = (ViewGroup) view.getParent();
        if (parent != null) {
            //如果存在,那么我就干掉,重写添加,这样的方式我们就可以缓存视图
            parent.removeView(view);
        }
        viewGroup.addView(view);

    }


    public boolean isLogin() {

        return AppLoginConfig.getInstance().getBoolean(getContext(), AppLoginConfig.IS_LOGIN);
    }

    public void setLogin(boolean isLogin) {
        AppLoginConfig.getInstance().putBoolean(getContext(), AppLoginConfig.IS_LOGIN, isLogin);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!isInit) {
            this.isInit = true;
            initData();
        }

    }

    @Override
    public void bindPresenter() {

    }

    protected void initData() {

    }

    public abstract int bindLayoutId();

    public abstract void initContentView(View contentView);

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        bind.unbind();
    }
}
