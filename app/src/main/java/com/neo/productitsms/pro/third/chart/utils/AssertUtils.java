package com.neo.productitsms.pro.third.chart.utils;

import android.content.Context;
import android.content.res.AssetManager;

import com.neo.productitsms.utils.UIUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Administrator on 2018/10/15.
 */

public class AssertUtils {

//    public static String getAssertJson(Context context, String fileName) {
//
//        AssetManager assets = context.getResources().getAssets();
//
//        StringBuilder builder = new StringBuilder();
//
//        try {
//            InputStream open = assets.open(fileName);
//
//            InputStreamReader streamReader = new InputStreamReader(open, "UTF-8");
//
//            BufferedReader reader = new BufferedReader(streamReader);
//
//            String line = "";
//
//            if ((line = reader.readLine()) != null) {
//                builder.append(line);
//            }
//
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        return builder.toString();
//    }
//
//
//    public static String getAsJson(Context context, String fileName) {
//
//        AssetManager assets = context.getResources().getAssets();
//        StringBuilder builder = new StringBuilder();
//        try {
//            InputStream is = assets.open(fileName);
//
//            InputStreamReader streamReader = new InputStreamReader(is, "UTF-8");
//
//            BufferedReader reader = new BufferedReader(streamReader);
//
//
//            String line;
//
//            if ((line = reader.readLine()) != null) {
//
//                builder.append(line);
//
//            }
//
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        return builder.toString();
//    }


    /**
     * 得到json文件中的内容
     *
     * @param context
     * @param fileName
     * @return
     */
    public static String getJson(Context context, String fileName) {
        StringBuilder stringBuilder = new StringBuilder();
        //获得assets资源管理器
        AssetManager assetManager = context.getAssets();
        //使用IO流读取json文件内容
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(assetManager.open(fileName), "utf-8"));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }


}
