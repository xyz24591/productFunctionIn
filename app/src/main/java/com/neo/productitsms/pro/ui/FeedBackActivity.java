package com.neo.productitsms.pro.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.neo.productitsms.R;
import com.neo.productitsms.config.ConstantParams;
import com.neo.productitsms.mvp.presenter.impl.MvpBasePresenter;
import com.neo.productitsms.pro.base.view.activity.BaseActivtiy;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Administrator on 2018/8/2.
 */

public class FeedBackActivity extends BaseActivtiy implements View.OnClickListener {
    @BindView(R.id.feed_note_info)
    EditText feedNoteInfo;
    @BindView(R.id.feed_remain_text)
    TextView feedRemainText;
    @BindView(R.id.feed_info_rl)
    RelativeLayout feedInfoRl;
    @BindView(R.id.feed_submit_btn)
    Button feedSubmitBtn;

    private int maxInput = 100;

    @Override
    public MvpBasePresenter bindPresenter() {
        return null;
    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_feedback;
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
        feedSubmitBtn.setOnClickListener(this);
        feedRemainText.setText("剩余" + maxInput + "字");

        RxTextView.textChanges(feedNoteInfo)
                .debounce(600, TimeUnit.MILLISECONDS)
                .skip(1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<CharSequence>() {
                    @Override
                    public void accept(CharSequence charSequence) throws Exception {
                        feedRemainText.setText("剩余" + (maxInput - charSequence.toString().length()) + "字");

                    }
                });


    }

    @Override
    public void onClick(View v) {

        String ifeedBackInput = feedNoteInfo.getText().toString().trim();


    }
}
