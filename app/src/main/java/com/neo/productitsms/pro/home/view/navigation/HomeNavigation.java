package com.neo.productitsms.pro.home.view.navigation;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.neo.productitsms.R;
import com.neo.productitsms.pro.base.view.navigation.impl.AbsNavigation;

/**
 * Created by Administrator on 2018/6/27.
 */

public class HomeNavigation extends AbsNavigation<HomeNavigation.Builder.HomeNavigationParams> {

    protected HomeNavigation(HomeNavigation.Builder.HomeNavigationParams params) {
        super(params);
    }

    @Override
    public int getLayoutId() {
        return R.layout.navigation_home;
    }

    @Override
    public void createAndBind() {
        super.createAndBind();
        setButton(R.id.bt_left, getParams().leftTextColor, getParams().leftImageRes, getParams().leftText, getParams().leftOnClickListener);
        setTextView(R.id.tv_center, getParams().centerTextColor, getParams().centerText, getParams().centerOnClickListener);
        setImageView(R.id.iv_arrow, getParams().centerImageRes);
        setButton(R.id.bt_right, getParams().rightTextColor, getParams().rightImageRes, getParams().rightText, getParams().rightOnClickListener);
    }

    protected ImageView setImageView(int viewId, int imageRes) {
        View view = findViewById(viewId);
        if (view == null && !(view instanceof ImageView)) {
            return null;
        }
        ImageView imageView = (ImageView) view;
        if (imageRes == 0) {
            imageView.setVisibility(View.GONE);
        } else {
            imageView.setImageResource(imageRes);
            imageView.setVisibility(View.VISIBLE);
        }
        return imageView;
    }

    protected void setTextView(int viewId, int textColor, String text, final OnCheckedListener onCheckedListener) {
        View view = findViewById(viewId);
        if (view == null && !(view instanceof TextView)) {
            return;
        }
        TextView textView = (TextView) view;
        if (TextUtils.isEmpty(text)) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setVisibility(View.VISIBLE);
            textView.setText(text);
            if (getParams().centerImageRes != 0 && getParams().centerImageResPress != 0) {
                textView.setOnClickListener(new View.OnClickListener() {

                    private boolean isChecked;
                    @Override
                    public void onClick(View v) {
                        if (isChecked){
                            isChecked = false;
                            setImageView(R.id.iv_arrow,getParams().centerImageResPress);
                        }else {
                            setImageView(R.id.iv_arrow,getParams().centerImageRes);
                            isChecked = true;
                        }
                        onCheckedListener.onChecked(v,false);
                    }
                });
            }
            if (textColor != 0) {
                textView.setTextColor(getColor(textColor));
            }
        }
    }

    protected void setButton(int viewId, int textColor, int imageRes, String text, View.OnClickListener onClickListener) {
        View view = findViewById(viewId);
        if (view == null && !(view instanceof Button)) {
            return;
        }
        Button button = (Button) view;
        if (TextUtils.isEmpty(text) && imageRes == 0) {
            button.setVisibility(View.GONE);
        } else {
            button.setVisibility(View.VISIBLE);
            if (TextUtils.isEmpty(text)) {
                ViewGroup.LayoutParams layoutParams = button.getLayoutParams();
                layoutParams.width = layoutParams.height;
                button.setBackgroundResource(imageRes);
                button.setText(null);
            } else {
                ViewGroup.LayoutParams layoutParams = button.getLayoutParams();
                layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
                layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                button.setText(text);
                button.setBackground(null);
            }

            button.setOnClickListener(onClickListener);

            if (textColor != 0) {
                button.setTextColor(getColor(textColor));
            }
        }

    }

    public interface OnCheckedListener{
        public void onChecked(View v,boolean isChecked);
    }

    public static class Builder extends AbsNavigation.Builder {

        protected HomeNavigationParams navigationParams;

        public Builder(Context context, ViewGroup parent) {
            navigationParams = bindNavigationParams(context, parent);
        }

        protected HomeNavigationParams bindNavigationParams(Context context, ViewGroup parent) {
            return new HomeNavigationParams(context, parent);
        }

        public Builder setLeftText(String leftText) {
            navigationParams.leftText = leftText;
            return this;
        }

        public Builder setCenterText(String centerText) {
            navigationParams.centerText = centerText;
            return this;
        }

        public Builder setRightText(String rightText) {
            navigationParams.rightText = rightText;
            return this;
        }

        public Builder setLeftText(int leftText) {
            navigationParams.leftText = navigationParams.context.getResources().getString(leftText);
            return this;
        }

        public Builder setCenterText(int centerText) {
            navigationParams.centerText = navigationParams.context.getResources().getString(centerText);
            return this;
        }

        public Builder setRightText(int rightText) {
            navigationParams.rightText = navigationParams.context.getResources().getString(rightText);
            return this;
        }

        public Builder setRightTextColor(int rightTextColor) {
            navigationParams.rightTextColor = rightTextColor;
            return this;
        }

        public Builder setCenterTextColor(int centerTextColor) {
            navigationParams.centerTextColor = centerTextColor;
            return this;
        }

        public Builder setLeftTextColor(int leftTextColor) {
            navigationParams.leftTextColor = leftTextColor;
            return this;
        }

        public Builder setLeftImageRes(int leftImageRes) {
            navigationParams.leftImageRes = leftImageRes;
            return this;
        }

        public Builder setCenterImage(int centerImageRes, int centerImageResPress) {
            navigationParams.centerImageRes = centerImageRes;
            navigationParams.centerImageResPress = centerImageResPress;
            return this;
        }

        public Builder setRightImageRes(int rightImageRes) {
            navigationParams.rightImageRes = rightImageRes;
            return this;
        }

        public Builder setLeftOnClickListener(View.OnClickListener leftOnClickListener) {
            navigationParams.leftOnClickListener = leftOnClickListener;
            return this;
        }

        public Builder setCenterOnCheckedListener(OnCheckedListener onCheckedListener) {
            navigationParams.centerOnClickListener = onCheckedListener;
            return this;
        }

        public Builder setRightOnClickListener(View.OnClickListener rightOnClickListener) {
            navigationParams.rightOnClickListener = rightOnClickListener;
            return this;
        }

        @Override
        public HomeNavigation create() {
            HomeNavigation defaultNavigation = new HomeNavigation(navigationParams);
            return defaultNavigation;
        }

        public static class HomeNavigationParams extends AbsNavigation.Builder.NavigationParams {
            public String leftText;
            public String centerText;
            public String rightText;
            public int leftImageRes;
            public int rightImageRes;
            public int centerImageResPress;
            public int centerImageRes;

            public int leftTextColor;
            public int centerTextColor;
            public int rightTextColor;
            public View.OnClickListener leftOnClickListener;
            public OnCheckedListener centerOnClickListener;
            public View.OnClickListener rightOnClickListener;

            public HomeNavigationParams(Context context, ViewGroup parent) {
                super(context, parent);
            }
        }
    }


}