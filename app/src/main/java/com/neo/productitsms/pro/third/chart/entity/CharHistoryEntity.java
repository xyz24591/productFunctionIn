package com.neo.productitsms.pro.third.chart.entity;

/**
 * Created by Administrator on 2018/10/11.
 */

public class CharHistoryEntity {

//    {
//        "itemId":"504833",  				 //监控项id
//            "startTime":"1538969137",			//开始时间（10位时间戳）
//            "endTime":"1539055537"			//结束时间（10位时间戳）
//    }


    private String itemId;
    private String startTime;
    private String endTime;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
