package com.neo.productitsms.pro.third.chart;

import android.os.Bundle;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.neo.productitsms.R;
import com.neo.productitsms.mvp.presenter.impl.MvpBasePresenter;
import com.neo.productitsms.pro.base.view.activity.BaseActivtiy;
import com.neo.productitsms.pro.third.chart.entity.BarChartBean;
import com.neo.productitsms.pro.third.chart.manger.BarChartManager;
import com.neo.productitsms.pro.third.chart.utils.AssertUtils;
import com.neo.productitsms.utils.GsonUtil;
import com.neo.productitsms.utils.LogUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/10/15.
 */

public class DemoBar extends BaseActivtiy {
    @BindView(R.id.bar_chart)
    BarChart barChart;

    BarChartManager chartManager;
    @BindView(R.id.dataSet1TextView)
    TextView dataSet1TextView;
    @BindView(R.id.dataSet2TextView)
    TextView dataSet2TextView;


    @Override
    public MvpBasePresenter bindPresenter() {
        return null;
    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_barchart;
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
        chartManager = new BarChartManager();
        chartManager.initBarChartManger(barChart);

        String json = AssertUtils.getJson(this, "bar_chart.json");

        LogUtils.e("--->" + json);

        BarChartBean barChartBean = GsonUtil.GsonToBean(json, BarChartBean.class);

        List<BarChartBean.StFinDateBean.VtDateValueBean> dateValueList = barChartBean.getStFinDate().getVtDateValue();

        LogUtils.e("转换之前：" + GsonUtil.GsonString(dateValueList));

        Collections.reverse(dateValueList);
        LogUtils.e("转换之后：" + GsonUtil.GsonString(dateValueList));


//        chartManager.showBarChart(dateValueList, "净资产收益率(%)", getResources().getColor(R.color.blue));


        //处理数据是 记得判断每条柱状图对应的数据集合 长度是否一致
        LinkedHashMap<String, List<Float>> chartDataMap = new LinkedHashMap<>();
        List<String> xValues = new ArrayList<>();
        List<Float> yValue1 = new ArrayList<>();
        List<Float> yValue2 = new ArrayList<>();
        List<Integer> colors = Arrays.asList(
                getResources().getColor(R.color.blue), getResources().getColor(R.color.orange)
        );

        List<BarChartBean.StFinDateBean.VtDateValueBean> valueList = barChartBean.getStFinDate().getVtDateValue();
        List<BarChartBean.StFinDateBean.VtDateValueAvgBean> avgValueList = barChartBean.getStFinDate().getVtDateValueAvg();
        Collections.reverse(valueList);

        for (BarChartBean.StFinDateBean.VtDateValueBean valueBean : valueList) {
            xValues.add(valueBean.getSYearMonth());
            yValue1.add((float) valueBean.getFValue());
        }
        for (BarChartBean.StFinDateBean.VtDateValueAvgBean valueAvgBean : avgValueList) {
            yValue2.add((float) valueAvgBean.getFValue());
        }
        chartDataMap.put("净资产收益率（%）", yValue1);
        chartDataMap.put("行业平均值（%）", yValue2);

        chartManager.showBarChart(xValues, chartDataMap, colors);



        barChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {

//                e.getX();       //X轴坐标 记得转 int
//                e.getY();       //当前柱状图Y轴值
//                e.getIcon();    //对应 BarEntry(float x, float y, Drawable icon)
//                e.getData();    //对应 BarEntry(float x, float y, Object data)

                //得到包含此柱状图的 数据集
                BarDataSet dataSets = (BarDataSet) barChart.getBarData().getDataSetForEntry(e);
                dataSet1TextView.setText("被点击的柱状图名称：\n" + dataSets.getLabel() + "X轴：" + (int) e.getX() + "    Y轴" + e.getX());

                StringBuffer allBarChart = new StringBuffer();
                allBarChart.append("所有柱状图：\n");
                for (IBarDataSet dataSet : barChart.getBarData().getDataSets()) {
                    BarEntry entry = dataSet.getEntryForIndex((int) e.getX());
                    allBarChart.append(dataSet.getLabel())
                            .append("X轴：")
                            .append((int) entry.getX())
                            .append("    Y轴")
                            .append(entry.getY())
                            .append("\n");
                }
                dataSet2TextView.setText(allBarChart.toString());

            }

            @Override
            public void onNothingSelected() {

            }
        });

    }



}
