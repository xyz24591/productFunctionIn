package com.neo.productitsms.pro.message.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.neo.productitsms.R;
import com.neo.productitsms.pro.message.model.MessageEntity;

import java.util.List;

/**
 * Created by Administrator on 2018/8/2.
 */

public class MessageInfoAdapter extends BaseQuickAdapter<MessageEntity, BaseViewHolder> {
    public MessageInfoAdapter(@LayoutRes int layoutResId, @Nullable List<MessageEntity> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, MessageEntity item) {

        ((TextView) helper.getView(R.id.message_item_title_tv)).setText(item.getMsgTitle());
        ((TextView) helper.getView(R.id.message_item_detail_tv)).setText(item.getMsgDetail());
        ((TextView) helper.getView(R.id.message_item_time_tv)).setText(item.getMsgTime());
        ((TextView) helper.getView(R.id.message_item_notif_num)).setText(String.valueOf((int) (Math.random() * 99)));

    }
}
