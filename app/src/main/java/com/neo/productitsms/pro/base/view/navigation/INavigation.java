package com.neo.productitsms.pro.base.view.navigation;

/**
 * Created by Administrator on 2018/6/26.
 */

public interface INavigation {

    int getLayoutId();

    void createAndBind();

}
