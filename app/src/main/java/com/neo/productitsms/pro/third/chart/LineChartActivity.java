package com.neo.productitsms.pro.third.chart;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.neo.productitsms.R;
import com.neo.productitsms.config.ConstantParams;
import com.neo.productitsms.http.framework.UIThreadCallBack;
import com.neo.productitsms.mvp.presenter.impl.MvpBasePresenter;
import com.neo.productitsms.pro.base.view.activity.BaseActivtiy;
import com.neo.productitsms.pro.third.chart.adapter.ExpandableItemAdapter;
import com.neo.productitsms.pro.third.chart.entity.BranchesEntity;
import com.neo.productitsms.pro.third.chart.entity.ChartEntity;
import com.neo.productitsms.pro.third.chart.presenter.LineChaetPresenter;
import com.neo.productitsms.utils.GsonUtil;
import com.neo.productitsms.utils.LogUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by Administrator on 2018/10/9.
 */

public class LineChartActivity extends BaseActivtiy {

    //    getLineChartData
    LineChaetPresenter presenter;
    @BindView(R.id.list_equ_recyclerview)
    RecyclerView listEquRecyclerview;


    @Override
    public MvpBasePresenter bindPresenter() {

        presenter = new LineChaetPresenter(this);

        return presenter;
    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_line_chart;
    }

    @Override
    protected void initData() {


    }

    private void processSBData() {

//        String stringProvider = JsonDataProvider.StringHolder();

//        JsonDataProvider.String2Bean();


        presenter.getLineChartData(ConstantParams.EQU_ID, new UIThreadCallBack<String>() {
            @Override
            public void onResponse(Response<String> response) {
                LogUtils.i("onResponse----");

                Map<String, List<ChartEntity>> stringListMap = JsonDataProvider.String2Bean(response.body());

                LogUtils.i("--获取到数据-" + GsonUtil.GsonString(stringListMap));

//                processChartData(stringListMap);
                ArrayList<MultiItemEntity> showListData = generateData(stringListMap);

                showAndSetValue(showListData);

            }

            @Override
            public void onResFalse(String message) {
                LogUtils.i("onResFalse----");
            }

            @Override
            public void onFail(String errorMessage) {
//                Map<String, List<ChartEntity>> stringListMap = JsonDataProvider.String2Bean(errorMessage);
//
//                LogUtils.i("--获取到数据-" + GsonUtil.GsonString(stringListMap));
//
//                processChartData(stringListMap);
//                ArrayList<MultiItemEntity> showListData = generateData(stringListMap);
//
//                showAndSetValue(showListData);

            }
        });


    }

    private void showAndSetValue(ArrayList<MultiItemEntity> showListData) {

        ExpandableItemAdapter adapter = new ExpandableItemAdapter(showListData);

        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        listEquRecyclerview.setLayoutManager(manager);

        listEquRecyclerview.setAdapter(adapter);
    }

    private ArrayList<MultiItemEntity> generateData(Map<String, List<ChartEntity>> stringListMap) {

        ArrayList<MultiItemEntity> res = new ArrayList<>();

        for (Map.Entry<String, List<ChartEntity>> groupData : stringListMap.entrySet()) {

            BranchesEntity lv0 = new BranchesEntity(groupData.getKey());

            List<ChartEntity> childData = stringListMap.get(groupData.getKey());

            for (int i = 0; i < childData.size(); i++) {
                lv0.addSubItem(childData.get(i));
            }
            res.add(lv0);

        }

        LogUtils.i("--转换 之后-" + GsonUtil.GsonString(res));

        return res;
    }


    private void processChartData(Map<String, List<ChartEntity>> stringListMap) {


        for (Map.Entry<String, List<ChartEntity>> entry : stringListMap.entrySet()) {

            LogUtils.i("-键-》" + entry.getKey() + "<-值->" + GsonUtil.GsonString(entry.getValue()));

        }


    }


    @Override
    protected void initView() {
        processSBData();
    }


}
