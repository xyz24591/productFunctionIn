package com.neo.productitsms.pro.widget.marquee;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.neo.productitsms.R;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Administrator
 * @date 2018/7/23
 * <p>
 * 公告广告通知显示滚动，类似淘宝等头条滚动效果
 */

public class MarqueeActivity extends AppCompatActivity {

    private MarqueeView marqueeView;
    private MarqueeView marqueeView1;
    private MarqueeView marqueeView3;
    private MarqueeView marqueeView2;
    private MarqueeView marqueeView4;
    private String notice = "我是 滚动轮播条";
    private int marqueeNum = 6;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marqueen);
        initView();
        initData();
        setOnclickListener();
    }

    /**
     * xml属性 Attribute 属性	Description 描述
     * mvAnimDuration	一行文字动画执行时间
     * mvInterval	两行文字翻页时间间隔
     * mvTextSize	文字大小
     * mvTextColor	文字颜色
     * mvGravity	文字位置:left、center、right
     * mvSingleLine	单行设置
     */
    private void initView() {

        marqueeView = (MarqueeView) findViewById(R.id.marqueeView);
        marqueeView1 = (MarqueeView) findViewById(R.id.marqueeView1);
        marqueeView3 = (MarqueeView) findViewById(R.id.marqueeView3);
        marqueeView2 = (MarqueeView) findViewById(R.id.marqueeView2);
        marqueeView4 = (MarqueeView) findViewById(R.id.marqueeView4);

    }

    private void initData() {

        /**
         * 设置一个集合
         */
        List<String> info = new ArrayList<>();

        for (int i = 0; i < marqueeNum; i++) {
            info.add(i + ". 滚动条展示 " + i);
        }
        marqueeView.startWithList(info);
        /**
         * 若是设置字符串 String notice = "心中有阳光，脚底有力量！心中有阳光，脚底有力量！心中有阳光，脚底有力量！";
         * marqueeView.startWithText(notice);
         */

        marqueeView1.startWithText(notice);
        marqueeView2.startWithText(notice);
        marqueeView3.startWithText(notice);
        marqueeView4.startWithText(notice);


    }

    protected void setOnclickListener() {
        marqueeView.setOnItemClickListener(new MarqueeView.OnItemClickListener() {
            @Override
            public void onItemClick(int position, TextView textView) {
                Toast.makeText(getApplicationContext(), String.valueOf(marqueeView.getPosition()) + ". " + textView.getText(), Toast.LENGTH_SHORT).show();
            }
        });

        marqueeView1.setOnItemClickListener(new MarqueeView.OnItemClickListener() {
            @Override
            public void onItemClick(int position, TextView textView) {
                Toast.makeText(getApplicationContext(), textView.getText() + "", Toast.LENGTH_SHORT).show();
            }
        });

    }


}
