package com.neo.productitsms.pro.qrpro;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.neo.productitsms.R;
import com.neo.productitsms.config.ConstantParams;
import com.neo.productitsms.http.framework.impl.RequestManger;
import com.neo.productitsms.qrcode.CaptureActivity;
import com.neo.productitsms.utils.GsonUtil;
import com.neo.productitsms.utils.ToastUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ScanLoginActivity extends CaptureActivity {
    protected Activity mActivity = this;

    private AlertDialog mDialog;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        mActivity = this;
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();

        //        protected RelativeLayout captureRl;
//        protected RelativeLayout loginFlashLinghtRl;
        isLoginBottom(true);
    }

    @Override
    protected void handleResult(final String resultString) {
        if (TextUtils.isEmpty(resultString)) {
            Toast.makeText(mActivity, R.string.scan_failed, Toast.LENGTH_SHORT).show();
            restartPreview();
        } else {
            if (mDialog == null) {
                mDialog = new AlertDialog.Builder(mActivity)
                        .setMessage(resultString)
                        .setPositiveButton("确定", null)
                        .create();
                mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        restartPreview();
                    }
                });
            }
            if (!mDialog.isShowing()) {
                mDialog.setMessage(resultString);
                mDialog.show();
            }

            Call<String> qrCall = RequestManger.getInstance().getStringService().scanCode(resultString);
            qrCall.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {

                    String body = response.body();

                    if (body != null) {

//                        QrLoginRespBean

                        QrLoginRespBean loginRespBean = GsonUtil.GsonToBean(body, QrLoginRespBean.class);

                        if (loginRespBean != null) {

                            boolean res = loginRespBean.isRes();

                            if (res) {
                                ConstantParams.QR_UID = resultString;

//                                Toast.makeText(mActivity, "跳转 登录界面", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(ScanLoginActivity.this, QrLoginSureActivity.class));
                                onBackPressed();
                            } else {

                                Toast.makeText(mActivity, loginRespBean.getMessage(), Toast.LENGTH_SHORT).show();
                                onBackPressed();
                            }


                        }


                    }

                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Toast.makeText(mActivity, "异常", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                }
            });

        }
    }
}


//public class SimpleCaptureActivity extends CaptureActivity {
//    protected Activity mActivity = this;
//
//    private AlertDialog mDialog;
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        mActivity = this;
//        super.onCreate(savedInstanceState);
//    }
//
//    @Override
//    protected void handleResult(final String resultString) {
//        if (TextUtils.isEmpty(resultString)) {
//            Toast.makeText(mActivity, io.github.xudaojie.qrcodelib.R.string.scan_failed, Toast.LENGTH_SHORT).show();
//            restartPreview();
//        } else {
//            if (mDialog == null) {
//                mDialog = new AlertDialog.Builder(mActivity)
//                        .setTitle("打开浏览器下载")
//                        .setMessage(resultString)
////                        .setMessage("打开浏览器下载")
//                        .setPositiveButton("确定", null)
//                        .create();
//                mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                    @Override
//                    public void onDismiss(DialogInterface dialog) {
//                        restartPreview();
//
//                        if (!TextUtils.isEmpty(resultString)) {
//                            boolean matches = Patterns.WEB_URL.matcher(resultString).matches();
//                            if (matches) {
//                                openBrowser(SimpleCaptureActivity.this, resultString);
//                            } else {
//                                Toast.makeText(mActivity, "不合法的URL", Toast.LENGTH_SHORT).show();
//                            }
//
//                        }
//
//
//                    }
//                });
//            }
//            if (!mDialog.isShowing()) {
//                mDialog.setMessage(resultString);
//                mDialog.show();
//            }
//        }
//    }
//
//    /**
//     * 调用第三方浏览器打开
//     *
//     * @param context
//     * @param url     要浏览的资源地址
//     */
//    public static void openBrowser(Context context, String url) {
//        final Intent intent = new Intent();
//        intent.setAction(Intent.ACTION_VIEW);
//        intent.setData(Uri.parse(url));
//        // 注意此处的判断intent.resolveActivity()可以返回显示该Intent的Activity对应的组件名
//        // 官方解释 : Name of the component implementing an activity that can display the intent
//        if (intent.resolveActivity(context.getPackageManager()) != null) {
//            final ComponentName componentName = intent.resolveActivity(context.getPackageManager());
//            // 打印Log   ComponentName到底是什么
//            Log.i("调用浏览器下载", "componentName = " + componentName.getClassName());
//            context.startActivity(Intent.createChooser(intent, "请选择浏览器"));
//        } else {
//            Toast.makeText(context.getApplicationContext(), "请下载浏览器", Toast.LENGTH_SHORT).show();
//        }
//    }
//
//
//}
