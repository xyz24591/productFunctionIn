package com.neo.productitsms.pro.home.view.navigation;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.neo.productitsms.R;
import com.neo.productitsms.pro.navibar.NavigationBuilderAdapter;
import com.neo.productitsms.utils.ToastUtil;

/**
 * Created by Administrator on 2018/7/26.
 */

public class HomeNaviBar extends NavigationBuilderAdapter implements View.OnClickListener {
    private Context context;
    private View.OnClickListener searchOnClickListener;
    private View.OnClickListener searchAddOnClickListener;

    public HomeNaviBar(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    public int getLayoutId() {
        return R.layout.bar_home;
    }

    public HomeNaviBar setOnSearchListener(View.OnClickListener onClickListener) {
        this.searchOnClickListener = onClickListener;
        return this;
    }

    public HomeNaviBar setOnSearchAddListener(View.OnClickListener onClickListener) {
        this.searchAddOnClickListener = onClickListener;
        return this;
    }

    @Override
    public void createAndBind(ViewGroup parent) {
        super.createAndBind(parent);


//        setImageViewStyle(R.id.home_bar_add_tv, R.mipmap.tabbar_message_center, getRightIconOnClickListener());

        LinearLayout homeBarSearchLl = (LinearLayout) findViewById(R.id.home_bar_search_ll);
        homeBarSearchLl.setOnClickListener(searchOnClickListener);

        TextView search = (TextView) findViewById(R.id.home_bar_add_tv);
        search.setOnClickListener(searchAddOnClickListener);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            default:
                break;
        }


    }
}
