package com.neo.productitsms.pro.third.chart.entity;

/**
 * Created by Administrator on 2018/10/15.
 */

public class VtDateValueAvgBean {

    /**
     * fValue : 7.50136
     * sYearMonth : 2016-12
     */

    private double fValue;
    private String sYearMonth;

    public double getfValue() {
        return fValue;
    }

    public void setfValue(double fValue) {
        this.fValue = fValue;
    }

    public String getsYearMonth() {
        return sYearMonth;
    }

    public void setsYearMonth(String sYearMonth) {
        this.sYearMonth = sYearMonth;
    }
}
