package com.neo.productitsms.pro.third.chart;

import com.neo.productitsms.pro.third.chart.entity.ChartEntity;
import com.neo.productitsms.pro.third.chart.entity.ChartRespEntity;
import com.neo.productitsms.utils.GsonUtil;
import com.neo.productitsms.utils.LogUtils;
import com.umeng.commonsdk.debug.E;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.DataFormatException;

/**
 * Created by Administrator on 2018/10/9.
 */

public class JsonDataProvider {

    public static String StringHolder() {

        return jsonData;

    }


    public static Map<String, List<ChartEntity>> String2Bean(String jsonData) {
//        ChartRespEntity chartEntity = GsonUtil.GsonToBean(StringHolder(), ChartRespEntity.class);
        ChartRespEntity chartEntity = GsonUtil.GsonToBean(jsonData, ChartRespEntity.class);

        List<ChartEntity> chartEntities = chartEntity.getBring();


        LogUtils.i("生成的是" + GsonUtil.GsonString(chartEntity));
        LogUtils.i("生成的是--------------------");
        LogUtils.i("------" + chartEntity.isRes());

//        Map<String, List<ChartEntity>> dataContainer = new HashMap<>();
//
//        for (int i = 0; i < chartEntities.size(); i++) {
//            ChartEntity entity = chartEntities.get(i);
//
//            String applicationName = entity.getApplicationName();
//
//            List<ChartEntity> childEntity = dataContainer.get(applicationName);
//
//            if (childEntity == null) {
//
//                childEntity = new ArrayList<>();
//                childEntity.add(entity);
//                dataContainer.put(applicationName, childEntity);
//
//                LogUtils.i("---"+applicationName);
//
//            } else {
//                childEntity.add(entity);
//                LogUtils.i("--000-"+applicationName);
//            }
//
//
//
//
//        }
//        LogUtils.i("==循环="+dataContainer.size());

        Map<String, List<ChartEntity>> entityMap = new HashMap<>();


        for (ChartEntity entity : chartEntities) {

            String applicationName = entity.getApplicationName();
            List<ChartEntity> tempList = entityMap.get(applicationName);

            if (tempList == null) {

                tempList = new ArrayList<>();
                tempList.add(entity);
                entityMap.put(applicationName, tempList);

            } else {
                tempList.add(entity);

            }
//            LogUtils.i("==遍历=" + tempList.size());

        }

        return entityMap;
    }


    private static String jsonData = "{\n" +
            "\t\"purl\" : \"properties/code.properties\",\n" +
            "\t\"res\" : true,\n" +
            "\t\"code\" : null,\n" +
            "\t\"message\" : \"\\n\",\n" +
            "\t\"bring\" : [{\n" +
            "\t\t\t\"newDataId\" : \"504822\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息:CPU信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"120\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:39:43\",\n" +
            "\t\t\t\"newData\" : \"0.23\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"5分钟CPU平均负载\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504782\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"TCP连接\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"60\",\n" +
            "\t\t\t\"historyPeriod\" : \"1\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:41:03\",\n" +
            "\t\t\t\"newData\" : \"0\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"8081端口TCP连接数\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504780\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"TCP连接\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"60\",\n" +
            "\t\t\t\"historyPeriod\" : \"1\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:41:01\",\n" +
            "\t\t\t\"newData\" : \"0\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"80端口TCP连接数\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504781\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"TCP连接\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"60\",\n" +
            "\t\t\t\"historyPeriod\" : \"1\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:41:02\",\n" +
            "\t\t\t\"newData\" : \"0\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"81端口TCP连接数\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504784\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"硬件信息:主板信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"Bios供应商\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"4\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504783\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"硬件信息:主板信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"Bios发布日期\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"4\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504785\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"硬件信息:主板信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"Bios版本\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"4\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504833\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息:内存信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"300\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:38:53\",\n" +
            "\t\t\t\"newData\" : \"155.69  MB\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"buffers\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504834\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息:内存信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"300\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:38:55\",\n" +
            "\t\t\t\"newData\" : \"751.93  MB\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"cached\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504825\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"硬件信息:处理器信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"60\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:40:45\",\n" +
            "\t\t\t\"newData\" : \"2.49  GHZ\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"CPU主频\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504821\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息:CPU信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"120\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:39:42\",\n" +
            "\t\t\t\"newData\" : \"6.68  %\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"CPU使用率\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"500602,500603,500604\",\n" +
            "\t\t\t\"triggerState\" : \"0,0,0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504792\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"其他\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"CPU最大频率\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"4\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504795\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"其他\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"CPU电压\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"4\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504827\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息:CPU信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"120\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:39:48\",\n" +
            "\t\t\t\"newData\" : \"93.32  %\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"CPU空闲率\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504794\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"其他\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"CPU类型\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"4\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504828\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息:文件系统\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"3600\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:13:49\",\n" +
            "\t\t\t\"newData\" : \"Filesystem      Size  Used Avail Use% Mounted on\\n/dev/sda3        26G  4.2G   20G  18% /\\ntmpfs           939M  228K  939M   1% /dev/shm\\n/dev/sda1       291M   66M  210M  24% /boot\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"Disks All\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"4\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504853\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"ICMP\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"60\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:40:19\",\n" +
            "\t\t\t\"newData\" : \"1\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"ICMP ping连通性\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"500617\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504854\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"ICMP\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"120\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:40:19\",\n" +
            "\t\t\t\"newData\" : \"0  %\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"ICMP丢包率\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"500618\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504855\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"ICMP\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"60\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:40:19\",\n" +
            "\t\t\t\"newData\" : \"0.7767  ms\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"ICMP响应时间\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"1\",\n" +
            "\t\t\t\"triggerIds\" : \"500619\",\n" +
            "\t\t\t\"triggerState\" : \"1\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504832\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息:内存信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"SWAP大小\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504800\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"硬件信息:主板信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"Wake-up Type\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"4\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504871\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息:磁盘IO\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"1200\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:34:31\",\n" +
            "\t\t\t\"newData\" : \"2.56\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"[sda]传输次数\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504872\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息:磁盘IO\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"1200\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:34:32\",\n" +
            "\t\t\t\"newData\" : \"649.26  GB\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"[sda]写入总量\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504868\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息:磁盘IO\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"1200\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:34:28\",\n" +
            "\t\t\t\"newData\" : \"24.15  KB\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"[sda]写入量/每秒\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504870\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息:磁盘IO\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"1200\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:34:30\",\n" +
            "\t\t\t\"newData\" : \"3.25  GB\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"[sda]读取总量\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504869\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息:磁盘IO\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"1200\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:34:29\",\n" +
            "\t\t\t\"newData\" : \"122.88  B\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"[sda]读取量/每秒\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504786\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"其他\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"一级缓存\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504788\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"其他\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"三级缓存\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504813\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"36000\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"主机名\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"4\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504787\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"其他\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"二级缓存\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504797\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"硬件信息:主板信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"产品名称\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"4\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504798\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"硬件信息:主板信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"产品序列号\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"4\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504814\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"1200\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:33:35\",\n" +
            "\t\t\t\"newData\" : \"0  个\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"僵尸进程\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"500611,500612,500613,500614\",\n" +
            "\t\t\t\"triggerState\" : \"0,0,0,0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504831\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息:内存信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"300\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:38:51\",\n" +
            "\t\t\t\"newData\" : \"82.4141  %\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"内存使用率\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"500605\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504852\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息:内存信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"300\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:39:12\",\n" +
            "\t\t\t\"newData\" : \"1.51  GB\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"内存已用量\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504836\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息:内存信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"300\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:38:57\",\n" +
            "\t\t\t\"newData\" : \"1.83  GB\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"内存总量\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504790\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"硬件信息:内存信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"内存槽数量\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504835\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息:内存信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"300\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:38:56\",\n" +
            "\t\t\t\"newData\" : \"334.99  MB\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"内存空闲量\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504796\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"硬件信息:主板信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"制造商\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"4\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504791\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"其他\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"外部时钟\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504779\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"TCP连接\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"60\",\n" +
            "\t\t\t\"historyPeriod\" : \"1\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:41:00\",\n" +
            "\t\t\t\"newData\" : \"1\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"已建立TCP连接数\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504799\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"硬件信息:主板信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"库存号码\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"4\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504777\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"内存使用情况\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"60\",\n" +
            "\t\t\t\"historyPeriod\" : \"1\",\n" +
            "\t\t\t\"trendPeriod\" : \"60\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"持久带内存使用率\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504776\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"内存使用情况\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"60\",\n" +
            "\t\t\t\"historyPeriod\" : \"1\",\n" +
            "\t\t\t\"trendPeriod\" : \"60\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"持久带已用内存\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504775\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"内存使用情况\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"60\",\n" +
            "\t\t\t\"historyPeriod\" : \"1\",\n" +
            "\t\t\t\"trendPeriod\" : \"60\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"持久带总内存\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504864\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息:文件系统\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"1200\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:34:24\",\n" +
            "\t\t\t\"newData\" : \"24  %\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"文件系统[/boot]使用率\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"500625,500628,500631\",\n" +
            "\t\t\t\"triggerState\" : \"0,0,0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504861\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息:文件系统\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"1200\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:34:21\",\n" +
            "\t\t\t\"newData\" : \"66  MB\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"文件系统[/boot]使用量\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504867\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息:文件系统\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"1600\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:27:47\",\n" +
            "\t\t\t\"newData\" : \"290.51  MB\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"文件系统[/boot]总量\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504863\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息:文件系统\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"1200\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:34:23\",\n" +
            "\t\t\t\"newData\" : \"1  %\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"文件系统[/dev/shm]使用率\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"500624,500627,500630\",\n" +
            "\t\t\t\"triggerState\" : \"0,0,0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504860\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息:文件系统\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"1200\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:34:20\",\n" +
            "\t\t\t\"newData\" : \"228  KB\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"文件系统[/dev/shm]使用量\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504866\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息:文件系统\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"1600\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:27:46\",\n" +
            "\t\t\t\"newData\" : \"938.34  MB\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"文件系统[/dev/shm]总量\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504862\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息:文件系统\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"1200\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:34:22\",\n" +
            "\t\t\t\"newData\" : \"18  %\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"文件系统[/]使用率\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"500623,500626,500629\",\n" +
            "\t\t\t\"triggerState\" : \"0,0,0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504859\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息:文件系统\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"1200\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:34:19\",\n" +
            "\t\t\t\"newData\" : \"4.19  GB\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"文件系统[/]使用量\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504865\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息:文件系统\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"1600\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:27:45\",\n" +
            "\t\t\t\"newData\" : \"25.39  GB\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"文件系统[/]总量\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504771\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"内存使用情况\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"60\",\n" +
            "\t\t\t\"historyPeriod\" : \"1\",\n" +
            "\t\t\t\"trendPeriod\" : \"60\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"新生带内存使用率\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504770\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"内存使用情况\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"60\",\n" +
            "\t\t\t\"historyPeriod\" : \"1\",\n" +
            "\t\t\t\"trendPeriod\" : \"60\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"新生带已用内存\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504769\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"内存使用情况\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"60\",\n" +
            "\t\t\t\"historyPeriod\" : \"1\",\n" +
            "\t\t\t\"trendPeriod\" : \"60\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"新生带总内存\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504816\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"42300\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"时间同步\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"1\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"500615,500616\",\n" +
            "\t\t\t\"triggerState\" : \"0,0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504789\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"硬件信息:内存信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"最大支持内存\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504815\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统启动信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"1200\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:33:36\",\n" +
            "\t\t\t\"newData\" : \"possible SYN flooding on port 7999. Sending cookies.\\npossible SYN flooding on port 7999. Sending cookies.\\npossible SYN flooding on port 7999. Sending cookies.\\npossible SYN flooding on port 7999. Sending cookies.\\npossible SYN flooding on port 7999. Sending cookies.\\npossible SYN flooding on port 7999. Sending cookies.\\npossible SYN flooding on port 7999. Sending cookies.\\npossible SYN flooding on port 7999. Sending cookies.\\npossible SYN flooding on port 7999. Sending cookies.\\npossible SYN flooding on port 7999. Sending cookies.\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"最近一次系统启动信息\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"4\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504806\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"其他\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"90\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"每个CPU核心数\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504823\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"硬件信息:处理器信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"每个CPU核心数\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504826\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"硬件信息:处理器信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"物理CPU数\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504805\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"其他\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"90\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"物理CPU数据\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504820\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"60\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:40:41\",\n" +
            "\t\t\t\"newData\" : \"3  用户\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"用户连接数\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504810\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"硬件信息:电源信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"90\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"电源型号\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"4\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504811\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"硬件信息:电源信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"90\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"电源序列号\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"4\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504812\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"硬件信息:电源信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"90\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"电源最大功率\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"4\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504802\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"其他\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"90\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"硬盘厂家\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"4\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504808\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"其他\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"90\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"硬盘型号\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504809\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"其他\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"90\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"硬盘大小\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504829\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"硬件信息:硬盘信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"硬盘数量\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504778\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统进程\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"60\",\n" +
            "\t\t\t\"historyPeriod\" : \"1\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:40:59\",\n" +
            "\t\t\t\"newData\" : \"152\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"系统当前进程数\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504818\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"60\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:40:39\",\n" +
            "\t\t\t\"newData\" : \"152  进程\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"系统总进程数\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504817\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"系统版本\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"4\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504819\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"60\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:40:40\",\n" +
            "\t\t\t\"newData\" : \"326天7时34分47秒\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"系统运行时间\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504793\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"其他\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"线程数\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"4\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504804\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"其他\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"90\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"网卡MAC地址\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"4\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504857\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息:网卡信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"300\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:39:17\",\n" +
            "\t\t\t\"newData\" : \"1.90  Kbps\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"网卡[eth2] 流量RX\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"500621\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504858\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"系统信息:网卡信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"300\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"2018/10/09 10:39:18\",\n" +
            "\t\t\t\"newData\" : \"2  Kbps\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"网卡[eth2] 流量TX\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"500622\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504803\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"其他\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"90\",\n" +
            "\t\t\t\"trendPeriod\" : \"0\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"网卡型号\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"4\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504801\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"其他\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"90\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"网卡数据\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504830\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"硬件信息:网卡信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"网卡数量\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504774\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"内存使用情况\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"60\",\n" +
            "\t\t\t\"historyPeriod\" : \"1\",\n" +
            "\t\t\t\"trendPeriod\" : \"60\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"老年带内存使用率\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504773\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"内存使用情况\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"60\",\n" +
            "\t\t\t\"historyPeriod\" : \"1\",\n" +
            "\t\t\t\"trendPeriod\" : \"60\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"老年带已用内存\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504772\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"内存使用情况\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"60\",\n" +
            "\t\t\t\"historyPeriod\" : \"1\",\n" +
            "\t\t\t\"trendPeriod\" : \"60\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"老年带总内存\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"0\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504807\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"其他\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"90\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"逻辑CPU总数\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"1\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"newDataId\" : \"504824\",\n" +
            "\t\t\t\"type\" : \"item\",\n" +
            "\t\t\t\"applicationName\" : \"硬件信息:处理器信息\",\n" +
            "\t\t\t\"parentId\" : \"\",\n" +
            "\t\t\t\"interval\" : \"86400\",\n" +
            "\t\t\t\"historyPeriod\" : \"7\",\n" +
            "\t\t\t\"trendPeriod\" : \"365\",\n" +
            "\t\t\t\"resentlyCheckDate\" : \"\",\n" +
            "\t\t\t\"newData\" : \"\",\n" +
            "\t\t\t\"change\" : \"\",\n" +
            "\t\t\t\"newDataName\" : \"逻辑CPU总数\",\n" +
            "\t\t\t\"valueMapId\" : null,\n" +
            "\t\t\t\"valueType\" : \"3\",\n" +
            "\t\t\t\"error\" : \"\",\n" +
            "\t\t\t\"valueMap\" : null,\n" +
            "\t\t\t\"hasAlert\" : \"0\",\n" +
            "\t\t\t\"triggerIds\" : \"\",\n" +
            "\t\t\t\"triggerState\" : \"0\",\n" +
            "\t\t\t\"itemStatus\" : \"0\"\n" +
            "\t\t}\n" +
            "\t]\n" +
            "}\n";

}
