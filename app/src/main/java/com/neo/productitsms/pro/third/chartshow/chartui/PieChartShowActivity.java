package com.neo.productitsms.pro.third.chartshow.chartui;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.neo.productitsms.R;
import com.neo.productitsms.mvp.presenter.impl.MvpBasePresenter;
import com.neo.productitsms.pro.base.view.activity.BaseActivtiy;
import com.neo.productitsms.pro.third.chartshow.chartentity.PieChartEntity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/11/10.
 */

public class PieChartShowActivity extends BaseActivtiy {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.new_pie_chart)
    PieChart newPieChart;
    @BindView(R.id.new_the_pie_chart)
    PieChart newThePieChart;

    @Override
    public MvpBasePresenter bindPresenter() {
        return null;
    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_pie_show;
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
        updatePieChart();
        updateThePieChart();
    }


    /**
     * 添加数据均匀饼装图
     */
    public void updatePieChart() {
        int[] colors = {Color.parseColor("#faa74c"), Color.parseColor("#58D4C5"), Color.parseColor("#36a3eb"), Color.parseColor("#cc435f"), Color.parseColor("#f1ea56"),
                Color.parseColor("#f49468"), Color.parseColor("#d5932c"), Color.parseColor("#34b5cc"), Color.parseColor("#8169c6"), Color.parseColor("#ca4561"), Color.parseColor("#fee335")};
        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();
        for (int i = 0; i <= 5; i++) {
            PieEntry pieEntry = new PieEntry(60, "项目" + i + "占比");
            entries.add(pieEntry);
        }

        for (int i = 6; i <= 7; i++) {
            PieEntry pieEntry = new PieEntry(100, "项目" + i + "占比");
            entries.add(pieEntry);
        }

        PieEntry pieEntry = new PieEntry(100, "项目8占比");
        entries.add(pieEntry);

        if (entries.size() != 0) {
            PieChart new_pie_chart = (PieChart) findViewById(R.id.new_pie_chart);
            PieChartEntity pieChartEntity = new PieChartEntity(new_pie_chart, entries, new String[]{"", "", ""}, colors, 12f, Color.GRAY, PieDataSet.ValuePosition.OUTSIDE_SLICE);
            pieChartEntity.setHoleEnabled(Color.TRANSPARENT, 40f, Color.TRANSPARENT, 40f);
            pieChartEntity.setLegendEnabled(false);
            pieChartEntity.setPercentValues(true);
        }
    }

    /**
     * 添加数据不均匀饼装图，避免文字重合
     */
    public void updateThePieChart() {
        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();
        PieChart new_the_pie_chart = (PieChart) findViewById(R.id.new_the_pie_chart);
        int[] colors = {Color.parseColor("#faa74c"), Color.parseColor("#58D4C5"), Color.parseColor("#36a3eb"), Color.parseColor("#cc435f"), Color.parseColor("#f1ea56"),
                Color.parseColor("#f49468"), Color.parseColor("#d5932c"), Color.parseColor("#34b5cc"), Color.parseColor("#8169c6"), Color.parseColor("#ca4561"), Color.parseColor("#fee335")};

        for (int i = 1; i <= 8; i++) {
            int all = 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8;
            PieEntry pieEntry = new PieEntry(i, "", i > 3);
            entries.add(pieEntry);
        }

//        PieChartEntity pieChartEntity = new PieChartEntity(new_the_pie_chart, entries, new String[]{"", "", ""}, colors, 12f, Color.WHITE);
        /**
         * 下面注释的位置与上面显示一样，为向外文字显示
         */
        PieChartEntity pieChartEntity = new PieChartEntity(new_the_pie_chart, entries, new String[]{"", "", ""}, colors, 12f, Color.GRAY, PieDataSet.ValuePosition.OUTSIDE_SLICE);
        pieChartEntity.setHoleEnabled(Color.TRANSPARENT, 40f, Color.TRANSPARENT, 40f);
        pieChartEntity.setLegendEnabled(false);
        pieChartEntity.setPercentValues(true);
    }


}
