package com.neo.productitsms.pro.compose.presenter;

import android.content.Context;

import com.neo.productitsms.pro.base.presenter.BasePresenter;
import com.neo.productitsms.pro.base.view.lce.ResultView;
import com.neo.productitsms.pro.compose.model.ComposeModel;

/**
 * Created by Administrator on 2018/6/26.
 */

public class ComposePresenter extends BasePresenter<ComposeModel> {
    public ComposePresenter(Context context) {
        super(context);
    }

    @Override
    public ComposeModel bindModel() {
        return new ComposeModel(getContext());
    }
}
