package com.neo.productitsms.pro.third.chart.entity;

import com.chad.library.adapter.base.entity.AbstractExpandableItem;
import com.chad.library.adapter.base.entity.MultiItemEntity;

/**
 * Created by Administrator on 2018/10/11.
 */

public class LeafEntity extends AbstractExpandableItem<ChartEntity> implements MultiItemEntity {

private String title;

    public LeafEntity() {
    }

    public LeafEntity(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int getLevel() {
        return 0;
    }

    @Override
    public int getItemType() {
        return 0;
    }
}
