package com.neo.productitsms.pro.third.chart;

/**
 * Created by Administrator on 2018/7/21.
 */

public class Entity {
    private int i;
    private float data;

    public Entity() {
    }

    public Entity(int i, float data) {
        this.i = i;
        this.data = data;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public float getData() {
        return data;
    }

    public void setData(float data) {
        this.data = data;
    }
}
