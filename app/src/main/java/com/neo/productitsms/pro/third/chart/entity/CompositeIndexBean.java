package com.neo.productitsms.pro.third.chart.entity;


/**
 * 沪深创指数
 */
public class CompositeIndexBean {
    /**
     * rate : -0.00034196
     * tradeDate : 20180502
     */
    private double rate;
    private String tradeDate;


    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(String tradeDate) {
        this.tradeDate = tradeDate;
    }
}
