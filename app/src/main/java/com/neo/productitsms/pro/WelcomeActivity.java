package com.neo.productitsms.pro;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.neo.productitsms.R;
import com.neo.productitsms.config.AppLoginConfig;
import com.neo.productitsms.config.ConstantParams;
import com.neo.productitsms.pro.login.view.LoginActivity;
import com.neo.productitsms.utils.LogUtils;

import java.io.IOException;

/**
 * Created by Administrator on 2018/6/26.
 */

public class WelcomeActivity extends FragmentActivity {

    private SkipCountDownTimer mSkipCountDownTimer;
    private TextView startSkipCountDown;

    Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
//            super.handleMessage(msg);

            if (msg.what == 0) {
                jumpAndFinish();
            }


        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        initWelcome();
        initLogoView();
        initSkip();
    }

    private void initSkip() {
        startSkipCountDown = findViewById(R.id.welcome_skip_tv);
        FrameLayout startSkip = (FrameLayout) findViewById(R.id.welcome_skip_ads_fl);
        startSkipCountDown.setText("3s 跳过");


        startSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancleTimeCount();
                jumpAndFinish();

            }
        });

        mSkipCountDownTimer = new SkipCountDownTimer(ConstantParams.ANIMATOR_DURATION, ConstantParams.ANIMATOR_PER);
        mSkipCountDownTimer.start();

    }

    private void initWelcome() {

        RelativeLayout welcomeRl = findViewById(R.id.rl_welcome);
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(welcomeRl, "alpha", 0.8f, 1.0f);
//        执行动画时间
        objectAnimator.setDuration(ConstantParams.ANIMATOR_DURATION);
//        启动动画
        objectAnimator.start();
//跳转
        objectAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
//                startActivity(new Intent(WelcomeActivity.this, GuideActivity.class));
//                finish();
            }
        });
    }

    private void initLogoView() {
        ImageView iv_logo = (ImageView) findViewById(R.id.iv_logo);

        ObjectAnimator scaleXAnimatorBig = ObjectAnimator.ofFloat(iv_logo,
                "scaleX", 1.0f, 1.2f, 1.0f);
        ObjectAnimator scaleYAnimatorBig = ObjectAnimator.ofFloat(iv_logo,
                "scaleY", 1.0f, 1.2f, 1.0f);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(scaleXAnimatorBig).with(scaleYAnimatorBig);
        animatorSet.setDuration(2000);
        animatorSet.start();
    }


    class SkipCountDownTimer extends CountDownTimer {


        /**
         * @param millisInFuture    The number of millis in the future from the call
         *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
         *                          is called.
         *                          表示以「 毫秒 」为单位倒计时的总数
         *                          例如 millisInFuture = 1000 表示1秒
         * @param countDownInterval The interval along the way to receive
         *                          {@link #onTick(long)} callbacks.
         *                          表示 间隔 多少微秒 调用一次 onTick()
         *                          例如: countDownInterval = 1000 ; 表示每 1000 毫秒调用一次 onTick()
         */
        public SkipCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {

            startSkipCountDown.setText(millisUntilFinished / 1000 + "s 跳过");
        }

        @Override
        public void onFinish() {
            startSkipCountDown.setText("0s 跳过");
            startSkipCountDown.setVisibility(View.GONE);
            handler.sendEmptyMessage(0);

        }
    }


    private void cancleTimeCount() {

        if (mSkipCountDownTimer != null) {
            mSkipCountDownTimer.cancel();
        }


    }

    private void jumpAndFinish() {
        boolean isFirstIn = AppLoginConfig.getInstance().getBoolean(WelcomeActivity.this, AppLoginConfig.IS_FIRST_IN);

        if (isFirstIn) {
            startActivity(new Intent(WelcomeActivity.this, MainActivity.class));
//            startActivity(new Intent(WelcomeActivity.this, LoginActivity.class));
        } else {
            startActivity(new Intent(WelcomeActivity.this, GuideActivity.class));
        }
        overridePendingTransition(R.animator.push_left_in,
                R.animator.push_left_out);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        cancleTimeCount();

    }
}
