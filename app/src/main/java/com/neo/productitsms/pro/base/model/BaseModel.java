package com.neo.productitsms.pro.base.model;

import android.content.Context;

import com.neo.productitsms.mvp.model.MvpModel;

/**
 * Created by Administrator on 2018/6/25.
 */

public abstract class BaseModel implements MvpModel {

    private Context context;

    public BaseModel(Context context){
        this.context = context;
    }


    public Context getContext() {
        return context;
    }
}
