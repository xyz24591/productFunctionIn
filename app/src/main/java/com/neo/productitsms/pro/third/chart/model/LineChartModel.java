package com.neo.productitsms.pro.third.chart.model;

import android.content.Context;

import com.neo.productitsms.http.framework.RequestStringCallBack;
import com.neo.productitsms.http.framework.impl.RequestManger;
import com.neo.productitsms.pro.base.model.BaseModel;
import com.neo.productitsms.pro.third.chart.entity.CharHistoryEntity;

import okhttp3.RequestBody;
import retrofit2.Call;

/**
 * Created by Administrator on 2018/10/10.
 */

public class LineChartModel extends BaseModel {
    public LineChartModel(Context context) {
        super(context);
    }


    public void getLineData(String equId, RequestStringCallBack callBack) {

        Call<String> stringCall = RequestManger.getInstance().getStringService().getNewItemListByEquId(equId);
        stringCall.enqueue(callBack);
    }
    public void getItemHistory(CharHistoryEntity historyReq, RequestStringCallBack callBack) {

        RequestBody requestBody = RequestManger.getInstance().getRequestBody(historyReq);

        Call<String> stringCall = RequestManger.getInstance().getStringService().getItemHistory(requestBody);
        stringCall.enqueue(callBack);
    }

}
