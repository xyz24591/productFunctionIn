package com.neo.productitsms.pro.listgroup;

import android.os.Bundle;
import android.widget.TextView;

import com.neo.productitsms.R;
import com.neo.productitsms.mvp.presenter.impl.MvpBasePresenter;
import com.neo.productitsms.pro.base.view.activity.BaseActivtiy;
import com.neo.productitsms.utils.GsonUtil;
import com.neo.productitsms.utils.LogUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/10/9.
 */

public class ListGroupActivity extends BaseActivtiy {
    @BindView(R.id.tv_show_result)
    TextView tvShowResult;

    @Override
    public MvpBasePresenter bindPresenter() {
        return null;
    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_list_group;
    }

    @Override
    protected void initData() {

        SkuVo sku1 = new SkuVo(1L, "p1", 100L);
        SkuVo sku2 = new SkuVo(2L, "p2", 101L);
        SkuVo sku3 = new SkuVo(3L, "p3", 102L);
        SkuVo sku4 = new SkuVo(3L, "p4", 103L);
        SkuVo sku5 = new SkuVo(2L, "p5", 100L);
        SkuVo sku6 = new SkuVo(5L, "p6", 100L);

        List<SkuVo> skuVoList = Arrays.asList(new SkuVo[]{sku1, sku2, sku3, sku4, sku5, sku6});

  /*2、分组算法**/
        Map<Long, List<SkuVo>> skuIdMap = new HashMap<>();
        for (SkuVo skuVo : skuVoList) {
            List<SkuVo> tempList = skuIdMap.get(skuVo.getSkuId());
            /*如果取不到数据,那么直接new一个空的ArrayList**/
            if (tempList == null) {
                tempList = new ArrayList<>();
                tempList.add(skuVo);
                skuIdMap.put(skuVo.getSkuId(), tempList);
            } else {
                /*某个sku之前已经存放过了,则直接追加数据到原来的List里**/
                tempList.add(skuVo);
            }
        }

        /*3、遍历map,验证结果**/
        for (Long skuId : skuIdMap.keySet()) {
            System.out.println(skuIdMap.get(skuId));
        }

        LogUtils.i("-->"+ skuIdMap.size()+ GsonUtil.GsonString(skuIdMap));

    }

    @Override
    protected void initView() {


    }


}
