package com.neo.productitsms.pro.third.chart.utils;

import com.neo.productitsms.utils.LogUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Administrator on 2018/10/13.
 */

public class TimeUtils {

    /**
     * 日期格式字符串转换成时间戳
     *
     * @param dateStr 字符串日期
     * @param format  如：yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String Date2TimeStamp(String dateStr, String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            return String.valueOf(sdf.parse(dateStr).getTime() / 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    /**
     * 取得当前时间戳（精确到秒）
     *
     * @return nowTimeStamp
     */
    public static String getNowTimeStamp() {
        long time = getCurrentTime();
        String nowTimeStamp = String.valueOf(time / 1000);
        return strTimeSub(nowTimeStamp);
    }


    /**
     * String 时间类型截取
     */
    public static String strTimeSub(String time) {
        String time1 = time;
        if (null != time && time.length() > 10) {
            time1 = time.substring(0, 10);
        }
        return time1;
    }


    public static long getCurrentTime() {

        return System.currentTimeMillis();
    }


    // 获取当前时间的上一小时(YYYY-MM-dd HH:mm:dd)
    public static String getBeforeHourTimeStamp() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY)
                - 1);

        String s = strTimeSub(String.valueOf(calendar.getTimeInMillis() / 1000));

        return s;

    }


    public static String getTimeByTimeStamp(String timeStamp) {

        String s = UnixTimestampToNormal(timeStamp);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String s1;
        try {
            Date parse = dateFormat.parse(timeStamp);
            Calendar calendar = Calendar.getInstance();

            calendar.setTime(parse);
            calendar.add(Calendar.HOUR, -1);

            long timeInMillis = calendar.getTimeInMillis();

            s1 = strTimeSub(String.valueOf(timeInMillis));

        } catch (ParseException e) {
            e.printStackTrace();
            s1 = "";
        }

        return s1;
    }


    // 获取当前时间的上一小时(YYYY-MM-dd HH:mm:dd)
    public static Date getBeforeHourTimeDate(int h) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY)
                - h);

        return calendar.getTime();

    }


    public static String UnixTimestampToNormal(String timestamp) {

        long timeLong = Long.parseLong(timestamp);

        long l = timeLong * 1000;
//        long l = timestamp;
        String date = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date(l));

        return date;
    }


//    long epoch = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse("01/01/1970 01:00:00");

    public static long NormalToUnixTimestamp(String timeText) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date parse = null;
        try {
            parse = dateFormat.parse(timeText);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long time = parse.getTime() / 1000;

//        Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("01/01/1970 01:00:00");

        return time;

    }

}
