package com.neo.productitsms.pro.message.model;

import com.neo.productitsms.pro.base.entity.BaseEntity;

/**
 * Created by Administrator on 2018/8/2.
 */

public class MessageEntity extends BaseEntity {

    private String msgAvtar;
    private String msgTitle;
    private String msgDetail;
    private String msgTime;


    public String getMsgAvtar() {
        return msgAvtar;
    }

    public void setMsgAvtar(String msgAvtar) {
        this.msgAvtar = msgAvtar;
    }

    public String getMsgTitle() {
        return msgTitle;
    }

    public void setMsgTitle(String msgTitle) {
        this.msgTitle = msgTitle;
    }

    public String getMsgDetail() {
        return msgDetail;
    }

    public void setMsgDetail(String msgDetail) {
        this.msgDetail = msgDetail;
    }

    public String getMsgTime() {
        return msgTime;
    }

    public void setMsgTime(String msgTime) {
        this.msgTime = msgTime;
    }
}
