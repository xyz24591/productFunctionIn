package com.neo.productitsms.pro.third.chart.manger;

import android.graphics.Color;
import android.support.annotation.ColorRes;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.neo.productitsms.pro.third.chart.entity.BarChartBean;
import com.neo.productitsms.pro.third.chart.entity.VtDateValueBean;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Administrator on 2018/10/15.
 */

public class BarChartManager {

    private BarChart barChart;
    private YAxis leftAxis;             //左侧Y轴
    private YAxis rightAxis;            //右侧Y轴
    private XAxis xAxis;                //X轴
    private Legend legend;              //图例
    private LimitLine limitLine;        //限制线


    public void initBarChartManger(final BarChart barChart) {

        /***图表设置***/
//        背景颜色
        barChart.setBackgroundColor(Color.WHITE);
//        不显示 网格
        barChart.setDrawGridBackground(false);
//        背景 阴影
        barChart.setDrawBarShadow(false);
        barChart.setHighlightFullBarEnabled(false);
//     true   显示 边框,false 不显示外框
        barChart.setDrawBorders(false);


//        设置 动画效果
        barChart.animateX(1000, Easing.EasingOption.Linear);
        barChart.animateY(1000, Easing.EasingOption.Linear);

        /***XY轴的设置***/
        //X轴设置显示位置在底部
        xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setAxisMinimum(0f);
        xAxis.setGranularity(1f);

        leftAxis = barChart.getAxisLeft();
        rightAxis = barChart.getAxisRight();
        //保证Y轴从0开始，不然会上移一点
        leftAxis.setAxisMinimum(0f);
        rightAxis.setAxisMinimum(0f);

        /***折线图例 标签 设置***/
        legend = barChart.getLegend();
        legend.setForm(Legend.LegendForm.LINE);
        legend.setTextSize(11f);
        //显示位置
        legend.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        legend.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        //是否绘制在图表里面
        legend.setDrawInside(false);





        xAxis.setDrawAxisLine(false);
        leftAxis.setDrawAxisLine(false);
        rightAxis.setDrawAxisLine(false);

        barChart.setDrawGridBackground(false);

        xAxis.setDrawGridLines(false);






//        barChart.setDoubleTapToZoomEnabled(false);
////禁止拖拽
//        barChart.setDragEnabled(false);
////X轴或Y轴禁止缩放
//        barChart.setScaleXEnabled(false);
//        barChart.setScaleYEnabled(false);
//        barChart.setScaleEnabled(false);
////禁止所有事件
//        barChart.setTouchEnabled(false);


        this.barChart = barChart;

    }


    public void initBarDataSet(BarDataSet barDataSet, int color) {

        barDataSet.setColor(color);
        barDataSet.setFormLineWidth(1f);
        barDataSet.setFormSize(15.f);
        //不显示柱状图顶部值
        barDataSet.setDrawValues(false);
//        barDataSet.setValueTextSize(10f);
//        barDataSet.setValueTextColor(color);

    }

    public void showBarChart(final List<BarChartBean.StFinDateBean.VtDateValueBean> dateValueList, String name, int color) {

        ArrayList<BarEntry> entries = new ArrayList<>();

        for (int i = 0; i < dateValueList.size(); i++) {

            /**
             * 此处还可传入Drawable对象 BarEntry(float x, float y, Drawable icon)
             * 即可设置柱状图顶部的 icon展示
             */

            BarEntry barEntry = new BarEntry(i, (float) dateValueList.get(i).getFValue());

            entries.add(barEntry);


        }

        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return dateValueList.get((int) value % dateValueList.size()).getSYearMonth();
            }
        });


//右侧Y轴自定义值
        rightAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return ((int) (value * 100)) + "%";
            }
        });
        xAxis.setAxisMinimum(0f);
//保证Y轴从0开始，不然会上移一点
        leftAxis.setAxisMinimum(0f);
        rightAxis.setAxisMinimum(0f);
        BarDataSet barDataSet = new BarDataSet(entries, name);

        initBarDataSet(barDataSet, color);

        BarData data = new BarData(barDataSet);
//        data.setBarWidth(1f); 间隔 宽度
        data.setBarWidth(0.5f);

        barChart.setData(data);

    }

    /**
     * @param xValues   X轴的值
     * @param dataLists LinkedHashMap<String, List<Float>>
     *                  key对应柱状图名字  List<Float> 对应每类柱状图的Y值
     * @param colors
     */
    public void showBarChart(final List<String> xValues, LinkedHashMap<String, List<Float>> dataLists,
                             List<Integer> colors) {

        List<IBarDataSet> dataSets = new ArrayList<>();
        int currentPosition = 0;//用于柱状图颜色集合的index

        for (LinkedHashMap.Entry<String, List<Float>> entry : dataLists.entrySet()) {
            String name = entry.getKey();
            List<Float> yValueList = entry.getValue();

            List<BarEntry> entries = new ArrayList<>();

            for (int i = 0; i < yValueList.size(); i++) {

//      回调函数中的Entry对应showBarChart方法中 添加的BarEntry，可以在添加的时候 传入一个自定义数据，如name
                entries.add(new BarEntry(i, yValueList.get(i), name));
            }
            // 每一个BarDataSet代表一类柱状图
            BarDataSet barDataSet = new BarDataSet(entries, name);
            initBarDataSet(barDataSet, colors.get(currentPosition));
            dataSets.add(barDataSet);

            currentPosition++;
        }

        xAxis.setAxisMinimum(0f);
        xAxis.setAxisMaximum(xValues.size());
////将X轴的值显示在中央
//        xAxis.setCenterAxisLabels(true);

//        加上 xAxis.setCenterAxisLabels(true); 这行代码后，前面自定义X轴的显示值则数组越界异常。

//        看错误日志是 X轴值为 -1，所以将自定义X轴的显示值改为：

//X轴自定义值
//        xAxis.setValueFormatter(new IAxisValueFormatter() {
//            @Override
//            public String getFormattedValue(float value, AxisBase axis) {
//                return xValues.get((int) Math.abs(value) % xValues.size());
//            }
//        });


//        //X轴自定义值
//        xAxis.setValueFormatter(new IAxisValueFormatter() {
//            @Override
//            public String getFormattedValue(float value, AxisBase axis) {
//                return xValues.get((int) value % xValues.size());
//            }
//        });


//X轴自定义值
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return xValues.get((int) Math.abs(value) % xValues.size());
            }
        });

        //右侧Y轴自定义值
        rightAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return (int) (value) + "%";
            }
        });


//        堆叠显示
//        BarData data = new BarData(dataSets);
//        barChart.setData(data);

        BarData data = new BarData(dataSets);

/**
 * 并列显示
 *
 * float groupSpace = 0.3f;   //柱状图组之间的间距
 * float barSpace =  0.05f;  //每条柱状图之间的间距  一组两个柱状图
 * float barWidth = 0.3f;    //每条柱状图的宽度     一组两个柱状图
 * (barWidth + barSpace) * barAmount + groupSpace = (0.3 + 0.05) * 2 + 0.3 = 1.00
 * 3个数值 加起来 必须等于 1 即100% 按照百分比来计算 组间距 柱状图间距 柱状图宽度
 */
        int barAmount = dataLists.size(); //需要显示柱状图的类别 数量
//设置组间距占比30% 每条柱状图宽度占比 70% /barAmount  柱状图间距占比 0%
        float groupSpace = 0.3f; //柱状图组之间的间距
        float barWidth = (1f - groupSpace) / barAmount;
        float barSpace = 0f;
//设置柱状图宽度
        data.setBarWidth(barWidth);
//(起始点、柱状图组间距、柱状图之间间距)
        data.groupBars(0f, groupSpace, barSpace);
        barChart.setData(data);


    }


}
