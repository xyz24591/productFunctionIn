package com.neo.productitsms.pro.widget;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.widget.TextView;

import com.neo.productitsms.config.ItsmsApplication;

/**
 * Created by Administrator on 2018/7/18.
 */

public class TextViewIcon extends AppCompatTextView {
    public TextViewIcon(Context context) {
        this(context, null);
    }

    public TextViewIcon(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TextViewIcon(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void init(Context context) {
        setTypeface(ItsmsApplication.getIconfont(context));

    }


}
