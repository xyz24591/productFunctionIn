package com.neo.productitsms.pro.allfunction.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.neo.productitsms.R;
import com.neo.productitsms.pro.allfunction.Entity.ExpandListViewChild;
import com.neo.productitsms.pro.allfunction.Entity.ExpandListViewGroup;
import com.neo.productitsms.pro.allfunction.adapter.FunctionAdapter;
import com.neo.productitsms.pro.base.view.fragment.BaseFragment;
import com.neo.productitsms.pro.base.view.navigation.impl.DefaultNavigation;
import com.neo.productitsms.utils.ToastUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Administrator on 2018/6/27.
 */

public class FunctionFragment extends BaseFragment {
    @BindView(R.id.expand_listView_function_fg)
    ExpandableListView expandListViewFunctionFg;
    private List<ExpandListViewGroup> groups;

    @Override
    public int bindLayoutId() {
        return R.layout.fragment_function;
    }

    @Override
    public void initContentView(View contentView) {

        initNavigation(contentView);
    }

    private void initNavigation(View contentView) {

        DefaultNavigation.Builder builder = new DefaultNavigation.Builder(getContext(), (ViewGroup) contentView);
        builder.setCenterText(R.string.tab_discover_text).create().createAndBind();

    }

    @Override
    protected void initData() {
        groups = new ArrayList<>();
        ExpandListViewGroup processStartGroup = new ExpandListViewGroup(R.mipmap.process_start_menu_group, "流程");
        processStartGroup.addChildrenItem(new ExpandListViewChild(R.mipmap.ic_launcher, "发起事项"));
        processStartGroup.addChildrenItem(new ExpandListViewChild(R.mipmap.ic_launcher, "已办事项"));
        processStartGroup.addChildrenItem(new ExpandListViewChild(R.mipmap.ic_launcher, "待办事项"));
        groups.add(processStartGroup);

        ExpandListViewGroup assetGroup = new ExpandListViewGroup(R.mipmap.assets_menu, "资产");
        assetGroup.addChildrenItem(new ExpandListViewChild(R.mipmap.ic_launcher, "资产盘点"));
        assetGroup.addChildrenItem(new ExpandListViewChild(R.mipmap.ic_launcher, "离线盘点"));
        assetGroup.addChildrenItem(new ExpandListViewChild(R.mipmap.ic_launcher, "资产登记"));
        assetGroup.addChildrenItem(new ExpandListViewChild(R.mipmap.ic_launcher, "资产查看"));
        assetGroup.addChildrenItem(new ExpandListViewChild(R.mipmap.ic_launcher, "出入库"));

        groups.add(assetGroup);
        ExpandListViewGroup monitorGroup = new ExpandListViewGroup(R.mipmap.monitor, "巡检管理");
        monitorGroup.addChildrenItem(new ExpandListViewChild(R.mipmap.ic_launcher, "巡检"));
        monitorGroup.addChildrenItem(new ExpandListViewChild(R.mipmap.ic_launcher, "排班"));
        monitorGroup.addChildrenItem(new ExpandListViewChild(R.mipmap.ic_launcher, "离线巡检"));
        groups.add(monitorGroup);

        ExpandListViewGroup knowledgeGroup = new ExpandListViewGroup(R.mipmap.knowledge_lib_menu, "知识库");
        knowledgeGroup.addChildrenItem(new ExpandListViewChild(R.mipmap.ic_launcher, "知识库查询"));
        groups.add(knowledgeGroup);

        initView();

    }

    private void initView() {

        FunctionAdapter functionMenuAdapter = new FunctionAdapter(getContext(), groups);
        expandListViewFunctionFg.setAdapter(functionMenuAdapter);

        expandListViewFunctionFg.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                ToastUtil.showShort(groups.get(groupPosition).getChildItem(childPosition).getName().toString().trim());
                return false;
            }
        });

        expandListViewFunctionFg.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                ToastUtil.showShort(groups.get(groupPosition).getGname().toString());
                return false;
            }
        });


    }


}
