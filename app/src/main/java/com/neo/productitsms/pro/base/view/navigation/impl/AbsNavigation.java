package com.neo.productitsms.pro.base.view.navigation.impl;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.neo.productitsms.pro.base.view.navigation.INavigation;

/**
 * Created by Administrator on 2018/6/26.
 */

public abstract class AbsNavigation <P extends AbsNavigation.Builder.NavigationParams> implements INavigation{

    private View contentView;
    private P params;

    protected AbsNavigation(P params){
        this.params = params;
    }

    protected View findViewById(int viewId){
        return contentView.findViewById(viewId);
    }

    public String getString(int text){
        if (text == 0){
            return null;
        }
        return this.params.context.getResources().getString(text);
    }

    public int getColor(int text){
        return this.params.context.getResources().getColor(text);
    }

    public P getParams() {
        return params;
    }

    @Override
    public void createAndBind() {
        contentView = LayoutInflater.from(params.context).inflate(getLayoutId(),params.parent,false);
        ViewGroup viewGroup = (ViewGroup)contentView.getParent();
        if (viewGroup != null){
            viewGroup.removeView(contentView);
        }
        params.parent.addView(contentView,0);
    }

    public static abstract class Builder{

        public abstract AbsNavigation create();

        public static class NavigationParams{
            public Context context;
            public ViewGroup parent;
            public NavigationParams(Context context,ViewGroup parent){
                this.context = context;
                this.parent = parent;
            }
        }
    }
}
