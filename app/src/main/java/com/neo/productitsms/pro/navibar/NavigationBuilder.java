package com.neo.productitsms.pro.navibar;

import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Administrator on 2018/7/26.
 * <p>
 * 构建ToolBar---Builder设计模式
 */

public interface NavigationBuilder {

    public NavigationBuilder setTitle(String title);

    public NavigationBuilder setTitle(int title);

    public NavigationBuilder setTitleIcon(int iconRes);

    public NavigationBuilder setLeftIcon(int iconRes);

    public NavigationBuilder setRightIcon(int iconRes);

    public NavigationBuilder setLeftIconOnClickListener(View.OnClickListener onClickListener);

    public NavigationBuilder setRightIconOnClickListener(View.OnClickListener onClickListener);

    public void createAndBind(ViewGroup parent);


}
