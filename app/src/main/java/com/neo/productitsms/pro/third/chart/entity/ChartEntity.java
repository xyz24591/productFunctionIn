package com.neo.productitsms.pro.third.chart.entity;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.neo.productitsms.pro.third.chart.adapter.ExpandableItemAdapter;

/**
 * Created by Administrator on 2018/10/9.
 */

public class ChartEntity  implements MultiItemEntity {

    /**
     * newDataId : 504822
     * type : item
     * applicationName : 系统信息:CPU信息
     * parentId : 
     * interval : 120
     * historyPeriod : 7
     * trendPeriod : 365
     * resentlyCheckDate : 2018/10/09 10:39:43
     * newData : 0.23
     * change : 
     * newDataName : 5分钟CPU平均负载
     * valueMapId : null
     * valueType : 0
     * error : 
     * valueMap : null
     * hasAlert : 0
     * triggerIds : 
     * triggerState : 0
     * itemStatus : 0
     */

    private String newDataId;
    private String type;
    private String applicationName;
    private String parentId;
    private String interval;
    private String historyPeriod;
    private String trendPeriod;
    private String resentlyCheckDate;
    private String newData;
    private String change;
    private String newDataName;
    private String valueMapId;
    private String valueType;
    private String error;
    private String valueMap;
    private String hasAlert;
    private String triggerIds;
    private String triggerState;
    private String itemStatus;

    public String getNewDataId() {
        return newDataId;
    }

    public void setNewDataId(String newDataId) {
        this.newDataId = newDataId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getInterval() {
        return interval;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }

    public String getHistoryPeriod() {
        return historyPeriod;
    }

    public void setHistoryPeriod(String historyPeriod) {
        this.historyPeriod = historyPeriod;
    }

    public String getTrendPeriod() {
        return trendPeriod;
    }

    public void setTrendPeriod(String trendPeriod) {
        this.trendPeriod = trendPeriod;
    }

    public String getResentlyCheckDate() {
        return resentlyCheckDate;
    }

    public void setResentlyCheckDate(String resentlyCheckDate) {
        this.resentlyCheckDate = resentlyCheckDate;
    }

    public String getNewData() {
        return newData;
    }

    public void setNewData(String newData) {
        this.newData = newData;
    }

    public String getChange() {
        return change;
    }

    public void setChange(String change) {
        this.change = change;
    }

    public String getNewDataName() {
        return newDataName;
    }

    public void setNewDataName(String newDataName) {
        this.newDataName = newDataName;
    }

    public String getValueMapId() {
        return valueMapId;
    }

    public void setValueMapId(String valueMapId) {
        this.valueMapId = valueMapId;
    }

    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getValueMap() {
        return valueMap;
    }

    public void setValueMap(String valueMap) {
        this.valueMap = valueMap;
    }

    public String getHasAlert() {
        return hasAlert;
    }

    public void setHasAlert(String hasAlert) {
        this.hasAlert = hasAlert;
    }

    public String getTriggerIds() {
        return triggerIds;
    }

    public void setTriggerIds(String triggerIds) {
        this.triggerIds = triggerIds;
    }

    public String getTriggerState() {
        return triggerState;
    }

    public void setTriggerState(String triggerState) {
        this.triggerState = triggerState;
    }

    public String getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(String itemStatus) {
        this.itemStatus = itemStatus;
    }

    @Override
    public int getItemType() {
        return ExpandableItemAdapter.TYPE_PERSON;
    }
}
