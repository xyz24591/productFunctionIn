package com.neo.productitsms.pro.third.chart;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewTreeObserver;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.neo.productitsms.R;
import com.neo.productitsms.http.framework.UIThreadCallBack;
import com.neo.productitsms.mvp.presenter.impl.MvpBasePresenter;
import com.neo.productitsms.pro.base.view.activity.BaseActivtiy;
import com.neo.productitsms.pro.third.chart.entity.CharHisResponse;
import com.neo.productitsms.pro.third.chart.entity.CharHistoryEntity;
import com.neo.productitsms.pro.third.chart.presenter.LineChaetPresenter;
import com.neo.productitsms.pro.third.chart.utils.TimeUtils;
import com.neo.productitsms.pro.third.chart.view.LineChartInViewPager;
import com.neo.productitsms.pro.third.chart.view.NewMarkerView;
import com.neo.productitsms.utils.GsonUtil;
import com.neo.productitsms.utils.LogUtils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Administrator on 2018/10/9.
 */

public class LineChartShowActivity extends BaseActivtiy {

    //    getLineChartData
    LineChaetPresenter presenter;
    @BindView(R.id.new_lineChart)
    LineChartInViewPager mLineChart;

    private static final String DATA_STR = "data";
    private String dataItemId;

    public static void actoinStart(Context context, String data) {

        Intent intent = new Intent(context, LineChartShowActivity.class);

        intent.putExtra(DATA_STR, data);
        context.startActivity(intent);

    }


    @Override
    public MvpBasePresenter bindPresenter() {

        presenter = new LineChaetPresenter(this);

        return presenter;
    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_line_chart_show;
    }

    @Override
    protected void initData() {

        Intent intent = getIntent();

        dataItemId = intent.getStringExtra(DATA_STR);

    }

    private void processSBData() {

        CharHistoryEntity historyEntity = new CharHistoryEntity();

        String nowTimeStamp = TimeUtils.getNowTimeStamp();

        String beforeOneHourTimeStamp = TimeUtils.getBeforeHourTimeStamp();


        LogUtils.e("当前时间戳：" + nowTimeStamp + ";;前;" + beforeOneHourTimeStamp);

        LogUtils.i("当前时间是：" + TimeUtils.UnixTimestampToNormal(nowTimeStamp));
        LogUtils.i("前时间是：" + TimeUtils.UnixTimestampToNormal(beforeOneHourTimeStamp));


//        historyEntity.setItemId(dataItemId);
//        historyEntity.setStartTime(beforeOneHourTimeStamp);
//        historyEntity.setEndTime(nowTimeStamp);

        historyEntity.setItemId("504833");
        historyEntity.setStartTime("1538969137");
        historyEntity.setEndTime("1539055537");

//        final RequestBody requestBody = RequestManger.getInstance().getRequestBody(historyEntity);
//
//        Call<String> call = RequestManger.getInstance().getStringService().getItemHistory(requestBody);
//        call.enqueue(new Callback<String>() {
//            @Override
//            public void onResponse(Call<String> call, Response<String> response) {
//
////                String jsonData = response.body();
////
////                JsonParser jsonParser = new JsonParser();
////                JsonElement jsonElement = jsonParser.parse(jsonData);
////
////                if (jsonElement != null) {
////                    JsonObject jsonObject = jsonElement.getAsJsonObject();
////
////                    JsonObject bring = jsonObject.get("bring").getAsJsonObject();
////                    JsonArray history = bring.get("history").getAsJsonArray();
////
////                    LogUtils.i("-----<" + GsonUtil.GsonString(history.get(0)));
////                }
//
//
//                CharHisResponse historyData = GsonUtil.GsonToBean(response.body(), CharHisResponse.class);
//
//                LogUtils.d("-转换>" + GsonUtil.GsonString(historyData));
//
//                CharHisResponse.BringBean bring = historyData.getBring();
//
//                List<CharHisResponse.BringBean.HistoryBean> history = bring.getHistory();
//
//
//                setLineData(history);
//
//
//            }
//
//            @Override
//            public void onFailure(Call<String> call, Throwable t) {
//
//            }
//        });

        presenter.getHistoryData(historyEntity, new UIThreadCallBack<String>() {
            @Override
            public void onResponse(Response<String> response) {
                LogUtils.i("onResponse---->" + response.body());

                CharHisResponse historyData = GsonUtil.GsonToBean(response.body(), CharHisResponse.class);

                LogUtils.d("-转换>" + GsonUtil.GsonString(historyData));

                CharHisResponse.BringBean bring = historyData.getBring();
                List<CharHisResponse.BringBean.HistoryBean> history = bring.getHistory();

                setLineData(history);

            }

            @Override
            public void onResFalse(String message) {
                LogUtils.i("onResFalse---->" + message);
            }

            @Override
            public void onFail(String errorMessage) {

                LogUtils.i("onResFalse---->" + errorMessage);
            }
        });


    }


    @Override
    protected void initView() {


        mLineChart.setLogEnabled(true);//打印日志
        //取消描述文字
        mLineChart.getDescription().setEnabled(false);
        mLineChart.setNoDataText("没有数据");//没有数据时显示的文字
        mLineChart.setNoDataTextColor(Color.WHITE);//没有数据时显示文字的颜色
        mLineChart.setDrawGridBackground(false);//chart 绘图区后面的背景矩形将绘制
        mLineChart.setDrawBorders(false);//是否禁止绘制图表边框的线
        mLineChart.setBorderColor(Color.WHITE); //设置 chart 边框线的颜色。
        mLineChart.setBorderWidth(3f); //设置 chart 边界线的宽度，单位 dp。
        mLineChart.setTouchEnabled(true);     //能否点击
        mLineChart.setDragEnabled(true);   //能否拖拽
        mLineChart.setScaleEnabled(true);  //能否缩放
        mLineChart.animateX(1000);//绘制动画 从左到右
        mLineChart.setDoubleTapToZoomEnabled(false);//设置是否可以通过双击屏幕放大图表。默认是true
        mLineChart.setHighlightPerDragEnabled(false);//能否拖拽高亮线(数据点与坐标的提示线)，默认是true
        mLineChart.setDragDecelerationEnabled(false);//拖拽滚动时，手放开是否会持续滚动，默认是true（false是拖到哪是哪，true拖拽之后还会有缓冲）

        Description description = new Description();
        description.setText("X 描述");

        mLineChart.setDescription(description);

//        MyMarkerView mv = new MyMarkerView(mActivity,
//                R.layout.custom_marker_view);
//        mv.setChartView(mLineChart); // For bounds control
//        mLineChart.setMarker(mv);        //设置 marker ,点击后显示的功能 ，布局可以自定义


        NewMarkerView mv = new NewMarkerView(this, R.layout.custom_marker_view_layout, false);
        mv.setChartView(mLineChart); // For bounds control
        mLineChart.setMarker(mv);        //设置 marker ,点击后显示的功能 ，布局可以自定义

        XAxis xAxis = mLineChart.getXAxis();       //获取x轴线
        xAxis.setDrawAxisLine(true);//是否绘制轴线
        xAxis.setDrawGridLines(false);//设置x轴上每个点对应的线
        xAxis.setDrawLabels(true);//绘制标签  指x轴上的对应数值
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);//设置x轴的显示位置
        xAxis.setTextSize(12f);//设置文字大小
        xAxis.setAxisMinimum(0f);//设置x轴的最小值 //`
        xAxis.setAxisMaximum(31f);//设置最大值 //
        xAxis.setLabelCount(10, false);  //设置X轴的显示个数
        xAxis.setAvoidFirstLastClipping(false);//图表将避免第一个和最后一个标签条目被减掉在图表或屏幕的边缘

        xAxis.setGranularity(1f); //设置X轴坐标之间的最小间隔（因为此图有缩放功能，X轴,Y轴可设置可缩放）
        xAxis.setLabelRotationAngle(-60); // 设置 X 逆时针旋转 60度

        xAxis.setAxisLineColor(Color.WHITE);//设置x轴线颜色
        xAxis.setAxisLineWidth(0.5f);//设置x轴线宽度
        YAxis leftAxis = mLineChart.getAxisLeft();
        YAxis axisRight = mLineChart.getAxisRight();
        leftAxis.enableGridDashedLine(10f, 10f, 0f);  //设置Y轴网格线条的虚线，参1 实线长度，参2 虚线长度 ，参3 周期
        leftAxis.setGranularity(20f); // 网格线条间距
        axisRight.setEnabled(false);   //设置是否使用 Y轴右边的
        leftAxis.setEnabled(true);     //设置是否使用 Y轴左边的
        leftAxis.setGridColor(Color.parseColor("#7189a9"));  //网格线条的颜色
        leftAxis.setDrawLabels(true);        //是否显示Y轴刻度
        leftAxis.setStartAtZero(true);        //设置Y轴数值 从零开始
        leftAxis.setDrawGridLines(true);      //是否使用 Y轴网格线条
        leftAxis.setTextSize(12f);            //设置Y轴刻度字体
        leftAxis.setTextColor(Color.WHITE);   //设置字体颜色
        leftAxis.setAxisLineColor(Color.WHITE); //设置Y轴颜色
        leftAxis.setAxisLineWidth(0.5f);
        leftAxis.setDrawAxisLine(true);//是否绘制轴线
        leftAxis.setMinWidth(0f);
        leftAxis.setMaxWidth(200f);
        leftAxis.setDrawGridLines(false);//设置x轴上每个点对应的线
        Legend l = mLineChart.getLegend();//图例
        l.setEnabled(false);   //是否使用 图例

//        setLineData(null);
        processSBData();
    }

    List<Entry> mValues;

    private void setLineData(final List<CharHisResponse.BringBean.HistoryBean> history) {
        mValues = new ArrayList<>();
        for (int i = 0; i < history.size(); i++) {

            CharHisResponse.BringBean.HistoryBean historyBean = history.get(i);
            String newData = historyBean.getItem().getNewData();

            BigDecimal decimal = new BigDecimal(newData);

            mValues.add(new Entry(i, decimal.floatValue()));

        }


//        mValues = new ArrayList<>();
//        mValues.add(new Entry(0, 10));
//        mValues.add(new Entry(1, 15));
//        mValues.add(new Entry(2, 25));
//        mValues.add(new Entry(3, 19));
//        mValues.add(new Entry(4, 25));
//        mValues.add(new Entry(5, 16));
//        mValues.add(new Entry(6, 40));
//        mValues.add(new Entry(7, 27));


        LineDataSet set1;
        //判断图表中原来是否有数据
        if (mLineChart.getData() != null && mLineChart.getData().getDataSetCount() > 0) {

            set1 = (LineDataSet) mLineChart.getData().getDataSetByIndex(0);
            set1.setValues(mValues);
//            刷新数据
            mLineChart.getData().notifyDataChanged();
            mLineChart.notifyDataSetChanged();
        } else {
//            设置数据 参数1： 数据源 参数2：图例名称
            set1 = new LineDataSet(mValues, "测试数据1");
            set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            set1.setColor(Color.WHITE);
            set1.setCircleColor(Color.parseColor("#AAFFFFFF"));
            set1.setHighLightColor(Color.WHITE);//设置点击交点后显示交高亮线的颜色
            set1.setHighlightEnabled(true);//是否使用点击高亮线
            set1.setDrawCircles(true);
            set1.setValueTextColor(Color.WHITE);
            set1.setLineWidth(1f);//设置线宽
            set1.setCircleRadius(2f);//设置焦点圆心的大小
            set1.setHighlightLineWidth(0.5f);//设置点击交点后显示高亮线宽
            set1.enableDashedHighlightLine(10f, 5f, 0f);//点击后的高亮线的显示样式
            set1.setValueTextSize(12f);//设置显示值的文字大小
            set1.setDrawFilled(true);//设置使用 范围背景填充

            set1.setDrawValues(false);
            //格式化显示数据
            final DecimalFormat mFormat = new DecimalFormat("###,###,##0");
            set1.setValueFormatter(new IValueFormatter() {
                @Override
                public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                    return mFormat.format(value);
                }
            });


            mLineChart.getXAxis().setValueFormatter(new IAxisValueFormatter() {
                @Override
                public String getFormattedValue(float value, AxisBase axis) {

                    return history.get(((int) value)).getTime();
                }
            });

            if (Utils.getSDKInt() >= 18) {
                // fill drawable only supported on api level 18 and above
                Drawable drawable = ContextCompat.getDrawable(LineChartShowActivity.this, R.color.common_link_blue);
                set1.setFillDrawable(drawable);//设置范围背景填充
            } else {
                set1.setFillColor(R.color.common_link_blue);
            }
            ArrayList<ILineDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1); // add the datasets
            //创建LineData对象 属于LineChart折线图的数据集合
            LineData data = new LineData(dataSets);
            // 添加到图表中
            mLineChart.setData(data);
            //绘制图表
            mLineChart.invalidate();


        }


    }


}
