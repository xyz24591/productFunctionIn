package com.neo.productitsms.pro.ui.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.neo.productitsms.R;
import com.neo.productitsms.pro.ui.Entity.SysNoticeEntity;

import java.util.List;

/**
 * Created by Administrator on 2018/8/2.
 */

public class SysNoticeAdapter extends BaseQuickAdapter<SysNoticeEntity, BaseViewHolder> {
    public SysNoticeAdapter(@LayoutRes int layoutResId, @Nullable List<SysNoticeEntity> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, SysNoticeEntity item) {

        ((TextView) helper.getView(R.id.sys_item_time_tv)).setText(item.getSysTime());
        ((TextView) helper.getView(R.id.sys_item_tip_tv)).setText(item.getSysNotice());


    }
}
