package com.neo.productitsms.pro.ui;

import com.neo.productitsms.R;
import com.neo.productitsms.mvp.presenter.impl.MvpBasePresenter;
import com.neo.productitsms.pro.base.view.activity.BaseActivtiy;

/**
 * Created by Administrator on 2018/7/28.
 */

public class AccountSettingActivity extends BaseActivtiy {
    @Override
    public MvpBasePresenter bindPresenter() {
        return null;
    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_account_setting;

    }


//    @Override
//    protected void setMiddleTitle(String middleTitle) {
//        super.setMiddleTitle(middleTitle);
//    }

    @Override
    protected void initData() {
        setMiddleTitle("账号设置");
    }

    @Override
    protected void initView() {

    }
}
