package com.neo.productitsms.pro.compose.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.neo.productitsms.pro.base.view.activity.BaseMapActivity;
import com.neo.productitsms.pro.compose.presenter.ComposePresenter;

/**
 * Created by Administrator on 2018/6/26.
 */

public abstract class CompseTextActivity extends BaseMapActivity {

    private ComposePresenter composePresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        TextView textView = new TextView(this);
        textView.setText("一个 界面");
        setContentView(textView);
    }


    @Override
    public void bindPresenter() {
        composePresenter = new ComposePresenter(this);
    }
}
