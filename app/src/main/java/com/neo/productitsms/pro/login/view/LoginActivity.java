package com.neo.productitsms.pro.login.view;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.neo.productitsms.R;
import com.neo.productitsms.bean.UserBean;
import com.neo.productitsms.bean.UserRespBean;
import com.neo.productitsms.config.ConstantParams;
import com.neo.productitsms.http.framework.RequestCallBack;
import com.neo.productitsms.http.framework.UIThreadCallBack;
import com.neo.productitsms.mvp.presenter.impl.MvpBasePresenter;
import com.neo.productitsms.pro.MainActivity;
import com.neo.productitsms.pro.base.view.activity.BaseActivtiy;
import com.neo.productitsms.pro.base.view.activity.BaseMapActivity;
import com.neo.productitsms.pro.login.presenter.LoginPresenter;
import com.neo.productitsms.pro.login.widget.BottomDialog;
import com.neo.productitsms.utils.GetUserInfoUtils;
import com.neo.productitsms.utils.GsonUtil;
import com.neo.productitsms.utils.LogUtils;
import com.neo.productitsms.utils.SPUtils;
import com.neo.productitsms.utils.ToastUtil;

import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by Administrator on 2018/6/29.
 */

public class LoginActivity extends BaseActivtiy {

    private LoginPresenter loginPresenter;

    @BindView(R.id.login_other_way_tv)
    TextView loginOtherWayTv;

    @BindView(R.id.bt_login)
    Button btLogin;

    @BindView(R.id.et_phone)
    EditText etPhone;

    @BindView(R.id.et_password)
    EditText etPassword;


    @Override
    public MvpBasePresenter bindPresenter() {
        loginPresenter = new LoginPresenter(this);
        return loginPresenter;
    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_login;
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
        Map<String, String> loginInfo = GetUserInfoUtils.getInstance(this).getLoginInfo();

        if (loginInfo != null) {
            String userName = loginInfo.get(ConstantParams.USER_NAME);
            String pwd = loginInfo.get(ConstantParams.USER_PWD);
            etPhone.setText(userName);
            etPassword.setText(pwd);
            if (!TextUtils.isEmpty(userName) && !TextUtils.isEmpty(pwd)) {
                login(userName, pwd);
            }
        }
    }

    @OnClick({R.id.login_other_way_tv, R.id.bt_login})
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.login_other_way_tv:
                BottomDialog bottomDialog = new BottomDialog();
                bottomDialog.show(getSupportFragmentManager(), "bottomDialog");
                break;
            case R.id.bt_login:

                String userName = etPhone.getText().toString().trim();
                String password = etPassword.getText().toString().trim();

                if (TextUtils.isEmpty(userName)) {
                    ToastUtil.showShort("请输入用户名");
                    return;
                }
                if (TextUtils.isEmpty(password)) {
                    ToastUtil.showShort("请输入密码");
                    return;
                }

                login(userName, password);
                break;


            default:
                break;

        }


    }


    public void login(final String userName, String pwd) {
        UserBean userBean = new UserBean();
        userBean.setUsername(userName);
        userBean.setPassword(pwd);
        loginPresenter.login(userBean, new UIThreadCallBack<UserRespBean>() {
            @Override
            public void onResponse(Response<UserRespBean> response) {

                UserRespBean body = response.body();

                LogUtils.i(GsonUtil.GsonString(body));

                SPUtils.getInstance().putString(LoginActivity.this, ConstantParams.USER_NAME, userName);
                SPUtils.getInstance().putString(LoginActivity.this, ConstantParams.USER_PWD, userName);

                if (body != null) {
                    GetUserInfoUtils.getInstance(LoginActivity.this).setUserInfo(body.getBring());
                }
//                ToastUtil.showShort("成功");
                openActivity(MainActivity.class);
                onBackPressed();

            }

            @Override
            public void onResFalse(String message) {

                ToastUtil.showShort(message);

            }

            @Override
            public void onFail(String errorMessage) {
                ToastUtil.showShort(errorMessage);

            }
        });
    }


    public void finishClick(View view) {

        onBackPressed();

    }

}
