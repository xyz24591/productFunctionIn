package com.neo.productitsms.pro.ui;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.neo.productitsms.R;
import com.neo.productitsms.mvp.presenter.impl.MvpBasePresenter;
import com.neo.productitsms.pro.base.view.activity.BaseActivtiy;
import com.neo.productitsms.pro.ui.Entity.SysNoticeEntity;
import com.neo.productitsms.pro.ui.adapter.SysNoticeAdapter;
import com.neo.productitsms.utils.ToastUtil;
import com.neo.productitsms.utils.UIUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/8/2.
 */

public class SystemNoticeActivity extends BaseActivtiy {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.sys_notice_recyclerview)
    RecyclerView sysNoticeRecyclerview;
    List<SysNoticeEntity> entities;

    @Override
    public MvpBasePresenter bindPresenter() {
        return null;
    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_sys_notice;
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {

        sysNoticeRecyclerview.setHasFixedSize(true);
        sysNoticeRecyclerview.setLayoutManager(new LinearLayoutManager(this));
        getData();
        initAdapter();


    }

    private void getData() {

        entities = new ArrayList<>();

        for (int i = 0; i < 3; i++) {
            SysNoticeEntity noticeEntity = new SysNoticeEntity();
            noticeEntity.setSysTime(UIUtils.getCurrentTime());
            noticeEntity.setSysNotice("更新提示" + i);
            entities.add(noticeEntity);
        }

    }

    private void initAdapter() {

        SysNoticeAdapter sysNoticeAdapter = new SysNoticeAdapter(R.layout.sys_notice_item, entities);

        sysNoticeRecyclerview.setAdapter(sysNoticeAdapter);
        sysNoticeAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {

                ToastUtil.showShort("点击查看...." + position);

            }
        });

    }


}
