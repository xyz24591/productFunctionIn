package com.neo.productitsms.pro.compose.view;

import android.view.View;

import com.neo.productitsms.R;
import com.neo.productitsms.pro.base.view.fragment.BaseFragment;

/**
 * Created by Administrator on 2018/6/27.
 */

public class ComposeFragment extends BaseFragment{

    @Override
    public int bindLayoutId() {
        return R.layout.fragment_compose;
    }

    @Override
    public void initContentView(View contentView) {

    }

    @Override
    public void initData() {
        super.initData();
    }
}
