package com.neo.productitsms.pro.home.widget.drag;

/**
 * Created by Administrator on 2018/7/23.
 */

public interface DragCallback {

    void startDrag(int position);

    void endDrag(int position);

}
