package com.neo.productitsms.pro.home.presenter;

import android.content.Context;

import com.neo.productitsms.pro.base.presenter.BasePresenter;
import com.neo.productitsms.pro.base.view.lce.ResultView;
import com.neo.productitsms.pro.home.model.HomeModel;

/**
 * Created by Administrator on 2018/6/27.
 */

public class HomePresenter extends BasePresenter<HomeModel> {
    public HomePresenter(Context context) {
        super(context);
    }

    @Override
    public HomeModel bindModel() {
        return new HomeModel(getContext());
    }

    public void getHomeData() {

        getModel().getHomeDataFromServer();
    }

}
