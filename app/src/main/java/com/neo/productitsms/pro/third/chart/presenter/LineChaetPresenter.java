package com.neo.productitsms.pro.third.chart.presenter;

import android.content.Context;

import com.neo.productitsms.http.framework.RequestStringCallBack;
import com.neo.productitsms.http.framework.UIThreadCallBack;
import com.neo.productitsms.http.framework.impl.RequestManger;
import com.neo.productitsms.pro.base.presenter.BasePresenter;
import com.neo.productitsms.pro.third.chart.JsonDataProvider;
import com.neo.productitsms.pro.third.chart.entity.CharHistoryEntity;
import com.neo.productitsms.pro.third.chart.model.LineChartModel;

import retrofit2.Response;

/**
 * Created by Administrator on 2018/10/10.
 */

public class LineChaetPresenter extends BasePresenter<LineChartModel> {
    LineChartModel chartModel;

    public LineChaetPresenter(Context context) {
        super(context);
        chartModel = new LineChartModel(context);
    }

    @Override
    public LineChartModel bindModel() {
        return new LineChartModel(getContext());
    }

    public void getLineChartData(String equId, final UIThreadCallBack callBack) {

        getModel().getLineData(equId, new RequestStringCallBack() {
            @Override
            public void onResponse(Response<String> response) {
                callBack.onResponse(response);
            }

            @Override
            public void onResFalse(String message) {


                callBack.onResFalse(message);
            }

            @Override
            public void onFail(String errorMessage) {

                String jsonData = JsonDataProvider.StringHolder();

//                callBack.onFail(errorMessage);
                callBack.onFail(jsonData);
            }
        });

    }


    public void getHistoryData(CharHistoryEntity historyEntity, final UIThreadCallBack callBack) {

        getModel().getItemHistory(historyEntity, new RequestStringCallBack() {
            @Override
            public void onResponse(Response<String> response) {
                callBack.onResponse(response);
            }

            @Override
            public void onResFalse(String message) {
                callBack.onResFalse(message);
            }

            @Override
            public void onFail(String errorMessage) {
                String jsonData = JsonDataProvider.StringHolder();
//                callBack.onFail(errorMessage);
                callBack.onFail(jsonData);
            }
        });

    }


}
