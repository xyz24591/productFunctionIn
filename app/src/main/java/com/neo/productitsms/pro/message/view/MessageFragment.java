package com.neo.productitsms.pro.message.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.neo.productitsms.R;
import com.neo.productitsms.pro.base.view.fragment.BaseFragment;
import com.neo.productitsms.pro.base.view.navigation.impl.DefaultNavigation;
import com.neo.productitsms.pro.message.adapter.MessageInfoAdapter;
import com.neo.productitsms.pro.message.model.MessageEntity;
import com.neo.productitsms.pro.ui.SystemNoticeActivity;
import com.neo.productitsms.utils.ToastUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Administrator on 2018/6/27.
 */

public class MessageFragment extends BaseFragment {
    @BindView(R.id.message_info_recyclerview)
    RecyclerView messageInfoRecyclerview;
    @BindView(R.id.message_notice_fl)
    FrameLayout messageNoticeFl;
    @BindView(R.id.message_todo_fl)
    FrameLayout messageTodoFl;
    @BindView(R.id.message_friends_fl)
    FrameLayout messageFriendsFl;
    @BindView(R.id.message_subscribe_fl)
    FrameLayout messageSubscribeFl;

    List<MessageEntity> messageEntities;

    @Override
    public int bindLayoutId() {
        return R.layout.fragment_message;
    }

    @Override
    public void initContentView(View contentView) {
        initLayoutView(contentView);
    }

    private void initLayoutView(View contentView) {

        initNavigation(contentView);

    }

    private void initNavigation(View contentView) {

        DefaultNavigation.Builder builder = new DefaultNavigation.Builder(getContext(), (ViewGroup) contentView);
        builder.setCenterText(R.string.tab_message_text).create().createAndBind();

    }


    @Override
    protected void initData() {
        messageInfoRecyclerview.setHasFixedSize(true);
        messageInfoRecyclerview.setLayoutManager(new LinearLayoutManager(getContext()));
        getData();
        initAdapter();

    }

    private void getData() {

        messageEntities = new ArrayList<>();
        for (int i = 0; i < 5; i++) {

            MessageEntity messageEntity = new MessageEntity();
            messageEntity.setMsgAvtar("");
            messageEntity.setMsgTitle("姓名->" + i);
            messageEntity.setMsgDetail("详情->" + i);
            messageEntity.setMsgTime("时间->" + i);
            messageEntities.add(messageEntity);
        }


    }

    private void initAdapter() {
        MessageInfoAdapter messageInfoAdapter = new MessageInfoAdapter(R.layout.message_item_info, messageEntities);

        messageInfoRecyclerview.setAdapter(messageInfoAdapter);

        messageInfoAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
//                ToastUtil.showShort("-点击-" + position);

                Intent intent = new Intent(getContext(), SystemNoticeActivity.class);
                startActivity(intent);


            }
        });
    }
}
