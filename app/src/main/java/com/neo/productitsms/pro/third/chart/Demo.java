package com.neo.productitsms.pro.third.chart;

import android.graphics.Color;
import android.os.Bundle;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.neo.productitsms.R;
import com.neo.productitsms.mvp.presenter.impl.MvpBasePresenter;
import com.neo.productitsms.pro.base.view.activity.BaseActivtiy;
import com.neo.productitsms.pro.third.chart.view.DemoMarkerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/10/13.
 * <p>
 * <p>
 * MPAndroidChart 一条曲线
 */

public class Demo extends BaseActivtiy {
    @BindView(R.id.line_chart)
    LineChart mLineChart;

    @Override
    public MvpBasePresenter bindPresenter() {
        return null;
    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_demo;
    }

    @Override
    protected void initData() {


    }

    @Override
    protected void initView() {

        initLineChart();

    }

    private void initLineChart() {
//生产数据
        final List<Entry> entries = new ArrayList<>();

        for (int i = 0; i < 12; i++) {
            entries.add(new Entry(i, ((float) Math.random() * 80)));
        }

        final List<String> months = new ArrayList<>();
        for (int i = 0; i < 13; i++) {
            months.add(i + "月份");
        }

//显示边界
        mLineChart.setDrawBorders(true);
//一个 lineDataSet就是 一条线
        LineDataSet lineDataSet = new LineDataSet(entries, "温度");
        lineDataSet.setColor(Color.GREEN);
        lineDataSet.setValueTextColor(Color.GRAY);
        lineDataSet.setCircleRadius(2f);
        //    线 模式 为 圆滑模式 （默认折线）
        lineDataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);

        lineDataSet.setValueFormatter(new IValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {

                return String.valueOf(((int) value)) + "摄氏度";
            }
        });


        LineData data = new LineData(lineDataSet);
        mLineChart.setData(data);

//        设置 X 轴
        XAxis xAxis = mLineChart.getXAxis();

        //值：BOTTOM,BOTH_SIDED,BOTTOM_INSIDE,TOP,TOP_INSIDE
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

//        设置X轴坐标之间的最小间隔（因为此图有缩放功能，X轴,Y轴可设置可缩放）
        xAxis.setGranularity(1f);
//设置 X 轴 刻度 数量,
// 第二个参数表示是否平均分配 如果为true则按比例分为12个点、如果为false则适配X刻度的值来分配点，可能没有12个点。
//        对比图：左图的参数为true，右图的参数为false
// 即 true X 轴 刻度总数 不变，false X 轴 刻度数总数 变化
        xAxis.setLabelCount(12, false);

//设置 x 轴 上 文字描述的 旋转角度
        xAxis.setLabelRotationAngle(-60);
//        设置X轴的值（最小值、最大值、然后会根据设置的刻度数量自动分配刻度显示）
//        xAxis.setAxisMinimum(0f);
//        xAxis.setAxisMaximum(20f);

//        设置 X轴 为 字符串
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return months.get(((int) value));
            }
        });


//        想要显示完整的12个月份，要与（x,y）坐标对应数量 10 改成 12
//        for (int i = 0; i < 12; i++) {
//            entries.add(new Entry(i, (float) (Math.random()) * 80));
//        }


        /************************设置 Y轴***********************************/

        YAxis leftYAxis = mLineChart.getAxisLeft();

        YAxis rightYAxis = mLineChart.getAxisRight();
//设置 左边的 Y轴的最大值 和最小值
        leftYAxis.setAxisMinimum(0f);
        leftYAxis.setAxisMaximum(100f);
//设置 右边的 Y轴 最大值 和最小值
        rightYAxis.setAxisMinimum(0f);
        rightYAxis.setAxisMaximum(100f);

//        设置 左边的 Y轴 单位
        leftYAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {

                return String.valueOf(value) + "*";
            }
        });
//        设置 左侧 Y轴的最小间隔
        leftYAxis.setGranularity(10f);
//        设置 右侧Y轴 不显示
//        rightYAxis.setEnabled(false);

//        X轴 和 Y轴 具有相同的属性方法

        rightYAxis.setGranularity(1f);
        rightYAxis.setLabelCount(11, false);
        rightYAxis.setTextColor(Color.BLUE);
        rightYAxis.setGridColor(Color.DKGRAY);
        rightYAxis.setAxisLineColor(Color.GREEN);

//        设置 限制线 limitLine

        LimitLine limitLine = new LimitLine(45, "温度限制");
//        限制线 宽度
        limitLine.setLineWidth(2f);
        limitLine.setTextSize(10f);
        limitLine.setTextColor(Color.RED);
        limitLine.setLineColor(Color.BLUE);
        rightYAxis.addLimitLine(limitLine);

//        legend 图例 上图所示的曲线图下面的 温度 标签 文字描述
//获取 legend
        Legend legend = mLineChart.getLegend();
//        设置 legend 的 位置
//       设置 legend 的 文字颜色
        legend.setTextColor(Color.GRAY);
//        legend.setForm(Legend.LegendForm.CIRCLE);
        // 设置 标签太多 是否换行
        legend.setWordWrapEnabled(true);
//隐匿 legend false, true 显示 legend
        legend.setEnabled(true);

        legend.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        legend.setOrientation(Legend.LegendOrientation.HORIZONTAL);

////        隐匿 描述
//        Description description=new Description();
//        description.setEnabled(false);
//        mLineChart.setDescription(description);

        Description description = new Description();
        description.setText("X 轴 描述");
        description.setTextColor(Color.DKGRAY);
        mLineChart.setDescription(description);

        DemoMarkerView markerView = new DemoMarkerView(this);
        mLineChart.setMarkerView(markerView);

//        LineDataSet lineDataSet1 = new LineDataSet(entries, "温度2");
////        设置 曲线值的 原点是 空心还是 实心
//        lineDataSet1.setDrawCircleHole(false);
////        设置 显示值的 字体大小
//        lineDataSet1.setValueTextSize(9f);
////    线 模式 为 圆滑模式 （默认折线）
//        lineDataSet1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//
//        LineData data1 = new LineData(lineDataSet1);
//
//        mLineChart.setData(data1);


    }


}
