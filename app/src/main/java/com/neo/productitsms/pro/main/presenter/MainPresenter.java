package com.neo.productitsms.pro.main.presenter;

import android.content.Context;

import com.neo.productitsms.pro.base.presenter.BasePresenter;
import com.neo.productitsms.pro.base.view.lce.ResultView;
import com.neo.productitsms.pro.main.model.MainModel;
import com.neo.productitsms.utils.LogUtils;

/**
 * Created by Administrator on 2018/6/27.
 */

public class MainPresenter extends BasePresenter<MainModel> {
    public MainPresenter(Context context) {
        super(context);
    }

    @Override
    public MainModel bindModel() {
        return new MainModel(getContext());
    }

    public void getDataFromServer(){
        LogUtils.d("presenter 中请求...");
        getModel().getHomeInfoFromServer();

    }

}
