package com.neo.productitsms.pro.login.widget;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.allen.library.SuperTextView;
import com.neo.productitsms.R;
import com.neo.productitsms.utils.UIUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Administrator on 2018/7/18.
 */

public class BottomDialog extends BottomSheetDialogFragment {

    @BindView(R.id.bt_login_wx)
    Button btLoginWx;
    @BindView(R.id.bt_login_wb)
    Button btLoginWb;
    @BindView(R.id.bt_login_qq)
    Button btLoginQq;
    @BindView(R.id.login_dismiss)
    SuperTextView loginDismiss;
    Unbinder unbinder;
    private View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = UIUtils.inflate(R.layout.fragment_bottom_login);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @OnClick({R.id.bt_login_wx, R.id.bt_login_wb, R.id.bt_login_qq, R.id.login_dismiss})
    public void OnClick(View view) {

        switch (view.getId()) {

            case R.id.bt_login_wx:

                Toast.makeText(getContext(), "微信", Toast.LENGTH_SHORT).show();
                break;
            case R.id.bt_login_wb:

                Toast.makeText(getContext(), "微博", Toast.LENGTH_SHORT).show();
                break;
            case R.id.bt_login_qq:

                Toast.makeText(getContext(), "qq", Toast.LENGTH_SHORT).show();
                break;
            case R.id.login_dismiss:
                dismiss();
                break;


            default:
                break;
        }

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
