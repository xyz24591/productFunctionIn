package com.neo.productitsms.config;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Administrator on 2018/6/26.
 */

public class AppLoginConfig {
    private static AppLoginConfig appConfig;

    private static final String NAME = "login";

    public static String IS_LOGIN = "isLogin";
    public static String IS_FIRST_IN = "isFirst";

    public static final String USER_NAME = "username";

    private SharedPreferences preferences;

    private AppLoginConfig() {

    }

    public static AppLoginConfig getInstance() {

        if (appConfig == null) {

            appConfig = new AppLoginConfig();

        }
        return appConfig;
    }


    public void initConfig(Context context) {

        if (preferences == null) {
            preferences = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        }

    }

    public void putString(Context context, String key, String value) {
        initConfig(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void putInt(Context context, String key, int value) {
        initConfig(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public void putBoolean(Context context, String key, boolean value) {
        initConfig(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public String getString(Context context, String key) {
        initConfig(context);
        return preferences.getString(key, null);
    }

    public int getInt(Context context, String key) {
        initConfig(context);
        return preferences.getInt(key, 0);
    }

    public boolean getBoolean(Context context, String key) {
        initConfig(context);
        return preferences.getBoolean(key, false);
    }


}
