package com.neo.productitsms.config;

/**
 * Created by Administrator on 2018/6/26.
 */

public class ConstantParams {

    public static final int ANIMATOR_DURATION = 3000;
    public static final int ANIMATOR_PER = 1000;

    /**
     * 允许输入 日志
     */
    public static boolean DEBUG = true;

    public static String BASE_URL = "http://192.168.1.172:8088";

    public static int REG_TOTAL_LONG = 60000;
    public static int REG_INTERVAL = 1000;

    public static String SP_NAME = "itsms";

    public static String DEFAULT_IP = "defaultIp";
    public static String DEFAULT_IP_INUSE = "useIp";

    public static final String EQU_ID = "c31cda441de248f6b47434f456481e8c";
    public static String QR_UID = "";
    public static String QR_USERID = "";

    //    保存的 key
    public static String USER_CONTENT = "user_content";
    public static String USER_NAME = "user_name";
    public static String USER_PWD = "user_pwd";

}
